//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SOJUSecurityConversationScoresSignal.h"

@class NSString, SOJUSecurityConversationScoreSignal;

@interface SOJUSecurityConversationScoresSignal : NSObject <SOJUSecurityConversationScoresSignal>
{
    SOJUSecurityConversationScoreSignal *_sender;
    SOJUSecurityConversationScoreSignal *_recipient;
}

@property(readonly, copy, nonatomic) SOJUSecurityConversationScoreSignal *recipient; // @synthesize recipient=_recipient;
@property(readonly, copy, nonatomic) SOJUSecurityConversationScoreSignal *sender; // @synthesize sender=_sender;
- (void).cxx_destruct;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
- (_Bool)isEqual:(id)arg1;
- (void)encodeWithCoder:(id)arg1;
- (id)initWithCoder:(id)arg1;
- (id)copyWithZone:(struct _NSZone *)arg1;
- (id)initWithSender:(id)arg1 recipient:(id)arg2;
- (id)toJson;
- (id)toDictionary;
- (id)initWithJSONDictionary:(id)arg1;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly) Class superclass;

@end

