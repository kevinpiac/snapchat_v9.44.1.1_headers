//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "NSCoding.h"
#import "NSFastEnumeration.h"

@class NSMutableArray, NSMutableDictionary;

@interface SCChatMessages : NSObject <NSFastEnumeration, NSCoding>
{
    NSMutableArray *_messagesArray;
    NSMutableDictionary *_messagesDictionary;
}

- (void).cxx_destruct;
- (unsigned long long)countByEnumeratingWithState:(CDStruct_70511ce9 *)arg1 objects:(id *)arg2 count:(unsigned long long)arg3;
- (id)messageWithMessageId:(id)arg1;
- (id)messages;
- (void)sort;
- (id)reverseObjectEnumerator;
- (unsigned long long)count;
- (id)lastObject;
- (_Bool)containsObject:(id)arg1;
- (unsigned long long)indexOfObjectIdenticalTo:(id)arg1;
- (unsigned long long)indexOfObject:(id)arg1;
- (id)objectAtIndex:(unsigned long long)arg1;
- (void)removeObject:(id)arg1;
- (void)removeObjects:(id)arg1;
- (void)addObject:(id)arg1;
- (void)addObjects:(id)arg1;
- (id)description;
- (void)encodeWithCoder:(id)arg1;
- (id)initWithCoder:(id)arg1;
- (id)initWithMessages:(id)arg1;
- (id)init;

@end

