//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCSearchCellController.h"

@class SCSearchNewsGroupCell, SCSearchNewsGroupViewModel;

@interface SCSearchNewsGroupCellController : SCSearchCellController
{
    SCSearchNewsGroupViewModel *_viewModel;
    SCSearchNewsGroupCell *_cellView;
}

- (void).cxx_destruct;
- (id)reuseIdentifier;
- (void)setupCellView:(id)arg1;
- (id)initWithViewModel:(id)arg1;

@end

