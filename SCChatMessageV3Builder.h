//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSArray, NSDate, NSDictionary, NSNumber, NSString, SCChatGroupUpdateContent, SCChatMediaContent, SCSnapState, SOJUSnapMetadata, SOJUSnapchatter, SOJUSticker, SOJUStoryShare;

@interface SCChatMessageV3Builder : NSObject
{
    NSString *_consistentId;
    NSString *_messageSender;
    NSString *_conversationId;
    NSDictionary *_knownChatSequenceNumbers;
    NSDate *_messageTimestamp;
    NSDate *_sentTimestamp;
    NSString *_iterToken;
    NSDictionary *_savedState;
    NSDictionary *_releaseState;
    NSDictionary *_preserveState;
    SCSnapState *_snapState;
    long long _type;
    NSString *_text;
    NSArray *_attributes;
    NSArray *_mediaCardAttributes;
    SCChatMediaContent *_media;
    SCChatMediaContent *_overlayMedia;
    NSArray *_medias;
    SOJUSticker *_sticker;
    SCChatGroupUpdateContent *_groupUpdate;
    SOJUSnapMetadata *_snapMetadata;
    NSString *_storyTitle;
    SOJUStoryShare *_storyShare;
    NSString *_mediaSendTaskId;
    SOJUSnapchatter *_snapchatterShare;
    long long _sendStatus;
    long long _groupId;
    unsigned long long _mischiefVersion;
    NSNumber *_typeVersion;
}

+ (id)withChatMessageV3:(id)arg1;
- (void).cxx_destruct;
- (id)setTypeVersion:(id)arg1;
- (id)setMischiefVersion:(unsigned long long)arg1;
- (id)setGroupId:(long long)arg1;
- (id)setSendStatus:(long long)arg1;
- (id)setSnapchatterShare:(id)arg1;
- (id)setMediaSendTaskId:(id)arg1;
- (id)setStoryShare:(id)arg1;
- (id)setStoryTitle:(id)arg1;
- (id)setSnapMetadata:(id)arg1;
- (id)setGroupUpdate:(id)arg1;
- (id)setSticker:(id)arg1;
- (id)setMedias:(id)arg1;
- (id)setOverlayMedia:(id)arg1;
- (id)setMedia:(id)arg1;
- (id)setMediaCardAttributes:(id)arg1;
- (id)setAttributes:(id)arg1;
- (id)setText:(id)arg1;
- (id)setType:(long long)arg1;
- (id)setSnapState:(id)arg1;
- (id)setPreserveState:(id)arg1;
- (id)setReleaseState:(id)arg1;
- (id)setSavedState:(id)arg1;
- (id)setIterToken:(id)arg1;
- (id)setSentTimestamp:(id)arg1;
- (id)setMessageTimestamp:(id)arg1;
- (id)setKnownChatSequenceNumbers:(id)arg1;
- (id)setConversationId:(id)arg1;
- (id)setMessageSender:(id)arg1;
- (id)setConsistentId:(id)arg1;
- (id)build;

@end

