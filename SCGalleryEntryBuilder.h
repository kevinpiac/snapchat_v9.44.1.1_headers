//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSDate, NSDictionary, NSString;

@interface SCGalleryEntryBuilder : NSObject
{
    NSString *_objectID;
    NSDate *_autosaveTimeUtc;
    NSDate *_createTimeUtc;
    NSDictionary *_dataVaultEncryption;
    NSDate *_duplicateTimeUtc;
    NSString *_entryId;
    int _galleryType;
    _Bool _isPrivate;
    int _pendingSyncs;
    long long _seqNum;
    NSString *_snapsHash;
    int _sources;
    NSDate *_syncedAutosaveTimeUtc;
    _Bool _syncedIsPrivate;
    NSString *_syncedTitle;
    NSString *_title;
    int _viewType;
}

+ (id)withGalleryEntry:(id)arg1;
- (void).cxx_destruct;
- (id)setViewType:(int)arg1;
- (id)setTitle:(id)arg1;
- (id)setSyncedTitle:(id)arg1;
- (id)setSyncedIsPrivate:(_Bool)arg1;
- (id)setSyncedAutosaveTimeUtc:(id)arg1;
- (id)setSources:(int)arg1;
- (id)setSnapsHash:(id)arg1;
- (id)setSeqNum:(long long)arg1;
- (id)setPendingSyncs:(int)arg1;
- (id)setIsPrivate:(_Bool)arg1;
- (id)setGalleryType:(int)arg1;
- (id)setEntryId:(id)arg1;
- (id)setDuplicateTimeUtc:(id)arg1;
- (id)setDataVaultEncryption:(id)arg1;
- (id)setCreateTimeUtc:(id)arg1;
- (id)setAutosaveTimeUtc:(id)arg1;
- (id)setObjectID:(id)arg1;
- (id)build;

@end

