//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSArray, NSDictionary, NSNumber, NSString, SOJUHeader, SOJUIdentityMischiefMetadata, SOJUMessageBody;

@interface SOJUMischiefUpdateMessageBuilder : NSObject
{
    SOJUMessageBody *_body;
    NSString *_chatMessageId;
    NSDictionary *_savedState;
    NSDictionary *_preservations;
    SOJUHeader *_header;
    NSNumber *_retried;
    NSDictionary *_knownChatSequenceNumbers;
    NSNumber *_mischiefVersion;
    NSNumber *_seqNum;
    NSNumber *_timestamp;
    NSString *_type;
    NSString *_idValue;
    NSString *_mischiefUpdateMessageType;
    NSString *_originParticipantId;
    NSArray *_participantsIds;
    NSString *_sojuNewMischiefName;
    SOJUIdentityMischiefMetadata *_mischiefMetadataResult;
}

+ (id)withJUMischiefUpdateMessage:(id)arg1;
- (void).cxx_destruct;
- (id)setMischiefMetadataResult:(id)arg1;
- (id)setSojuNewMischiefName:(id)arg1;
- (id)setParticipantsIds:(id)arg1;
- (id)setOriginParticipantId:(id)arg1;
- (id)setMischiefUpdateMessageType:(id)arg1;
- (id)setIdValue:(id)arg1;
- (id)setType:(id)arg1;
- (id)setTimestamp:(id)arg1;
- (id)setSeqNum:(id)arg1;
- (id)setMischiefVersion:(id)arg1;
- (id)setKnownChatSequenceNumbers:(id)arg1;
- (id)setRetried:(id)arg1;
- (id)setHeader:(id)arg1;
- (id)setPreservations:(id)arg1;
- (id)setSavedState:(id)arg1;
- (id)setChatMessageId:(id)arg1;
- (id)setBody:(id)arg1;
- (id)build;
- (id)setMischiefUpdateMessageTypeEnum:(long long)arg1;
- (id)setTypeEnum:(long long)arg1;
- (id)setTimestampValue:(long long)arg1;
- (id)setSeqNumValue:(long long)arg1;
- (id)setMischiefVersionValue:(long long)arg1;
- (id)setRetriedValue:(_Bool)arg1;

@end

