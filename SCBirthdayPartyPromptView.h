//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UIButton.h"

@class UILabel;

@interface SCBirthdayPartyPromptView : UIButton
{
    UILabel *_partyPopperLabel;
    UILabel *_promptTitleLabel;
    UILabel *_promptDescriptionLabel;
    UIButton *_dismissButton;
    id <SCBirthdayPartyPromptDelegate> _delegate;
}

+ (long long)getHeightForWidthConstraint:(double)arg1;
@property(nonatomic) __weak id <SCBirthdayPartyPromptDelegate> delegate; // @synthesize delegate=_delegate;
- (void).cxx_destruct;
- (void)_initDismissIcon;
- (void)_initPromptDescriptionLabel;
- (void)_initPromptTitleLabel;
- (void)_initPartyPopperLabel;
- (void)notificationPressed;
- (void)dismissPressed;
- (id)initWithWidth:(double)arg1;

@end

