//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSNumber;

@interface SOJUAdAdPreferencesBuilder : NSObject
{
    NSNumber *_isAudienceMatchOptOut;
}

+ (id)withJUAdAdPreferences:(id)arg1;
- (void).cxx_destruct;
- (id)setIsAudienceMatchOptOut:(id)arg1;
- (id)build;
- (id)setIsAudienceMatchOptOutValue:(_Bool)arg1;

@end

