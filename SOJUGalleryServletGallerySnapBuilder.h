//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSNumber, NSString, SOJUCoordinate, SOJUGallerySnapSource, SOJUStoryFrame;

@interface SOJUGalleryServletGallerySnapBuilder : NSObject
{
    NSString *_snapId;
    NSNumber *_defunct;
    NSString *_mediaId;
    NSString *_encryption;
    NSNumber *_mediaType;
    NSString *_overlay;
    NSNumber *_createTime;
    NSNumber *_orientation;
    NSNumber *_overlayOrientation;
    SOJUCoordinate *_location;
    NSString *_timeZone;
    NSNumber *_temperature;
    NSNumber *_speed;
    NSNumber *_battery;
    NSNumber *_width;
    NSNumber *_height;
    NSNumber *_duration;
    NSNumber *_size;
    NSString *_mediaDownloadUrl;
    NSString *_hdMediaDownloadUrl;
    NSNumber *_hdMediaStatus;
    NSString *_overlayDownloadUrl;
    NSNumber *_hasOverlayImage;
    NSString *_thumbnailDownloadUrl;
    NSNumber *_hasThumbnail;
    NSString *_tags;
    NSNumber *_tagsVersion;
    NSNumber *_cameraHardwareMountingDegrees;
    NSNumber *_cameraFrontFacing;
    SOJUGallerySnapSource *_source;
    SOJUStoryFrame *_framing;
    NSNumber *_statusCode;
    NSNumber *_contentScore;
    NSString *_deviceId;
}

+ (id)withJUGalleryServletGallerySnap:(id)arg1;
- (void).cxx_destruct;
- (id)setDeviceId:(id)arg1;
- (id)setContentScore:(id)arg1;
- (id)setStatusCode:(id)arg1;
- (id)setFraming:(id)arg1;
- (id)setSource:(id)arg1;
- (id)setCameraFrontFacing:(id)arg1;
- (id)setCameraHardwareMountingDegrees:(id)arg1;
- (id)setTagsVersion:(id)arg1;
- (id)setTags:(id)arg1;
- (id)setHasThumbnail:(id)arg1;
- (id)setThumbnailDownloadUrl:(id)arg1;
- (id)setHasOverlayImage:(id)arg1;
- (id)setOverlayDownloadUrl:(id)arg1;
- (id)setHdMediaStatus:(id)arg1;
- (id)setHdMediaDownloadUrl:(id)arg1;
- (id)setMediaDownloadUrl:(id)arg1;
- (id)setSize:(id)arg1;
- (id)setDuration:(id)arg1;
- (id)setHeight:(id)arg1;
- (id)setWidth:(id)arg1;
- (id)setBattery:(id)arg1;
- (id)setSpeed:(id)arg1;
- (id)setTemperature:(id)arg1;
- (id)setTimeZone:(id)arg1;
- (id)setLocation:(id)arg1;
- (id)setOverlayOrientation:(id)arg1;
- (id)setOrientation:(id)arg1;
- (id)setCreateTime:(id)arg1;
- (id)setOverlay:(id)arg1;
- (id)setMediaType:(id)arg1;
- (id)setEncryption:(id)arg1;
- (id)setMediaId:(id)arg1;
- (id)setDefunct:(id)arg1;
- (id)setSnapId:(id)arg1;
- (id)build;
- (id)setContentScoreValue:(double)arg1;
- (id)setStatusCodeEnum:(long long)arg1;
- (id)setStatusCodeValue:(int)arg1;
- (id)setCameraFrontFacingValue:(_Bool)arg1;
- (id)setCameraHardwareMountingDegreesEnum:(long long)arg1;
- (id)setCameraHardwareMountingDegreesValue:(int)arg1;
- (id)setTagsVersionValue:(int)arg1;
- (id)setHasThumbnailValue:(_Bool)arg1;
- (id)setHasOverlayImageValue:(_Bool)arg1;
- (id)setHdMediaStatusEnum:(long long)arg1;
- (id)setHdMediaStatusValue:(int)arg1;
- (id)setSizeValue:(long long)arg1;
- (id)setDurationValue:(double)arg1;
- (id)setHeightValue:(int)arg1;
- (id)setWidthValue:(int)arg1;
- (id)setBatteryValue:(double)arg1;
- (id)setSpeedValue:(double)arg1;
- (id)setTemperatureValue:(double)arg1;
- (id)setOverlayOrientationEnum:(long long)arg1;
- (id)setOverlayOrientationValue:(int)arg1;
- (id)setOrientationEnum:(long long)arg1;
- (id)setOrientationValue:(int)arg1;
- (id)setCreateTimeValue:(long long)arg1;
- (id)setMediaTypeEnum:(long long)arg1;
- (id)setMediaTypeValue:(int)arg1;
- (id)setDefunctValue:(_Bool)arg1;

@end

