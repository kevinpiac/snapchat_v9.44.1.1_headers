//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSNumber, NSString;

@interface SOJUSecurityFideliusUpdatesRequestBuilder : NSObject
{
    NSString *_alpha;
    NSNumber *_lastTimestamp;
}

+ (id)withJUSecurityFideliusUpdatesRequest:(id)arg1;
- (void).cxx_destruct;
- (id)setLastTimestamp:(id)arg1;
- (id)setAlpha:(id)arg1;
- (id)build;
- (id)setLastTimestampValue:(long long)arg1;

@end

