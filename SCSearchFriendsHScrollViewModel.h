//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCSearchCellModel.h"

@class NSDictionary, NSString, SCSearchSession;

@interface SCSearchFriendsHScrollViewModel : NSObject <SCSearchCellModel>
{
    SCSearchSession *_searchSession;
    NSDictionary *_sectionNames;
    NSDictionary *_groupControllers;
}

@property(readonly, nonatomic) NSDictionary *groupControllers; // @synthesize groupControllers=_groupControllers;
@property(readonly, nonatomic) NSDictionary *sectionNames; // @synthesize sectionNames=_sectionNames;
- (void).cxx_destruct;
- (_Bool)shouldDisplay;
- (double)cellHeight;
- (struct CGSize)cellSize;
- (id)initWithSearchSession:(id)arg1 quickAddFriends:(id)arg2 latestAddedFriends:(id)arg3 addressBookFriends:(id)arg4;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

