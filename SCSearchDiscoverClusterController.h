//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UIViewController.h"

#import "SCDiscoverEditionsSessionDelegate.h"
#import "SCSearchDiscoverEditionPresenterDelegate.h"

@class NSString, NSURL, SCSearchDiscoverEditionPresenter, SCSearchNewsClusterViewModel, UIView;

@interface SCSearchDiscoverClusterController : UIViewController <SCSearchDiscoverEditionPresenterDelegate, SCDiscoverEditionsSessionDelegate>
{
    SCSearchDiscoverEditionPresenter *_presenter;
    NSURL *_deeplink;
    SCSearchNewsClusterViewModel *_viewModel;
    long long _currentIndex;
    _Bool _didTapNext;
    UIView *_sourceView;
}

- (void).cxx_destruct;
- (void)currentEditionDidChange:(id)arg1;
- (void)discoverEditionPresenter:(id)arg1 didDismissEdition:(id)arg2 becameFullyViewed:(_Bool)arg3;
- (void)discoverEditionPresenterDidFinishDismissing:(id)arg1;
- (void)discoverEditionPresenterWillBeginAnimatingToDismissEdition:(id)arg1;
- (void)discoverEditionPresenterDidCancelDismissingEdition:(id)arg1;
- (void)discoverEditionPresenterDidBeginDismissingEdition:(id)arg1 transitionAnimator:(id)arg2;
- (void)discoverEditionPresenterDidFinishPresentingEdition:(id)arg1;
- (void)discoverEditionPresenterWillBeginPresentingEdition:(id)arg1 transitionAnimator:(id)arg2;
- (void)discoverEditionPresenterDidFailToPresentEdition:(id)arg1;
- (void)_didSwipeDown:(id)arg1;
- (void)_didTap:(id)arg1;
- (void)navigateToDeeplink:(id)arg1;
- (void)viewDidAppear:(_Bool)arg1;
- (void)setupWithViewModel:(id)arg1 sourceView:(id)arg2;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

