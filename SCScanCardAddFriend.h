//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCScanCardTableViewCell.h"

@class Friend, NSString, SCScanCardButton, SCSnapcodeView, UILabel;

@interface SCScanCardAddFriend : SCScanCardTableViewCell
{
    Friend *_friend;
    _Bool _hasDisplayName;
    SCSnapcodeView *_snapcodeView;
    UILabel *_displayNameLabel;
    UILabel *_usernameLabel;
    SCScanCardButton *_addFriendButton;
    SCScanCardButton *_dismissButton;
    _Bool _isAddingFriend;
    NSString *_scanData;
    UILabel *_emojiLabel;
    NSString *_emojiString;
    _Bool _hasEmoji;
    _Bool _addedWithDiscoverCard;
    _Bool _addedWithUnlockCard;
}

@property(readonly, nonatomic) Friend *friend; // @synthesize friend=_friend;
@property(nonatomic) _Bool addedWithUnlockCard; // @synthesize addedWithUnlockCard=_addedWithUnlockCard;
@property(nonatomic) _Bool addedWithDiscoverCard; // @synthesize addedWithDiscoverCard=_addedWithDiscoverCard;
- (void).cxx_destruct;
- (void)_addFriendDidComplete;
- (void)_didTapCancelButton;
- (void)_didTapAddFriend;
- (void)_didTapSnapcode;
- (void)updateStyle;
- (void)transitionToContentAnimated:(_Bool)arg1;
- (id)initWithFriend:(id)arg1 scanData:(id)arg2 emojiString:(id)arg3;

@end

