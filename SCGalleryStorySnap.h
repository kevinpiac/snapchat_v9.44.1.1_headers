//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCGalleryStorySnap.h"

@class CLLocation, NSDate, NSString, NSURL, SOJUGallerySnapOverlay, SOJUStoryFrame;

@interface SCGalleryStorySnap : NSObject <SCGalleryStorySnap>
{
    unsigned long long _mediaType;
    unsigned long long _source;
    NSURL *_originalMediaURL;
    NSURL *_renderedOverlayURL;
    double _duration;
    NSDate *_createTimeUtc;
    long long _orientation;
    SOJUGallerySnapOverlay *_sojuOverlay;
    SOJUStoryFrame *_framing;
    CLLocation *_location;
}

@property(readonly, copy, nonatomic) CLLocation *location; // @synthesize location=_location;
@property(readonly, copy, nonatomic) SOJUStoryFrame *framing; // @synthesize framing=_framing;
@property(readonly, copy, nonatomic) SOJUGallerySnapOverlay *sojuOverlay; // @synthesize sojuOverlay=_sojuOverlay;
@property(readonly, nonatomic) long long orientation; // @synthesize orientation=_orientation;
@property(readonly, copy, nonatomic) NSDate *createTimeUtc; // @synthesize createTimeUtc=_createTimeUtc;
@property(readonly, nonatomic) double duration; // @synthesize duration=_duration;
@property(readonly, copy, nonatomic) NSURL *renderedOverlayURL; // @synthesize renderedOverlayURL=_renderedOverlayURL;
@property(readonly, copy, nonatomic) NSURL *originalMediaURL; // @synthesize originalMediaURL=_originalMediaURL;
@property(readonly, nonatomic) unsigned long long source; // @synthesize source=_source;
@property(readonly, nonatomic) unsigned long long mediaType; // @synthesize mediaType=_mediaType;
- (void).cxx_destruct;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
- (_Bool)isEqual:(id)arg1;
- (void)encodeWithCoder:(id)arg1;
- (id)initWithCoder:(id)arg1;
- (id)copyWithZone:(struct _NSZone *)arg1;
- (id)initWithMediaType:(unsigned long long)arg1 source:(unsigned long long)arg2 originalMediaURL:(id)arg3 renderedOverlayURL:(id)arg4 duration:(double)arg5 createTimeUtc:(id)arg6 orientation:(long long)arg7 sojuOverlay:(id)arg8 framing:(id)arg9 location:(id)arg10;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly) Class superclass;

@end

