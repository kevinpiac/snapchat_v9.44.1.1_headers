//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSArray, NSIndexPath, NSString;

@protocol SCChatConversationViewModel <NSObject>
@property(readonly, nonatomic) NSArray *messageViewModels;
@property(readonly, nonatomic) NSIndexPath *previewModeFirstBelowTheFoldIndexPath;
@property(readonly, nonatomic) NSIndexPath *firstBelowTheFoldIndexPath;
- (_Bool)canLoadMoreMessagesByRetrying:(_Bool)arg1;
- (NSString *)displayName;
- (NSIndexPath *)lastIndexPath;
- (id <SCChatMessageViewModel>)viewModelAtIndexPath:(NSIndexPath *)arg1;
- (NSString *)conversationId;
@end

