//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCLocationControllerDelegate.h"

@class NSMutableSet, NSString, SCLocationController, SCUserSession;

@interface SCGalleryTakenNearbySearch : NSObject <SCLocationControllerDelegate>
{
    id <SCPerforming> _performer;
    SCUserSession *_userSession;
    SCLocationController *_locationController;
    NSMutableSet *_pendingRequestItems;
}

- (void).cxx_destruct;
- (id)_gallerySnapsFromGallerySnapItems:(id)arg1;
- (id)_gallerySnapItemsSortedByDistance:(id)arg1;
- (id)_gallerySnapItemsTakenNearbyLocation:(id)arg1 withinDistance:(double)arg2 forOwner:(id)arg3;
- (id)_gallerySnapsTakenNearbyLocation:(id)arg1 forOwner:(id)arg2;
- (void)_completePendingRequestItemsWithResult:(id)arg1;
- (void)_completeRequestItem:(id)arg1 withResult:(id)arg2;
- (void)locationController:(id)arg1 didFailWithError:(id)arg2;
- (void)locationController:(id)arg1 didUpdateLocation:(id)arg2;
- (id)getSearchResultTakenNearbyToSnapIds:(id)arg1 withGeoTag:(id)arg2;
- (id)fetchTakenNearbySearchResultWithQueue:(id)arg1 completionHandler:(CDUnknownBlockType)arg2;
- (id)initWithPerformer:(id)arg1 userSession:(id)arg2;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

