//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

@class NSArray, NSString, TCConversationCtx;

@protocol TCTalkCoreDelegate
- (void)requestUIUpdate;
- (void)refreshAuth:(id <TCRefreshAuthResponder>)arg1;
- (TCConversationCtx *)getConversationCtx;
- (void)sendPushNotification:(long long)arg1 payload:(NSString *)arg2;
- (void)broadcastConversationPresences:(NSString *)arg1 presences:(NSArray *)arg2;
@end

