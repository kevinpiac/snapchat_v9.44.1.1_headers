//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSMutableDictionary;

@interface SCStickerTagInvertedIndex : NSObject
{
    NSMutableDictionary *_allMapping;
}

- (void).cxx_destruct;
- (id)fetchOneWord:(id)arg1;
- (void)addOneWord:(id)arg1 index:(int)arg2;
- (id)init;

@end

