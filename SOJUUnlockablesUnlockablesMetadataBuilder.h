//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSArray, NSString;

@interface SOJUUnlockablesUnlockablesMetadataBuilder : NSObject
{
    NSArray *_filters;
    NSArray *_bitmojiFilters;
    NSArray *_lensFilters;
    NSArray *_geoStickerPacks;
    NSString *_encGeocell;
}

+ (id)withJUUnlockablesUnlockablesMetadata:(id)arg1;
- (void).cxx_destruct;
- (id)setEncGeocell:(id)arg1;
- (id)setGeoStickerPacks:(id)arg1;
- (id)setLensFilters:(id)arg1;
- (id)setBitmojiFilters:(id)arg1;
- (id)setFilters:(id)arg1;
- (id)build;

@end

