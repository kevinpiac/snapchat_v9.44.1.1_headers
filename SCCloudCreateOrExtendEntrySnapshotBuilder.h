//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSArray, NSDictionary;

@interface SCCloudCreateOrExtendEntrySnapshotBuilder : NSObject
{
    id <SCGalleryProfile> _profile;
    id <SCGalleryEntry> _entryPlaceholder;
    NSArray *_snapPlaceholders;
    NSArray *_detailPlaceholders;
    NSDictionary *_dataVaultEncryption;
}

+ (id)withCloudCreateOrExtendEntrySnapshot:(id)arg1;
- (void).cxx_destruct;
- (id)setDataVaultEncryption:(id)arg1;
- (id)setDetailPlaceholders:(id)arg1;
- (id)setSnapPlaceholders:(id)arg1;
- (id)setEntryPlaceholder:(id)arg1;
- (id)setProfile:(id)arg1;
- (id)build;

@end

