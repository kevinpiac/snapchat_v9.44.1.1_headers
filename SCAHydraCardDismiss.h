//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCAHydraCardActionBase.h"

#import "NamedEvent.h"

@interface SCAHydraCardDismiss : SCAHydraCardActionBase <NamedEvent>
{
    long long exitEvent;
}

+ (id)copy:(id)arg1;
- (id)asDictionary;
- (long long)getExitEvent;
- (void)setExitEvent:(long long)arg1;
- (id)getEventName;
- (id)init;

@end

