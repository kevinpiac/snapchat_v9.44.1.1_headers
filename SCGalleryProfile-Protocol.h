//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSCoding.h"
#import "NSCopying.h"
#import "NSObject.h"

@class NSString;

@protocol SCGalleryProfile <NSObject, NSCoding, NSCopying>
@property(readonly, copy, nonatomic) NSString *userId;
@property(readonly, nonatomic) _Bool topSecretPrivateGalleryEnabled;
@property(readonly, nonatomic) _Bool swipedIntoMemories;
@property(readonly, nonatomic) _Bool storyAutoSaving;
@property(readonly, nonatomic) int snapTotalQuota;
@property(readonly, nonatomic) int snapSaveOption;
@property(readonly, nonatomic) _Bool saveToPrivateGalleryByDefault;
@property(readonly, nonatomic) _Bool privateGalleryEnabled;
@property(readonly, nonatomic) long long lastUpdateSeqNum;
@property(readonly, nonatomic) _Bool forceSyncRequired;
@property(readonly, nonatomic) int dbVersion;
@property(readonly, nonatomic) _Bool backupOnCellular;
@property(readonly, copy, nonatomic) NSString *objectID;
@end

