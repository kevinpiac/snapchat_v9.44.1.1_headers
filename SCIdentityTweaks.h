//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "FBTweakObserver.h"

@class FBTweak, NSString;

@interface SCIdentityTweaks : NSObject <FBTweakObserver>
{
    FBTweak *_fakeEmojiCount;
    FBTweak *_fakeSuggestedFriendCount;
    FBTweak *_snapcodeCarouselTweak;
    FBTweak *_quickLoginFakeDelay;
    FBTweak *_displayNameResultSize;
}

+ (long long)getRegistrationVerificationTypeExperimentId;
+ (long long)getFilterOnboardingPromptExperimentId;
+ (long long)getQuickAddForSendToLimitExperiementalValue;
+ (long long)getQuickAddForStoriesLimitExperiementalValue;
+ (long long)getLowerLimitFriendsAddedToHideInvites;
+ (long long)getLowerLimitSnapchattersFoundToHideInvites;
+ (id)shared;
- (void).cxx_destruct;
- (void)tweakDidChange:(id)arg1;
- (void)tweaks;
- (unsigned long long)getDisplayNameResultSize;
- (_Bool)isDisplayNameSearchDebugEnabled;
- (_Bool)isLocalDisplayNameSearchEnabled;
- (_Bool)isSearchByDisplayNameEnabled;
- (_Bool)isSnapcodeCarouselEnabled;
- (double)getQuickLoginFakeDelaySeconds;
- (unsigned long long)getFakeSuggestedFriendCount;
- (unsigned long long)getFakeFriendmojiCount;
- (_Bool)isRegistrationV2ShareUsernameEnabled;
- (_Bool)shouldEnableGeofilterPassport;
- (_Bool)isSMSSnapEnabled;
- (_Bool)shouldFakeBirthdayForUsername:(id)arg1;
- (_Bool)isPreLoginAppResetPasswordEnabled;
- (unsigned long long)safariCookieMatchSetting;
- (id)init;
- (_Bool)shouldShowQuickAddChatFeedStories;
- (long long)getNearbyPopTreatment;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

