//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCSavableItemChatTableViewCell.h"

#import "SCActionMenuRenderableCell.h"
#import "SCStackedCollectionViewCellActionDelegate.h"
#import "UICollectionViewDataSource.h"
#import "UICollectionViewDelegate.h"
#import "UICollectionViewDelegateFlowLayout.h"

@class NSMutableArray, NSString, UICollectionView<SCActionMenuRenderableCellContentView>, UICollectionViewFlowLayout, UIView, UIViewController<SCChatCellMessageStateUpdateDelegate>, UIViewController<SCMediaFullScreenViewDelegate><SCChatCellMessageStateUpdateDelegate>;

@interface SCStackedChatTableViewCell : SCSavableItemChatTableViewCell <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, SCStackedCollectionViewCellActionDelegate, SCActionMenuRenderableCell, UICollectionViewDelegate>
{
    UICollectionView<SCActionMenuRenderableCellContentView> *_stackedCollectionView;
    UICollectionViewFlowLayout *_flowLayout;
    NSMutableArray *_timeLabelArray;
    UIView *_timerLabelContainerView;
    UIViewController<SCMediaFullScreenViewDelegate><SCChatCellMessageStateUpdateDelegate> *_parentVC;
    id <SCChatCellNoteGestureDelegate><SCChatFullscreenMediaChatTableCellDelegate> _delegate;
}

@property(readonly, nonatomic) __weak UIViewController<SCChatCellMessageStateUpdateDelegate> *parentVC; // @synthesize parentVC=_parentVC;
- (void).cxx_destruct;
- (void)resetWithOriginalContent;
- (id)actionMenuContentView;
- (void)configureWithActionMenuVC:(id)arg1;
- (_Bool)isSendingOrReceivingAudioOrVideo;
- (void)didFinishPlayingNoteWithMessageId:(id)arg1;
- (void)didShowPendingDisplayForCollectionViewCellForMessageId:(id)arg1;
- (void)didShowCompleteDisplayForCollectionViewCellForMessageId:(id)arg1;
- (void)stopAnimations;
- (void)startAnimations;
- (void)clearContents;
- (struct UIEdgeInsets)collectionView:(id)arg1 layout:(id)arg2 insetForSectionAtIndex:(long long)arg3;
- (struct CGSize)collectionView:(id)arg1 layout:(id)arg2 sizeForItemAtIndexPath:(id)arg3;
- (void)reloadTableViewCellAtIndexPath:(id)arg1;
- (id)collectionView:(id)arg1 cellForItemAtIndexPath:(id)arg2;
- (long long)collectionView:(id)arg1 numberOfItemsInSection:(long long)arg2;
- (void)collectionView:(id)arg1 didSelectItemAtIndexPath:(id)arg2;
- (id)indexPathOfCollectionViewCellForPoint:(struct CGPoint)arg1 ignoreInsideCell:(_Bool)arg2;
- (void)setUpTimeLabelForMessage:(id)arg1 atIndex:(unsigned long long)arg2;
- (void)_resetTimeLabels;
- (void)setViewModel:(id)arg1;
- (void)renderMetadata;
- (void)_setCollectionViewConstraints;
- (void)renderPayload;
- (void)prepareForReuse;
- (void)_initTimerLabelContainerView;
- (id)stackedCollectionView;
- (void)_initCollectionView;
- (id)initWithStyle:(long long)arg1 reuseIdentifier:(id)arg2 parentVC:(id)arg3 delegate:(id)arg4;
- (id)stackedViewModel;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

