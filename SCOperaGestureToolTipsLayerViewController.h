//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCOperaLayerViewController.h"

#import "SCOperaBlockingViewController.h"

@class NSString, SCOperaGestureToolTipsLayer, SCOperaGestureToolTipsLayerView;

@interface SCOperaGestureToolTipsLayerViewController : SCOperaLayerViewController <SCOperaBlockingViewController>
{
    SCOperaGestureToolTipsLayer *_layer;
    SCOperaGestureToolTipsLayerView *_layerView;
    _Bool _isDismissing;
    id <SCOperaBlockingViewControllerDelegate> _blockingViewControllerDelegate;
    id <SCOperaPropertyUpdateModerator> _propertyUpdateModerator;
}

@property(nonatomic) __weak id <SCOperaPropertyUpdateModerator> propertyUpdateModerator; // @synthesize propertyUpdateModerator=_propertyUpdateModerator;
@property(nonatomic) __weak id <SCOperaBlockingViewControllerDelegate> blockingViewControllerDelegate; // @synthesize blockingViewControllerDelegate=_blockingViewControllerDelegate;
- (void).cxx_destruct;
- (void)_didUpdateSubviewsVisible:(_Bool)arg1;
- (long long)pageabilityForRelativePosition:(unsigned long long)arg1;
- (_Bool)isBeingDismissed;
- (void)didTryPagingWhenPagingDisabled;
- (void)viewDidFullyAppear;
- (void)loadView;
- (id)initWithLayer:(id)arg1 configuration:(id)arg2 eventAnnouncer:(id)arg3;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

