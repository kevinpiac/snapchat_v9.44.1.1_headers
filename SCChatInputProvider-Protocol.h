//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSString, SCReplyParameters, UIPanGestureRecognizer, UIScrollView;

@protocol SCChatInputProvider <NSObject>
- (void)handleDraggingDownPanWithGestureRecognizer:(UIPanGestureRecognizer *)arg1;
- (void)drawerScrollViewDidEndDragging:(UIScrollView *)arg1 willDecelerate:(_Bool)arg2;
- (void)drawerScrollViewDidScroll:(UIScrollView *)arg1;
- (void)drawerScrollViewWillBeginDragging:(UIScrollView *)arg1;
- (_Bool)isFromMischief;
- (_Bool)isQuickSnapEnabled;
- (long long)drawerMode;
- (void)requestInputAccessory:(id <SCChatInputAccessory>)arg1 drawerHeight:(double)arg2 animated:(_Bool)arg3;
- (void)detachDrawerForAccessory:(id <SCChatInputAccessory>)arg1;
- (void)attachDrawerForAccessory:(id <SCChatInputAccessory>)arg1;
- (SCReplyParameters *)replyParameters;
- (_Bool)isKeyboardSnapshotVisible;
- (_Bool)isPresentingAccessory:(id <SCChatInputAccessory>)arg1;
- (void)shouldHighlightText;
- (void)clearInputText;
- (NSString *)previousText;
- (NSString *)inputText;
- (NSString *)recipient;
@end

