//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class SCStoryMediaCache;

@interface SCSingleIconConfigurer : NSObject
{
    id <SCCachingMediaRequest> _loadingRequest;
    SCStoryMediaCache *_storyMediaCache;
}

- (void).cxx_destruct;
- (void)_updateIconView:(id)arg1 withLoadState:(long long)arg2;
- (void)_loadThumbnailForIconView:(id)arg1 thumbnailKey:(id)arg2 encryptionDictionary:(id)arg3 encryptor:(id)arg4 expiration:(id)arg5 cacheInfoDataSource:(id)arg6 isStoryMedia:(_Bool)arg7 iconViewTagID:(long long)arg8 thumbnailState:(long long)arg9;
- (void)_loadThumbnailForIconView:(id)arg1 story:(id)arg2 iconViewTagID:(long long)arg3 thumbnailState:(long long)arg4;
- (void)_loadIconMediaForIconView:(id)arg1 friendStories:(id)arg2 ID:(long long)arg3;
- (void)_loadIconMediaForIconView:(id)arg1 discoverChannel:(id)arg2 ID:(long long)arg3;
- (void)configureSingleIconView:(id)arg1 withFriend:(id)arg2;
- (void)configureSingleIconView:(id)arg1 withDiscoverChannel:(id)arg2;
- (void)configureSingleIconView:(id)arg1 withFriendStories:(id)arg2 viewingType:(long long)arg3;
- (void)configureSingleIconView:(id)arg1 withMyStory:(id)arg2;
- (void)prepareForReuse;
- (void)_preConfigureWithSingleIconView:(id)arg1 ID:(unsigned long long)arg2;
- (long long)_loadStateWithDiscoverChannel:(id)arg1;
- (long long)_loadStateWithMyStory:(id)arg1;
- (long long)_loadStateWithFriendStories:(id)arg1 viewingType:(long long)arg2;
- (id)init;

@end

