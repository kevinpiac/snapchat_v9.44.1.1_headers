//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSString, SCCDGalleryProfile, SCObjectPlaceholder;

@interface SCGalleryProfileChangeRequest : NSObject
{
    SCCDGalleryProfile *_galleryProfile;
    SCObjectPlaceholder *_objectPlaceholder;
}

+ (void)deleteGalleryProfiles:(id)arg1;
+ (id)creationRequestWithGalleryProfile:(id)arg1;
+ (id)changeRequestForGalleryProfile:(id)arg1;
- (void).cxx_destruct;
@property(copy, nonatomic) NSString *userId;
@property(nonatomic) _Bool topSecretPrivateGalleryEnabled;
@property(nonatomic) _Bool swipedIntoMemories;
@property(nonatomic) _Bool storyAutoSaving;
@property(nonatomic) int snapTotalQuota;
@property(nonatomic) int snapSaveOption;
@property(nonatomic) _Bool saveToPrivateGalleryByDefault;
@property(nonatomic) _Bool privateGalleryEnabled;
@property(nonatomic) long long lastUpdateSeqNum;
@property(nonatomic) _Bool forceSyncRequired;
@property(nonatomic) int dbVersion;
@property(nonatomic) _Bool backupOnCellular;
- (void)setWithGalleryProfile:(id)arg1;
@property(readonly, copy, nonatomic) NSString *objectID;
@property(readonly, nonatomic) SCObjectPlaceholder *placeholderForCreatedGalleryProfile;
- (void)setUserDefaults:(id)arg1;
- (void)removeSnaps:(id)arg1;
- (void)addSnaps:(id)arg1;
- (void)setQuotaStatus:(id)arg1;
- (void)removeOperations:(id)arg1;
- (void)addOperations:(id)arg1;
- (void)removeFailedEntries:(id)arg1;
- (void)addFailedEntries:(id)arg1;
- (void)removeEntries:(id)arg1;
- (void)addEntries:(id)arg1;
- (void)removeDeletedSnaps:(id)arg1;
- (void)addDeletedSnaps:(id)arg1;
- (void)removeDeletedEntries:(id)arg1;
- (void)addDeletedEntries:(id)arg1;
- (id)initWithGalleryProfile:(id)arg1;

@end

