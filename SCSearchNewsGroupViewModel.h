//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCSearchCellModel.h"

@class NSArray, NSNumber, NSString;

@interface SCSearchNewsGroupViewModel : NSObject <SCSearchCellModel>
{
    NSArray *_viewModels;
    NSNumber *_maxClusterId;
    NSNumber *_minClusterId;
}

@property(retain, nonatomic) NSNumber *minClusterId; // @synthesize minClusterId=_minClusterId;
@property(retain, nonatomic) NSNumber *maxClusterId; // @synthesize maxClusterId=_maxClusterId;
@property(retain, nonatomic) NSArray *viewModels; // @synthesize viewModels=_viewModels;
- (void).cxx_destruct;
- (_Bool)shouldDisplay;
- (struct CGSize)cellSize;
- (id)initWithViewModels:(id)arg1;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

