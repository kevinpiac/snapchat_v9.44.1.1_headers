//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCSearchGenericCellView.h"

#import "SCAddFriendButtonV2Delegate.h"

@class Friend, NSString, SCAddFriendButtonV2, UIImageView, UILabel, UIView;

@interface SCSearchPersonEntityCell : SCSearchGenericCellView <SCAddFriendButtonV2Delegate>
{
    UIImageView *_thumbnailImageView;
    UILabel *_titleLabel;
    UILabel *_descriptionLabel;
    UILabel *_headerLabel;
    UILabel *_wikiLabel;
    UIView *_buttonContainer;
    SCAddFriendButtonV2 *_button;
    Friend *_aFriend;
}

+ (id)reuseIdentifier;
- (void).cxx_destruct;
- (_Bool)searchGenericCellViewShouldChangeBackgroundColorOnHighlight;
- (void)buttonV2Pressed:(id)arg1 friend:(id)arg2;
- (void)updateButtonWithState:(long long)arg1 friend:(id)arg2 style:(long long)arg3;
- (id)wikiLabel;
- (id)headerLabel;
- (id)descriptionLabel;
- (id)titleLabel;
- (id)thumbnailImageView;
- (void)makeCardViewConstraints;
- (void)setupWithViewModel:(id)arg1;
- (id)initWithFrame:(struct CGRect)arg1;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

