//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SOJUAuthHumpbackGetAllPasswordHashesResponse.h"

@class NSNumber, NSString;

@interface SOJUAuthHumpbackGetAllPasswordHashesResponse : NSObject <SOJUAuthHumpbackGetAllPasswordHashesResponse>
{
    NSString *_passwordHash;
    NSNumber *_passwordVersion;
    NSString *_oldPasswordHash;
    NSNumber *_oldPasswordVersion;
    NSString *_status;
}

@property(readonly, copy, nonatomic) NSString *status; // @synthesize status=_status;
@property(readonly, copy, nonatomic) NSNumber *oldPasswordVersion; // @synthesize oldPasswordVersion=_oldPasswordVersion;
@property(readonly, copy, nonatomic) NSString *oldPasswordHash; // @synthesize oldPasswordHash=_oldPasswordHash;
@property(readonly, copy, nonatomic) NSNumber *passwordVersion; // @synthesize passwordVersion=_passwordVersion;
@property(readonly, copy, nonatomic) NSString *passwordHash; // @synthesize passwordHash=_passwordHash;
- (void).cxx_destruct;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
- (_Bool)isEqual:(id)arg1;
- (void)encodeWithCoder:(id)arg1;
- (id)initWithCoder:(id)arg1;
- (id)copyWithZone:(struct _NSZone *)arg1;
- (id)initWithPasswordHash:(id)arg1 passwordVersion:(id)arg2 oldPasswordHash:(id)arg3 oldPasswordVersion:(id)arg4 status:(id)arg5;
- (id)toJson;
- (id)toDictionary;
- (id)initWithJSONDictionary:(id)arg1;
- (long long)statusEnum;
- (long long)oldPasswordVersionValue;
- (long long)passwordVersionValue;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly) Class superclass;

@end

