//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCGalleryItemQuickActionViewController.h"

@class PHAsset, SCPlayerView, SCSentinel;

@interface SCGalleryItemQuickActionCameraRollViewController : SCGalleryItemQuickActionViewController
{
    SCSentinel *_sentinel;
    SCPlayerView *_playerView;
    PHAsset *_cameraRollAsset;
}

- (void).cxx_destruct;
- (void)dealloc;
- (void)editItem;
- (void)promptToConfirmAndSetItemToPrivate;
- (void)deleteItem;
- (void)makeItemPrivate;
- (void)_playerItemDidPlayToEndTime:(id)arg1;
- (void)updateUIAccordingContent;
- (double)_contentAspectRatio;
- (void)_fillDeleteMenuRow:(id)arg1;
- (void)_fillShareMenuRow:(id)arg1;
- (void)_fillEditMenuRow:(id)arg1;
- (void)fillMenuItems;
- (id)presentationItem;
- (id)entryOrAsset;
- (id)initWithUserSession:(id)arg1 item:(id)arg2 initialIndex:(unsigned long long)arg3;

@end

