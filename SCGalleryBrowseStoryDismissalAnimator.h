//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCGalleryBrowseViewDismissalAnimator.h"

@class NSString, SCPullToDismissEffectHandler, UICollectionView, UICollectionViewCell<SCGalleryEntryViewCellProtocol>, UIPanGestureRecognizer, UIView;

@interface SCGalleryBrowseStoryDismissalAnimator : NSObject <SCGalleryBrowseViewDismissalAnimator>
{
    UICollectionView *_collectionView;
    UIPanGestureRecognizer *_panGestureRecognizer;
    UIView *_blackView;
    UICollectionViewCell<SCGalleryEntryViewCellProtocol> *_destinationCell;
    _Bool _isStarted;
    id <UIViewControllerContextTransitioning> _transitionContext;
    UIView *_transitionToView;
    UIView *_transitionFromView;
    SCPullToDismissEffectHandler *_storyEffectHandler;
}

- (void).cxx_destruct;
- (void)_dismiss;
- (void)_asyncCancelTransition;
- (void)_cancelTransition;
- (double)_ratio:(double)arg1;
- (id)_destinationImage;
- (struct CGRect)_destinationRectForView:(id)arg1;
- (void)_scrollToItemAtIndexPath:(id)arg1;
- (_Bool)finishPan:(id)arg1;
- (void)didPan:(id)arg1;
- (void)startInteractiveTransition:(id)arg1;
- (void)animateTransition:(id)arg1;
- (double)transitionDuration:(id)arg1;
- (id)initWithCollectionView:(id)arg1 indexPath:(id)arg2 panGestureRecognizer:(id)arg3;

// Remaining properties
@property(readonly, nonatomic) long long completionCurve;
@property(readonly, nonatomic) double completionSpeed;
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;
@property(readonly, nonatomic) _Bool wantsInteractiveStart;

@end

