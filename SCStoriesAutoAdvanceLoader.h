//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSOrderedSet, SCExperimentManager, SCStoryLoader;

@interface SCStoriesAutoAdvanceLoader : NSObject
{
    id <SCStoriesAutoAdvanceDataSource> _friendStoriesDataSource;
    SCExperimentManager *_experimentManager;
    NSOrderedSet *_kvoUserNamesOrderedSet;
    SCStoryLoader *__storyLoader;
}

@property(retain, nonatomic) SCStoryLoader *_storyLoader; // @synthesize _storyLoader=__storyLoader;
@property(retain, nonatomic) NSOrderedSet *kvoUserNamesOrderedSet; // @synthesize kvoUserNamesOrderedSet=_kvoUserNamesOrderedSet;
- (void).cxx_destruct;
- (id)initWithDataSource:(id)arg1 experimentManager:(id)arg2;
- (void)setRequestManagerContextWithFriendStories:(id)arg1 nextStoriesKVOUserNamesList:(id)arg2 viewingType:(long long)arg3;
- (void)loadStory:(id)arg1 viewingType:(long long)arg2 isInStoryPlaylistMode:(_Bool)arg3;

@end

