//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSObject<OS_dispatch_queue>, SCUserSession;

@interface SCGalleryDataMutator : NSObject
{
    NSObject<OS_dispatch_queue> *_storeQueue;
    id <SCPerforming> _storeQueuePerformer;
    SCUserSession *_userSession;
}

- (void).cxx_destruct;
- (void)_cleanupFailedEntry:(id)arg1 snaps:(id)arg2 completionHandler:(CDUnknownBlockType)arg3;
- (void)_createGalleryQuotaStatusForProfileIfNoneExists:(id)arg1;
- (id)_fetchEligibleLatestAutosavedEntryIsPrivate:(_Bool)arg1;
- (id)_placeholderForPhotoAsset:(id)arg1 mediaURL:(id)arg2 orientation:(id)arg3 isPrivate:(_Bool)arg4 dataVaultEncryption:(id)arg5 mutationInfo:(id)arg6;
- (id)_duplicateSnap:(id)arg1 cloudFileContainer:(id)arg2;
- (void)appendToDayStory:(id)arg1 snapPlaceholders:(id)arg2 completionHandler:(CDUnknownBlockType)arg3;
- (id)_generateEncryptedDayStorySnapFromPlaceholder:(id)arg1 dataVaultEncryption:(id)arg2 mutationInfo:(id)arg3;
- (void)deleteFailedEntry:(id)arg1;
- (void)retryFailedEntry:(id)arg1 cloudFileContainers:(id)arg2 completionHandler:(CDUnknownBlockType)arg3;
- (void)highlightSnapIds:(id)arg1 forEntry:(id)arg2 completionHandler:(CDUnknownBlockType)arg3;
- (void)detachSnap:(id)arg1 cloudFileContainer:(id)arg2 fromEntry:(id)arg3 completionHandler:(CDUnknownBlockType)arg4;
- (void)deleteSnap:(id)arg1 fromEntry:(id)arg2 completionHandler:(CDUnknownBlockType)arg3;
- (void)deleteEntries:(id)arg1 completionHandler:(CDUnknownBlockType)arg2;
- (void)updateEntries:(id)arg1 cloudFileContainers:(id)arg2 isPrivate:(_Bool)arg3 completionHandler:(CDUnknownBlockType)arg4;
- (void)updateEntry:(id)arg1 cloudFileContainers:(id)arg2 addingSnaps:(id)arg3 addingPhotoAssets:(id)arg4 photoAssetMediaURLs:(id)arg5 photoAssetOrientations:(id)arg6 completionHandler:(CDUnknownBlockType)arg7;
- (void)updateEntry:(id)arg1 title:(id)arg2 completionHandler:(CDUnknownBlockType)arg3;
- (void)_addStoryWithEntryId:(id)arg1 entryType:(unsigned long long)arg2 snapPlaceholders:(id)arg3 snapDetails:(id)arg4 isPrivate:(_Bool)arg5 dataVaultEncryption:(id)arg6 completionHandler:(CDUnknownBlockType)arg7;
- (void)addStoryWithStorySnaps:(id)arg1 isPrivate:(_Bool)arg2 completionHandler:(CDUnknownBlockType)arg3;
- (void)addStoryWithSnaps:(id)arg1 cloudFileContainers:(id)arg2 photoAssets:(id)arg3 photoAssetMediaURLs:(id)arg4 photoAssetOrientations:(id)arg5 isPrivate:(_Bool)arg6 completionHandler:(CDUnknownBlockType)arg7;
- (void)replacePhoto:(id)arg1 cloudFileContainer:(id)arg2 entry:(id)arg3 duration:(double)arg4 overlayFormat:(id)arg5 overlay:(id)arg6 completionHandler:(CDUnknownBlockType)arg7;
- (void)addAutosavedVideoProvider:(id)arg1 mediaType:(unsigned long long)arg2 orientation:(long long)arg3 overlayFormat:(id)arg4 overlay:(id)arg5 location:(id)arg6 isPrivate:(_Bool)arg7 completionHandler:(CDUnknownBlockType)arg8;
- (void)addAutosavedPhoto:(id)arg1 orientation:(long long)arg2 duration:(double)arg3 overlayFormat:(id)arg4 overlay:(id)arg5 location:(id)arg6 isPrivate:(_Bool)arg7 completionHandler:(CDUnknownBlockType)arg8;
- (void)addPhoto:(id)arg1 source:(unsigned long long)arg2 cameraRollId:(id)arg3 attribution:(id)arg4 framing:(id)arg5 createTimeUtc:(id)arg6 orientation:(long long)arg7 duration:(double)arg8 overlayFormat:(id)arg9 overlay:(id)arg10 location:(id)arg11 isPrivate:(_Bool)arg12 completionHandler:(CDUnknownBlockType)arg13;
- (void)_addPhoto:(id)arg1 source:(unsigned long long)arg2 cameraRollId:(id)arg3 attribution:(id)arg4 framing:(id)arg5 autosaveTimeUtc:(id)arg6 createTimeUtc:(id)arg7 orientation:(long long)arg8 duration:(double)arg9 overlayFormat:(id)arg10 overlay:(id)arg11 location:(id)arg12 isPrivate:(_Bool)arg13 autosave:(_Bool)arg14 completionHandler:(CDUnknownBlockType)arg15;
- (id)_generateEncryptedPhotoSnap:(id)arg1 overlayFormat:(id)arg2 source:(unsigned long long)arg3 cameraRollId:(id)arg4 attribution:(id)arg5 framing:(id)arg6 createTimeUtc:(id)arg7 orientation:(long long)arg8 duration:(double)arg9 location:(id)arg10 isPrivate:(_Bool)arg11 dataVaultEncryption:(id)arg12 mutationInfo:(id)arg13;
- (void)replaceVideo:(id)arg1 cloudFileContainer:(id)arg2 entry:(id)arg3 overlayFormat:(id)arg4 overlay:(id)arg5 completionHandler:(CDUnknownBlockType)arg6;
- (void)_addVideo:(id)arg1 mediaType:(unsigned long long)arg2 source:(unsigned long long)arg3 cameraRollId:(id)arg4 attribution:(id)arg5 framing:(id)arg6 autosaveTimeUtc:(id)arg7 createTimeUtc:(id)arg8 orientation:(long long)arg9 overlayFormat:(id)arg10 overlay:(id)arg11 location:(id)arg12 isPrivate:(_Bool)arg13 autosave:(_Bool)arg14 completionHandler:(CDUnknownBlockType)arg15;
- (id)_generateEncryptedVideoSnap:(id)arg1 mediaType:(unsigned long long)arg2 overlayFormat:(id)arg3 timeScale:(double)arg4 source:(unsigned long long)arg5 cameraRollId:(id)arg6 attribution:(id)arg7 framing:(id)arg8 createTimeUtc:(id)arg9 orientation:(long long)arg10 location:(id)arg11 isPrivate:(_Bool)arg12 dataVaultEncryption:(id)arg13 mutationInfo:(id)arg14;
- (void)addVideoData:(id)arg1 mediaType:(unsigned long long)arg2 source:(unsigned long long)arg3 cameraRollId:(id)arg4 attribution:(id)arg5 framing:(id)arg6 createTimeUtc:(id)arg7 orientation:(long long)arg8 overlayFormat:(id)arg9 overlay:(id)arg10 location:(id)arg11 isPrivate:(_Bool)arg12 completionHandler:(CDUnknownBlockType)arg13;
- (void)addVideoProvider:(id)arg1 mediaType:(unsigned long long)arg2 source:(unsigned long long)arg3 cameraRollId:(id)arg4 attribution:(id)arg5 framing:(id)arg6 createTimeUtc:(id)arg7 orientation:(long long)arg8 overlayFormat:(id)arg9 overlay:(id)arg10 location:(id)arg11 isPrivate:(_Bool)arg12 completionHandler:(CDUnknownBlockType)arg13;
- (_Bool)_needsForceUpdateForEntry:(id)arg1 updatedEntry:(id)arg2;
- (_Bool)_isQuotaWithinLimitForEntry:(long long)arg1 completionHandler:(CDUnknownBlockType)arg2 mutationInfo:(id)arg3;
- (_Bool)_isQuotaWithinLimitForSnap:(CDUnknownBlockType)arg1 mutationInfo:(id)arg2;
- (_Bool)_isQuotaWithinLimitForSnap:(CDUnknownBlockType)arg1 addedSnaps:(long long)arg2 mutationInfo:(id)arg3;
- (_Bool)_isQuotaWithinLimitForAddedSnaps:(long long)arg1 mutationInfo:(id)arg2;
- (int)_snapTotalQuota;
- (id)initWithUserSession:(id)arg1;

@end

