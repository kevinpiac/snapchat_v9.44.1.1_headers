//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCOperaLayerViewController.h"

@class SCOperaTextLayer, SCOperaTextLayerView;

@interface SCOperaTextLayerViewController : SCOperaLayerViewController
{
    SCOperaTextLayer *_layer;
    SCOperaTextLayerView *_layerView;
}

- (void).cxx_destruct;
- (id)image;
- (void)loadView;
- (id)shareableMedia;
- (void)viewDidFullyAppear;
- (id)initWithLayer:(id)arg1 configuration:(id)arg2 eventAnnouncer:(id)arg3;

@end

