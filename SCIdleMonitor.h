//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class MainViewController, NSMutableArray;

@interface SCIdleMonitor : NSObject
{
    CDUnknownBlockType _afterBlock;
    CDUnknownBlockType _prioritizedStartBlock;
    NSMutableArray *_idleQueue;
    NSMutableArray *_blockingQueue;
    NSMutableArray *_prioritizedQueue;
    MainViewController *_mainViewController;
    struct {
        unsigned int touchEventEnded:1;
        unsigned int pendingIdleOps:1;
        unsigned int startWillEnd:1;
        unsigned int startEnded:1;
    } _signal;
    _Bool _attempting;
}

+ (id)sharedInstance;
- (void).cxx_destruct;
- (void)_attemptExecuteOps;
- (void)_pushForFutureAttemptWithInterval:(double)arg1;
- (void)_cancelFutureAttempt;
- (void)_cancelFuturePrioritizedStart;
- (void)_completePrioritizedStart;
- (void)_unlockMainViewControllerSwipe;
- (void)_pushForFuturePrioritizedStart;
- (void)_waitUntilStartCompleteWithQueue:(id)arg1 tag:(id)arg2 callbackQueue:(id)arg3 block:(CDUnknownBlockType)arg4;
- (void)unmarkStartComplete;
- (void)markStartComplete;
- (void)markMainViewControllerLoaded:(id)arg1;
- (void)receiveTouchEvent:(id)arg1;
- (void)waitUntilStartCompleteButBlockSwipeForTag:(id)arg1 callbackQueue:(id)arg2 block:(CDUnknownBlockType)arg3;
- (void)waitUntilStartCompleteForTag:(id)arg1 callbackQueue:(id)arg2 block:(CDUnknownBlockType)arg3;
- (void)waitUntilIdleForTag:(id)arg1 callbackQueue:(id)arg2 block:(CDUnknownBlockType)arg3;
- (id)init;

@end

