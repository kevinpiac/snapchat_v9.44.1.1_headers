//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSNumber;

@interface SOJUSecurityConversationScoreSignalBuilder : NSObject
{
    NSNumber *_interactionScore;
    NSNumber *_numSnapsSent;
    NSNumber *_weightedScore;
    NSNumber *_streakCount;
}

+ (id)withJUSecurityConversationScoreSignal:(id)arg1;
- (void).cxx_destruct;
- (id)setStreakCount:(id)arg1;
- (id)setWeightedScore:(id)arg1;
- (id)setNumSnapsSent:(id)arg1;
- (id)setInteractionScore:(id)arg1;
- (id)build;
- (id)setStreakCountValue:(long long)arg1;
- (id)setWeightedScoreValue:(long long)arg1;
- (id)setNumSnapsSentValue:(long long)arg1;
- (id)setInteractionScoreValue:(long long)arg1;

@end

