//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCADirectSnapPreviewBase.h"

#import "NamedEvent.h"

@class NSString;

@interface SCADirectSnapPreview : SCADirectSnapPreviewBase <NamedEvent>
{
    NSString *filterLensId;
    NSString *lensOptionId;
    NSString *snapSessionId;
    NSString *lensSessionId;
}

+ (id)copy:(id)arg1;
- (void).cxx_destruct;
- (id)asDictionary;
- (id)getLensSessionId;
- (void)setLensSessionId:(id)arg1;
- (id)getSnapSessionId;
- (void)setSnapSessionId:(id)arg1;
- (id)getLensOptionId;
- (void)setLensOptionId:(id)arg1;
- (id)getFilterLensId;
- (void)setFilterLensId:(id)arg1;
- (id)getEventName;
- (id)init;

@end

