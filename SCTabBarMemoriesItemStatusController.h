//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCLagunaAppStatusListener.h"
#import "SCTimeProfilable.h"

@class NSString, SCTabBarMemoriesItem, SCUserSession;

@interface SCTabBarMemoriesItemStatusController : NSObject <SCTimeProfilable, SCLagunaAppStatusListener>
{
    SCTabBarMemoriesItem *_memoriesItem;
    SCUserSession *_userSession;
    long long _currentLagunaState;
}

+ (long long)context;
+ (id)profiledSelectorNames;
@property(nonatomic) long long currentLagunaState; // @synthesize currentLagunaState=_currentLagunaState;
@property(retain, nonatomic) SCUserSession *userSession; // @synthesize userSession=_userSession;
@property(retain, nonatomic) SCTabBarMemoriesItem *memoriesItem; // @synthesize memoriesItem=_memoriesItem;
- (void).cxx_destruct;
- (void)statusCoordinator:(id)arg1 needsToUpdateStateForDevice:(id)arg2;
- (void)statusCoordinatorNumberOfDevicesUpdated:(id)arg1;
- (void)statusCoordinatorBluetoothTurnedOff:(id)arg1;
- (void)statusCoordinatorBluetoothTurnedOn:(id)arg1;
- (unsigned long long)memoriesStateForLagunaState:(long long)arg1;
- (void)_lagunaStatusDidUpdateForDevice:(id)arg1;
- (void)dealloc;
- (void)observeLagunaAppStatusChanges;
- (id)initWithMemoriesItem:(id)arg1 userSession:(id)arg2;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

