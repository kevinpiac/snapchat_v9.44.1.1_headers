//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UIColor.h"

@interface UIColor (Edit)
+ (id)sc_EditTimelineIndicatorGray;
+ (id)sc_EditTimelineBackgroundGray;
+ (id)sc_EditHeaderSecondaryTitleGray;
+ (id)sc_EditHeaderMainTitleLightGray;
+ (id)sc_EditActionButtonBlue;
+ (id)sc_EditSeparatorBlack;
+ (id)sc_EditMenuItemHightlightSemitransparentOverlayBlack;
+ (id)sc_EditHeaderBlack;
@end

