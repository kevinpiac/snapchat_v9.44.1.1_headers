//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCMediaCardViewModel.h"

@class NSString;

@interface SCAddressMediaCardViewModel : SCMediaCardViewModel
{
    NSString *_address;
}

@property(readonly, copy, nonatomic) NSString *address; // @synthesize address=_address;
- (void).cxx_destruct;
- (_Bool)isCircularThumbnail;
- (id)defaultThumbnailImage;
- (long long)type;
- (id)initWithAddressMediaCardContent:(id)arg1 isGrayScale:(_Bool)arg2 shouldActOnGesture:(_Bool)arg3;
- (id)initWithSummary:(id)arg1 isGrayScale:(_Bool)arg2 shouldActOnGesture:(_Bool)arg3;

@end

