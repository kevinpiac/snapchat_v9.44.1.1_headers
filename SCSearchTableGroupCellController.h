//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCSearchCellController.h"

#import "SCSearchGroupCellController.h"
#import "SCTimeProfilable.h"

@class NSArray, NSString, SCSearchTableGroupCell;

@interface SCSearchTableGroupCellController : SCSearchCellController <SCTimeProfilable, SCSearchGroupCellController>
{
    SCSearchTableGroupCell *_cellView;
    long long _collapsedViewCellCount;
    _Bool _isExpanded;
    _Bool _scrollEnabled;
    NSArray *_totalCellControllers;
    NSArray *_visibleCellControllers;
}

+ (long long)context;
+ (id)profiledSelectorNames;
@property(nonatomic) _Bool scrollEnabled; // @synthesize scrollEnabled=_scrollEnabled;
@property(retain, nonatomic) NSArray *visibleCellControllers; // @synthesize visibleCellControllers=_visibleCellControllers;
@property(retain, nonatomic) NSArray *totalCellControllers; // @synthesize totalCellControllers=_totalCellControllers;
- (void).cxx_destruct;
- (id)excludedProfiledSelectorNames;
- (void)searchGroupCellDidTapViewMore:(id)arg1;
- (_Bool)shouldDisplay;
- (double)cellHeight;
- (struct CGSize)cellSize;
- (id)reuseIdentifier;
- (void)handleIntent:(id)arg1;
- (void)setupCellView:(id)arg1;
- (void)setDelegate:(id)arg1;
- (id)initWithCellControllers:(id)arg1 numVisible:(long long)arg2;
- (id)initWithCellControllers:(id)arg1;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

