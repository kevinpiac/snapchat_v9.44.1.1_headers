//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCStoriesSelectableCell.h"

#import "UIGestureRecognizerDelegate.h"

@class Friend, FriendStories, NSString, SCExpandedButton, SCFriendRectangleChatButton, SCFriendmojiView, SCMyStories, SCPieSliceView, SCSingleIconConfigurer, SCStoryIconView, SCStorySaveButton, UIButton, UIImageView, UILabel, UIView;

@interface StoriesCell : SCStoriesSelectableCell <UIGestureRecognizerDelegate>
{
    Friend *_curFriend;
    SCStoryIconView *_thumbnailView;
    SCSingleIconConfigurer *_singleIconConfigurer;
    NSString *_subText;
    NSString *_alternativeSubText;
    _Bool _tapToReplyMode;
    _Bool _needsTopBorder;
    _Bool _needsBottomBorder;
    _Bool _needsRightEdgeInset;
    _Bool _animatingShowSubView;
    _Bool _animatingHideSubView;
    _Bool _myStoriesExpanded;
    id <StoriesCellDelegate> _delegate;
    id <SCStoriesCellTextRotationDataSource> _textRotationDataSource;
    long long _cellType;
    long long _previousCellType;
    SCMyStories *_myStories;
    FriendStories *_friendStories;
    FriendStories *_previousStories;
    UILabel *_nameLabel;
    SCExpandedButton *_expandMyStoryButton;
    SCExpandedButton *_myStorySettingButton;
    SCExpandedButton *_addToStoryButton;
    UIButton *_replyButton;
    SCFriendmojiView *_friendMojiView;
    SCStorySaveButton *_saveButton;
    UIButton *_mySharedStoryInfoButton;
    UILabel *_subLabel;
    UIView *_topBorder;
    UIView *_bottomBorder;
    SCPieSliceView *_pieSliceView;
    UIView *_subView;
    UIImageView *_cameraReplyIcon;
    SCFriendRectangleChatButton *_chatButton;
    UILabel *_chatText;
    UIView *_separateLine;
}

@property(retain, nonatomic) UIView *separateLine; // @synthesize separateLine=_separateLine;
@property(retain, nonatomic) UILabel *chatText; // @synthesize chatText=_chatText;
@property(retain, nonatomic) SCFriendRectangleChatButton *chatButton; // @synthesize chatButton=_chatButton;
@property(retain, nonatomic) UIImageView *cameraReplyIcon; // @synthesize cameraReplyIcon=_cameraReplyIcon;
@property(retain, nonatomic) UIView *subView; // @synthesize subView=_subView;
@property(retain, nonatomic) SCPieSliceView *pieSliceView; // @synthesize pieSliceView=_pieSliceView;
@property(retain, nonatomic) UIView *bottomBorder; // @synthesize bottomBorder=_bottomBorder;
@property(retain, nonatomic) UIView *topBorder; // @synthesize topBorder=_topBorder;
@property(nonatomic) _Bool myStoriesExpanded; // @synthesize myStoriesExpanded=_myStoriesExpanded;
@property(retain, nonatomic) UILabel *subLabel; // @synthesize subLabel=_subLabel;
@property(retain, nonatomic) UIButton *mySharedStoryInfoButton; // @synthesize mySharedStoryInfoButton=_mySharedStoryInfoButton;
@property(retain, nonatomic) SCStorySaveButton *saveButton; // @synthesize saveButton=_saveButton;
@property(retain, nonatomic) SCFriendmojiView *friendMojiView; // @synthesize friendMojiView=_friendMojiView;
@property(retain, nonatomic) UIButton *replyButton; // @synthesize replyButton=_replyButton;
@property(retain, nonatomic) SCExpandedButton *addToStoryButton; // @synthesize addToStoryButton=_addToStoryButton;
@property(retain, nonatomic) SCExpandedButton *myStorySettingButton; // @synthesize myStorySettingButton=_myStorySettingButton;
@property(retain, nonatomic) SCExpandedButton *expandMyStoryButton; // @synthesize expandMyStoryButton=_expandMyStoryButton;
@property(retain, nonatomic) UILabel *nameLabel; // @synthesize nameLabel=_nameLabel;
@property(retain, nonatomic) FriendStories *previousStories; // @synthesize previousStories=_previousStories;
@property(retain, nonatomic) FriendStories *friendStories; // @synthesize friendStories=_friendStories;
@property(retain, nonatomic) SCMyStories *myStories; // @synthesize myStories=_myStories;
@property(nonatomic) long long previousCellType; // @synthesize previousCellType=_previousCellType;
@property(nonatomic) long long cellType; // @synthesize cellType=_cellType;
@property(nonatomic) _Bool animatingHideSubView; // @synthesize animatingHideSubView=_animatingHideSubView;
@property(nonatomic) _Bool animatingShowSubView; // @synthesize animatingShowSubView=_animatingShowSubView;
@property(nonatomic) _Bool needsRightEdgeInset; // @synthesize needsRightEdgeInset=_needsRightEdgeInset;
@property(nonatomic) _Bool needsBottomBorder; // @synthesize needsBottomBorder=_needsBottomBorder;
@property(nonatomic) _Bool needsTopBorder; // @synthesize needsTopBorder=_needsTopBorder;
@property(nonatomic) __weak id <SCStoriesCellTextRotationDataSource> textRotationDataSource; // @synthesize textRotationDataSource=_textRotationDataSource;
@property(nonatomic) __weak id <StoriesCellDelegate> delegate; // @synthesize delegate=_delegate;
- (void).cxx_destruct;
- (void)setPressedState:(_Bool)arg1;
- (long long)loadStateForFriendStories:(id)arg1 cellType:(long long)arg2;
- (id)thumbnailLayer;
- (id)thumbnailIcon;
- (void)_chatButtonPressed;
- (void)mySharedStoryInfoPressed;
- (void)savePressed;
- (void)addToStoryButtonPressed;
- (void)myStorySettingButtonPressed;
- (void)expandMyStoryButtonPressed;
- (void)replyButtonPressed;
- (void)_updateSubLabelWithSubText:(id)arg1 alternativeSubText:(id)arg2 useAlternative:(_Bool)arg3;
- (void)_updateSubLabel:(id)arg1;
- (void)saveStorySucceeded:(_Bool)arg1;
- (void)updateSaveStoryProgress:(double)arg1;
- (void)_updateStatusTextForFriendStoriesCellWithFriendStories:(id)arg1 forViewingType:(long long)arg2;
- (void)toggleSelectedThumbnail:(_Bool)arg1 animated:(_Bool)arg2;
- (void)toggleRightOffset:(_Bool)arg1;
- (void)_updateSubviewsForFriend;
- (void)updateWithFriend:(id)arg1;
- (_Bool)_cellIsBeingRefreshed;
- (void)_updateSubLabelWithMyStories:(id)arg1;
- (void)updateWithMyStories:(id)arg1 expanded:(_Bool)arg2 isContributionStory:(_Bool)arg3;
- (void)_setFriendmojiViewForFriend:(id)arg1;
- (void)_addFriendmojiViewForFriend:(id)arg1;
- (void)showPieSliceView:(_Bool)arg1 forFriendStories:(id)arg2;
- (_Bool)_shouldShowTapToReplyWithCellType:(long long)arg1 friendStories:(id)arg2;
- (void)_updateWithCellType:(long long)arg1 friendStories:(id)arg2;
- (void)updateWithFriendStories:(id)arg1;
- (void)updateWithUnviewedFriendStories:(id)arg1;
- (void)updateWithDiscoverChannel:(id)arg1;
- (void)toggleSubText:(_Bool)arg1;
- (void)_showTapToReply:(_Bool)arg1;
- (void)hideSubViewWithoutAnimation;
- (void)hideSubViewIfNecessaryAnimated:(_Bool)arg1 duration:(double)arg2 delay:(double)arg3 completion:(CDUnknownBlockType)arg4;
- (void)hideSubViewIfNecessaryAnimated:(_Bool)arg1 duration:(double)arg2;
- (void)hideSubViewIfNecessaryAnimated:(_Bool)arg1;
- (void)showSubViewWithoutAnimation;
- (void)showSubViewIfNecessaryAnimated:(_Bool)arg1;
- (void)setDisplayNameForDiscoverChannel:(id)arg1;
- (void)setDisplayNameForFriendStories:(id)arg1;
- (void)layoutLabels;
- (void)layoutSubviews;
- (CDUnknownBlockType)_leftButtonConstraintMakerWithView:(id)arg1 referenceView:(id)arg2 resizable:(_Bool)arg3;
- (id)replySnapButton;
- (_Bool)isTapToReplyMode;
- (id)dismissBaseView;
- (struct CGRect)thumbnailTouchRegion;
- (struct CGRect)thumbnailRect;
- (void)clearThumbnail;
- (void)prepareForReuse;
- (id)initWithStyle:(long long)arg1 reuseIdentifier:(id)arg2;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

