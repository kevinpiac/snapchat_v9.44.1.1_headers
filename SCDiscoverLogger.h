//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCLongformContentViewLogger.h"

@class ASIdentifierManager, NSString, SCDiscoverChunk, SCDiscoverEditionsSession, SCSingleDiscoverEditionSession;

@interface SCDiscoverLogger : NSObject <SCLongformContentViewLogger>
{
    NSString *_snapcodePublisherId;
    NSString *_snapcodeDSnapId;
    NSString *_snapcodeEditionId;
    NSString *_snapcodeActionId;
    NSString *_snapcodeDenyReason;
    _Bool _snapcodeWasConsumed;
    SCSingleDiscoverEditionSession *_currentEditionSession;
    SCDiscoverEditionsSession *_currentAutoAdvanceEditionsSession;
    SCDiscoverChunk *_lastViewedLoadingChunk;
    ASIdentifierManager *_asIdentifierManager;
}

+ (double)roundCGFloat:(double)arg1;
+ (void)_setSharingMediaType:(id)arg1 forEvent:(id)arg2;
+ (id)shared;
+ (void)logBcovErrorForPublisher:(id)arg1 videoID:(id)arg2 videoURL:(id)arg3 isInline:(_Bool)arg4 brightcoveError:(id)arg5 AVFoundationError:(id)arg6;
+ (void)logBadPublisherDataForChannel:(id)arg1 field:(id)arg2;
+ (void)logBadChunkDataForChunk:(id)arg1 field:(id)arg2;
+ (void)logBadChunkMetadataForChunk:(id)arg1;
+ (id)errorTypeStringForError:(id)arg1;
@property(retain, nonatomic) ASIdentifierManager *asIdentifierManager; // @synthesize asIdentifierManager=_asIdentifierManager;
@property(retain, nonatomic) SCDiscoverChunk *lastViewedLoadingChunk; // @synthesize lastViewedLoadingChunk=_lastViewedLoadingChunk;
@property(nonatomic) __weak SCDiscoverEditionsSession *currentAutoAdvanceEditionsSession; // @synthesize currentAutoAdvanceEditionsSession=_currentAutoAdvanceEditionsSession;
@property(nonatomic) __weak SCSingleDiscoverEditionSession *currentEditionSession; // @synthesize currentEditionSession=_currentEditionSession;
- (void).cxx_destruct;
- (long long)_editionReadState;
- (void)_setMediaTypePropertiesForEvent:(id)arg1;
- (void)_setCommonChunkPropertiesForEvent:(id)arg1 chunk:(id)arg2;
- (void)_setCommonEditionSessionPropertiesForEvent:(id)arg1;
- (void)_setCommonPropertiesForViewEvent:(id)arg1 chunk:(id)arg2 timeViewed:(double)arg3;
- (void)logSubscriptionLongformView;
- (void)logStoreView;
- (void)logRemoteWebpageViewWithPageLoadCount:(long long)arg1 pageLoadErrorCount:(long long)arg2 loadedOnEntry:(_Bool)arg3 loadedOnExit:(_Bool)arg4 visiblePageLoadTimeSec:(double)arg5 userPermissionPromptCount:(unsigned long long)arg6 userPermissionPromptAllowedCount:(unsigned long long)arg7;
- (void)logScreenshot;
- (void)logAdSkipWithAdChunk:(id)arg1;
- (void)logInlineInlineVideoViewWithID:(id)arg1 mediaDisplayedTime:(double)arg2 totalVideoDuration:(double)arg3 fullscreenTime:(double)arg4 inlineTime:(double)arg5 startedWithCaptionOn:(_Bool)arg6 videoAspectRatio:(double)arg7 videoInLandscapeModeTimeViewed:(double)arg8 videoWithCaptionOnTimeViewed:(double)arg9;
- (void)logLongformVideoViewWithStartedWithCaptionOn:(_Bool)arg1 videoWithCaptionOnTimeViewedSeconds:(double)arg2 videoDurationSeconds:(double)arg3 videoViewDurationSeconds:(double)arg4 aspectRatio:(double)arg5 videoInLandscapeModeTimeViewedSeconds:(double)arg6;
- (void)logLocalWebpageViewWithPageHeightTotal:(double)arg1 pageHeightSeen:(double)arg2 inlineVideosTotalCount:(long long)arg3 inlineVideosViewedCount:(unsigned long long)arg4 inlineVideosTotalTimeViewedSec:(double)arg5;
- (void)logTopSnapVideoViewWithFullView:(_Bool)arg1 mediaDisplayTimeSec:(double)arg2 durationSec:(double)arg3;
- (void)logTopSnapImageView;
- (void)_logEditionViewSummary;
- (void)logEditionView;
- (void)logAutoAdvanceSession;
- (void)_setSharingPropertiesForSharingEvent:(id)arg1 sharingParameters:(id)arg2;
- (void)_setCommonPropertiesForSharingEvent:(id)arg1 chunk:(id)arg2;
- (void)logFinishSavingWithSavingParameters:(id)arg1;
- (void)logDeniedSharing;
- (void)logCompletedSharingContentWithSharingParameters:(id)arg1;
- (void)logBeginSharing;
- (void)logUnsubscribeFromChannelWithPublisherId:(id)arg1 source:(unsigned long long)arg2;
- (void)logSubscribeToChannelWithPublisherId:(id)arg1 source:(unsigned long long)arg2 editionId:(id)arg3 dSnapId:(id)arg4;
- (id)deviceAdvertiserIdString;
- (void)setCommonPropertiesForDeepLinkEvent:(id)arg1 publisherId:(id)arg2 editionId:(id)arg3 dSnapId:(id)arg4 scanActionId:(id)arg5 denyReason:(id)arg6;
- (void)didDismissSnapcode:(unsigned long long)arg1;
- (void)didConsumeSnapcode;
- (void)didProcessSnapcodeWithPublisherId:(id)arg1 editionId:(id)arg2 dSnapId:(id)arg3 actionId:(id)arg4 denyReason:(id)arg5;
- (void)wasDeniedDeepLinkForPublisherId:(id)arg1 editionId:(id)arg2 dSnapId:(id)arg3 reason:(id)arg4;
- (void)logDiscoverViewWithContentSeenIdsDict:(id)arg1 totalCellCount:(long long)arg2 sortOrderId:(id)arg3;
- (void)didFinishOpeningEdition;
- (void)didStartOpeningEdition;
- (void)didFinishDisplayingChannelIconsWithContext:(unsigned long long)arg1;
- (void)didStartDisplayingChannelIconsWithContext:(unsigned long long)arg1;
- (void)didFinishWaitingForDeepLinkingValidationForDSnapID:(id)arg1 editionID:(id)arg2 publisher:(id)arg3 result:(id)arg4;
- (void)didStartWaitingForDeepLinkingValidation;
- (void)didViewDsnapWithoutWaitingForLoading:(id)arg1;
- (void)didFinishWaitingForDsnapToLoad:(id)arg1 success:(_Bool)arg2 error:(id)arg3 abandoned:(_Bool)arg4;
- (void)didPossiblyAbandonLoadingDsnap;
- (void)didStartWaitingForDsnapToLoad:(id)arg1;
- (void)logNumberOfBytesTransferredToBCOVControllerForVideoID:(id)arg1 editionID:(id)arg2 publisherName:(id)arg3 isInline:(_Bool)arg4 withNumberOfBytes:(long long)arg5;
- (void)didFinishBufferingLongformVideoForDSnapID:(id)arg1 editionID:(id)arg2 videoID:(id)arg3 publisherName:(id)arg4 videoSessionID:(id)arg5 success:(_Bool)arg6 loadingAbandoned:(_Bool)arg7 isInline:(_Bool)arg8;
- (void)didStartBufferingLongformVideoForDSnapID:(id)arg1;
- (void)didFinishWaitingForUnloadedLongformVideoForDSnapID:(id)arg1 editionID:(id)arg2 videoID:(id)arg3 publisherName:(id)arg4 videoSessionID:(id)arg5 success:(_Bool)arg6 loadingAbandoned:(_Bool)arg7 videoSize:(struct CGSize)arg8 isInline:(_Bool)arg9 cachedTSFileCount:(long long)arg10;
- (void)didStartWaitingForUnloadedLongformVideoForDSnapID:(id)arg1;
- (void)didEnterEditionWithoutWaitingForChannelMedia:(id)arg1;
- (void)didFinishLoadingIconForChannel:(id)arg1 iconType:(unsigned long long)arg2 success:(_Bool)arg3;
- (void)didFinishLoadingChunk:(id)arg1 success:(_Bool)arg2;
- (void)didStartLoadingChunk:(id)arg1;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

