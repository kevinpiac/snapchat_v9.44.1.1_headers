//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UIViewController.h"

#import "SCLagunaAppStatusListener.h"
#import "SCLagunaEventListener.h"
#import "SCManagedDeviceCapacityAnalyzerListener.h"
#import "SCTimeProfilable.h"

@class NSDate, NSString, NSTimer, SCLagunaPairingAnimatableSnapCodeView, SCLoadingIndicatorView, SCUserSession, UIButton, UIImageView, UILabel, UIView;

@interface SCLagunaSnapCodeViewController : UIViewController <SCTimeProfilable, SCLagunaEventListener, SCManagedDeviceCapacityAnalyzerListener, SCLagunaAppStatusListener>
{
    NSString *_managedCapturerToken;
    UIView *_snapcodePlaceholder;
    SCLagunaPairingAnimatableSnapCodeView *_snapcodeView;
    UIImageView *_backupPairingHelperView;
    UIImageView *_backupPairingSignalAnimatingView;
    UILabel *_pairingStatusLabel;
    UILabel *_pairingSubMessageLabel;
    UILabel *_brightnessLabel;
    SCLoadingIndicatorView *_pairingLoadingIndicator;
    UIImageView *_pairingSuccessfulImageView;
    UIButton *_cancelButton;
    long long _samplingCounter;
    long long _pairingFailureCounter;
    double _previousBrightness;
    double _lastAdaptiveBrightness;
    double _targetBrightness;
    _Bool _didLookingHintAppear;
    unsigned long long _currentIndicatorPos;
    unsigned long long _currentStatusLabelPos;
    unsigned long long _currentSubMsgLabelPos;
    SCUserSession *_userSession;
    _Bool _isBluetoothPowerOff;
    double _screenRatio;
    NSString *_pairingDeviceName;
    NSDate *_pairingStartTime;
    id <SCLagunaSnapcodeViewControllerDelegate> _delegate;
    unsigned long long _state;
    _Bool _isManualMode;
    NSTimer *_lookingTimeoutTimer;
    NSTimer *_lookingHintTimer;
    NSTimer *_pairingTimeoutTimer;
    NSTimer *_singleTapConfirmTimer;
    NSTimer *_brightnessTimer;
    double _pageStartTimestamp;
    long long _currentPageName;
    id <SCLagunaLibraryLogger><SCLagunaAppLogger> _analyticsLogger;
}

+ (long long)context;
+ (id)profiledSelectorNames;
- (void).cxx_destruct;
- (void)_updateCurrentPage:(long long)arg1;
- (void)_logPairingSuccess:(id)arg1;
- (unsigned long long)supportedInterfaceOrientations;
- (void)brightnessAnimate;
- (void)tapConfirmTimeout;
- (void)pairingStateTimeout;
- (void)lookingHintTimeout;
- (void)lookingStateTimeout;
- (void)pairWithSnapcodeButtonPressed;
- (void)pairManuallyButtonPressed;
- (void)resumeButtonPressed;
- (void)cancelButtonPressed;
- (void)pairRetryButtonPressed;
- (void)statusCoordinatorBluetoothTurnedOn:(id)arg1;
- (void)statusCoordinatorBluetoothTurnedOff:(id)arg1;
- (void)managedDeviceCapacityAnalyzer:(id)arg1 didChangeBrightness:(float)arg2;
- (id)_getSwitchModeButton:(unsigned long long)arg1;
- (void)_updateBackupPairingSignalAnimatingView:(_Bool)arg1;
- (void)_updateBackupPairingHelperView:(_Bool)arg1;
- (void)_updateSnapcodeBrightness:(float)arg1;
- (void)_updatePairingSuccessfulImageView:(_Bool)arg1;
- (void)_updatePairingLoadingIndicatorPosition:(unsigned long long)arg1;
- (void)_updatePairingLoadingIndicator:(_Bool)arg1 pos:(unsigned long long)arg2;
- (void)_updatePairingSubMessagePosition:(unsigned long long)arg1;
- (void)_updatePairingSubMessageLabel:(id)arg1 pos:(unsigned long long)arg2;
- (void)_updatePairingStatusLabelPosition:(unsigned long long)arg1;
- (void)_updatePairingStatusLabel:(id)arg1 pos:(unsigned long long)arg2;
- (void)_dismissPage;
- (void)_refreshViewForNameExisted:(id)arg1 inAlertFlow:(id)arg2;
- (_Bool)_deviceNameExisted:(id)arg1;
- (void)_refreshViewForNameTooShortInAlertFlow:(id)arg1;
- (void)_refreshViewForPreviouslyPairedDeviceInAlertFlow:(id)arg1;
- (void)_refreshViewForNamingDeviceInAlertFlow:(id)arg1;
- (void)_refreshViewForPairingCanceled:(unsigned long long)arg1 inAlertFlow:(id)arg2;
- (void)_refreshViewForDetectOverload;
- (void)_refreshViewForWrongiPhoneVersion;
- (void)_refreshViewForBTSelectorFailure;
- (id)_getCancelButton:(unsigned long long)arg1;
- (id)_pairingFailureDescription:(id)arg1;
- (void)_showPairingFailureAlert:(unsigned long long)arg1;
- (void)_refreshViewForPairingFailure:(unsigned long long)arg1;
- (void)_refreshViewForAlreadyPaired;
- (void)_setScreenBrightnessGently:(double)arg1;
- (void)_stopBrightnessAdjust;
- (void)_resetScreenBrightnessIfNecessary;
- (void)_updateState:(unsigned long long)arg1;
- (void)lagunaOnDevicePaired:(id)arg1;
- (void)lagunaOnPairingStateUpdate:(unsigned long long)arg1;
- (void)viewDidDisappear:(_Bool)arg1;
- (void)viewDidAppear:(_Bool)arg1;
- (void)dealloc;
- (void)viewDidLoad;
- (id)backupPairingSignalAnimatingView;
- (id)backupPairingHelperView;
- (id)pairingSuccessfulImageView;
- (id)pairingLoadingIndicator;
- (id)brightnessLabel;
- (id)pairingSubMessageLabel;
- (id)pairingStatusLabel;
- (void)_startSingleTapConfirmTimer;
- (void)_startPairingTimeoutTimer;
- (void)_startLookingStateTimer;
- (void)_startLookingHintTimer;
- (void)_cleanUpTimers;
- (void)_initCancelButton;
- (void)_initSnapcodeView;
- (void)loadView;
- (id)initWithUserSession:(id)arg1 snapcodeView:(id)arg2 delegate:(id)arg3;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

