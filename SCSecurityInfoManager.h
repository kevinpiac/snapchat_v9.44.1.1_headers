//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSArray;

@interface SCSecurityInfoManager : NSObject
{
    int _capricornNumber;
    NSArray *_capricornEndpoints;
}

+ (id)path;
+ (id)shared;
@property(retain, nonatomic) NSArray *capricornEndpoints; // @synthesize capricornEndpoints=_capricornEndpoints;
@property(nonatomic) int capricornNumber; // @synthesize capricornNumber=_capricornNumber;
- (void).cxx_destruct;
- (_Bool)isChallengeActive;
- (void)clearChallengeAtLogin;
- (void)setOptionalFromDictionary:(id)arg1;
- (_Bool)saveState;
- (id)initWithCoder:(id)arg1;
- (void)encodeWithCoder:(id)arg1;
- (id)init;

@end

