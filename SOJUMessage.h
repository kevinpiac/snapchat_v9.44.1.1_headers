//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SOJUMessage.h"

@class NSString;

@interface SOJUMessage : NSObject <SOJUMessage>
{
    NSString *_type;
    NSString *_idValue;
}

+ (long long)sojuSnapOrientationFromUIImageOrientation:(long long)arg1;
+ (id)createMessageFromDictionary:(id)arg1;
+ (_Bool)shouldShowNetworkActivity:(id)arg1;
+ (_Bool)shouldRetryFailed:(id)arg1;
+ (_Bool)message:(id)arg1 supersedesPriorMessage:(id)arg2;
+ (_Bool)canSupersedePriorMessages:(id)arg1;
+ (_Bool)shouldPerformActionOnSuccessOrFailure:(id)arg1;
+ (_Bool)expectsACKMessage:(id)arg1;
+ (_Bool)isPresenceMessage:(id)arg1;
@property(readonly, copy, nonatomic) NSString *idValue; // @synthesize idValue=_idValue;
@property(readonly, copy, nonatomic) NSString *type; // @synthesize type=_type;
- (void).cxx_destruct;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
- (_Bool)isEqual:(id)arg1;
- (void)encodeWithCoder:(id)arg1;
- (id)initWithCoder:(id)arg1;
- (id)copyWithZone:(struct _NSZone *)arg1;
- (id)initWithType:(id)arg1 idValue:(id)arg2;
- (id)toJson;
- (id)toDictionary;
- (id)initWithJSONDictionary:(id)arg1;
- (long long)typeEnum;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly) Class superclass;

@end

