//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UIView.h"

@interface SCLagunaOnboardingDescriptionLabel : UIView
{
}

- (id)_prepareDoneButton;
- (id)_prepareLabel:(id)arg1 font:(id)arg2;
- (id)initWithFrame:(struct CGRect)arg1 offsetForVideo:(double)arg2 primaryText:(id)arg3 secondaryText:(id)arg4 scrollViewDelegate:(id)arg5;

@end

