//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSString, SCClientEncryption, SCStoryMediaCache, Story;

@protocol SCStoryMediaCacheDataSource <NSObject>
- (Story *)storyForCacheKey:(NSString *)arg1 withStoryMediaCache:(SCStoryMediaCache *)arg2;
- (SCClientEncryption *)encryptorForCacheKey:(NSString *)arg1 withStoryMediaCache:(SCStoryMediaCache *)arg2;
@end

