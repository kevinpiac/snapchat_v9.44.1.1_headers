//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSArray, SCUserSession;

@interface SCGalleryEntryThumbnailGenerator : NSObject
{
    SCUserSession *_userSession;
    id <SCGalleryEntry> _entry;
    struct CGSize _targetSize;
    CDStruct_bac8f6e9 _mediaScenePath;
    id <SCGalleryEntry> _latestEntry;
    NSArray *_latestSnaps;
    NSArray *_highlightedSnaps;
    long long _indexOfSnapForStoryThumbnail;
    long long _indexOfHighlightedSnapsForStoryThumbnail;
    _Bool _generatingUpdates;
    _Bool _shouldGenerateHighlightThumbnail;
    id <SCDataObjectObserveContext> _entryObserveContext;
    id _timerObserveContext;
    id <SCCachingMediaRequest> _cachingMediaRequest;
    id <SCGalleryEntryThumbnailGeneratorDelegate> _delegate;
}

+ (id)dispatchQueue;
+ (double)delayOfUpdatingStoryThumbnailForCellAtIndexPath:(id)arg1;
+ (id)invertedStoryOverlayForRequestFormat:(unsigned long long)arg1;
+ (void)stopUpdatingStoryThumbnails;
+ (void)startUpdatingStoryThumbnails;
@property(nonatomic) __weak id <SCGalleryEntryThumbnailGeneratorDelegate> delegate; // @synthesize delegate=_delegate;
- (void).cxx_destruct;
- (void)_generateThumbnailForHighlightedEntry:(id)arg1;
- (void)_generateThumbnailForStoryEntry;
- (void)_generateThumbnailForSnapEntry;
- (void)_storyThumbnailUpdateTimerDidFire;
- (void)_updateThumbnailWithLatestEntry:(id)arg1 latestSnaps:(id)arg2 latestHighlightedSnaps:(id)arg3;
- (void)stopGeneratingUpdates;
- (void)startGeneratingUpdates;
- (void)dealloc;
- (id)initWithUserSession:(id)arg1 entry:(id)arg2 targetSize:(struct CGSize)arg3 mediaScenePath:(CDStruct_bac8f6e9)arg4 shouldGenerateHighlightThumbnail:(_Bool)arg5;

@end

