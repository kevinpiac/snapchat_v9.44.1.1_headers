//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "NSCopying.h"
#import "SCSnapAdsSerializableData.h"

@class NSArray, NSNumber, NSString;

@interface SCAdStoryImpressionTrackData : NSObject <SCSnapAdsSerializableData, NSCopying>
{
    NSNumber *_timeViewedSeconds;
    NSNumber *_mediaDurationSeconds;
    NSNumber *_snapCount;
    NSNumber *_viewedSnapIndex;
    NSString *_exitEvent;
    NSNumber *_uniqueSwipeUps;
    NSNumber *_totalSwipeUps;
    NSNumber *_isAudioOn;
    NSArray *_snapImpressions;
}

@property(readonly, copy, nonatomic) NSArray *snapImpressions; // @synthesize snapImpressions=_snapImpressions;
@property(readonly, copy, nonatomic) NSNumber *isAudioOn; // @synthesize isAudioOn=_isAudioOn;
@property(readonly, copy, nonatomic) NSNumber *totalSwipeUps; // @synthesize totalSwipeUps=_totalSwipeUps;
@property(readonly, copy, nonatomic) NSNumber *uniqueSwipeUps; // @synthesize uniqueSwipeUps=_uniqueSwipeUps;
@property(readonly, copy, nonatomic) NSString *exitEvent; // @synthesize exitEvent=_exitEvent;
@property(readonly, copy, nonatomic) NSNumber *viewedSnapIndex; // @synthesize viewedSnapIndex=_viewedSnapIndex;
@property(readonly, copy, nonatomic) NSNumber *snapCount; // @synthesize snapCount=_snapCount;
@property(readonly, copy, nonatomic) NSNumber *mediaDurationSeconds; // @synthesize mediaDurationSeconds=_mediaDurationSeconds;
@property(readonly, copy, nonatomic) NSNumber *timeViewedSeconds; // @synthesize timeViewedSeconds=_timeViewedSeconds;
- (void).cxx_destruct;
- (id)serialize;
- (id)copyWithZone:(struct _NSZone *)arg1;
- (id)initWithTimeViewedSeconds:(id)arg1 mediaDurationSeconds:(id)arg2 snapCount:(id)arg3 viewedSnapIndex:(id)arg4 exitEvent:(id)arg5 uniqueSwipeUps:(id)arg6 totalSwipeUps:(id)arg7 isAudioOn:(id)arg8 snapImpressions:(id)arg9;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

