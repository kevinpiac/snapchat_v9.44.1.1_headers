//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UITableViewCell.h"

@class SCItemView;

@interface SCItemViewTableViewCell : UITableViewCell
{
    SCItemView *_itemView;
}

@property(readonly, nonatomic) SCItemView *itemView; // @synthesize itemView=_itemView;
- (void).cxx_destruct;
- (void)layoutSubviews;
- (void)prepareForReuse;
- (id)initWithStyle:(long long)arg1 reuseIdentifier:(id)arg2;

@end

