//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "GPBMessage.h"

@class NSMutableArray, VLKNrfActionRequest, VLKNrfBluetoothRequest, VLKNrfDeviceRenameRequest, VLKNrfDiffUpdateRequest, VLKNrfGpsUpdateRequest, VLKNrfShellCmdRequest, VLKNrfTimeRequest, VLKNrfUserAssociationRequest, VLKNrfWifiRequest;

@interface VLKNrfRequest : GPBMessage
{
}

+ (id)descriptor;

// Remaining properties
@property(nonatomic) _Bool hasNrfActionRequest; // @dynamic hasNrfActionRequest;
@property(nonatomic) _Bool hasNrfBluetoothRequest; // @dynamic hasNrfBluetoothRequest;
@property(nonatomic) _Bool hasNrfDeviceRenameRequest; // @dynamic hasNrfDeviceRenameRequest;
@property(nonatomic) _Bool hasNrfDiffUpdateRequest; // @dynamic hasNrfDiffUpdateRequest;
@property(nonatomic) _Bool hasNrfGpsUpdateRequest; // @dynamic hasNrfGpsUpdateRequest;
@property(nonatomic) _Bool hasNrfShellCmdRequest; // @dynamic hasNrfShellCmdRequest;
@property(nonatomic) _Bool hasNrfTimeRequest; // @dynamic hasNrfTimeRequest;
@property(nonatomic) _Bool hasNrfUserAssociationRequest; // @dynamic hasNrfUserAssociationRequest;
@property(nonatomic) _Bool hasNrfWifiRequest; // @dynamic hasNrfWifiRequest;
@property(nonatomic) _Bool hasReadyForBackupPairingConfirmationTap; // @dynamic hasReadyForBackupPairingConfirmationTap;
@property(retain, nonatomic) VLKNrfActionRequest *nrfActionRequest; // @dynamic nrfActionRequest;
@property(retain, nonatomic) VLKNrfBluetoothRequest *nrfBluetoothRequest; // @dynamic nrfBluetoothRequest;
@property(retain, nonatomic) VLKNrfDeviceRenameRequest *nrfDeviceRenameRequest; // @dynamic nrfDeviceRenameRequest;
@property(retain, nonatomic) VLKNrfDiffUpdateRequest *nrfDiffUpdateRequest; // @dynamic nrfDiffUpdateRequest;
@property(retain, nonatomic) VLKNrfGpsUpdateRequest *nrfGpsUpdateRequest; // @dynamic nrfGpsUpdateRequest;
@property(retain, nonatomic) VLKNrfShellCmdRequest *nrfShellCmdRequest; // @dynamic nrfShellCmdRequest;
@property(retain, nonatomic) NSMutableArray *nrfStatusRequestArray; // @dynamic nrfStatusRequestArray;
@property(readonly, nonatomic) unsigned long long nrfStatusRequestArray_Count; // @dynamic nrfStatusRequestArray_Count;
@property(retain, nonatomic) VLKNrfTimeRequest *nrfTimeRequest; // @dynamic nrfTimeRequest;
@property(retain, nonatomic) VLKNrfUserAssociationRequest *nrfUserAssociationRequest; // @dynamic nrfUserAssociationRequest;
@property(retain, nonatomic) VLKNrfWifiRequest *nrfWifiRequest; // @dynamic nrfWifiRequest;
@property(nonatomic) _Bool readyForBackupPairingConfirmationTap; // @dynamic readyForBackupPairingConfirmationTap;

@end

