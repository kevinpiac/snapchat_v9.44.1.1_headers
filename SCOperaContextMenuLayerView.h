//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UIView.h"

#import "SCOperaLayerView.h"

@class NSString, SCGrowingButton, SCOperaContextMenuLayer, UIButton, UIImageView, UILabel;

@interface SCOperaContextMenuLayerView : UIView <SCOperaLayerView>
{
    UILabel *_displayName;
    UILabel *_usernameAndScore;
    SCOperaContextMenuLayer *_layer;
    UIImageView *_gradientView;
    SCGrowingButton *_editButton;
    UIView *_header;
    SCGrowingButton *_sendButton;
    UIButton *_reportButton;
}

+ (id)usernameColor;
+ (id)displayNameColor;
+ (id)headerColor;
+ (id)layerViewWithFrame:(struct CGRect)arg1;
@property(readonly, nonatomic) UIButton *reportButton; // @synthesize reportButton=_reportButton;
@property(readonly, nonatomic) SCGrowingButton *sendButton; // @synthesize sendButton=_sendButton;
@property(readonly, nonatomic) UIView *header; // @synthesize header=_header;
@property(readonly, nonatomic) SCGrowingButton *editButton; // @synthesize editButton=_editButton;
- (void).cxx_destruct;
- (void)setVisible:(_Bool)arg1 animationDuration:(double)arg2 animationOptions:(unsigned long long)arg3;
- (void)_updateHeaderFadeOutConstraints;
- (void)_updateHeaderFadeInConstraints;
- (void)_updateButtonFadeOutConstraints;
- (void)_updateButtonFadeInConstraints;
- (void)_setupUsernameAndScoreLabelWithLayer:(id)arg1;
- (void)_setupDisplayNameLabelWithLayer:(id)arg1;
- (void)_setupHeaderWithLayer:(id)arg1;
- (void)_setupGradient;
- (void)_setupButtonsWithLayer:(id)arg1;
- (void)setupViewForLayer:(id)arg1;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

