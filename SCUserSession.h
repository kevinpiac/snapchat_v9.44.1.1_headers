//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSMutableDictionary, NSString, SCCustomStickerManager, SCGalleryDataMutator, SCGalleryEncryptedDatabase, SCGalleryFilePathManager, SCGalleryLogger, SCGalleryOnboardingManager, SCGalleryPrivateGalleryManager, SCGallerySearch, SCGallerySearchDataSynchronizer, SCGallerySearchIndexer, SCGallerySearchQueryResultsCollector, SCGallerySearchTagUploader, SCGallerySuggestedQueryUpdater, SCGalleryUserDefaultsManager, SCGalleryUserSettingsManager, SCGalleryVisualConceptThresholdUpdater, SCGeofilterPassportGalleryDataSynchronizer, SCKeyService, SCMergedGalleryDataSource;

@interface SCUserSession : NSObject
{
    struct recursive_mutex _mutex;
    NSMutableDictionary *_userSessionScopedObjects;
    _Bool _invalidated;
    NSString *_userId;
    NSString *_username;
    NSString *_lagunaId;
}

+ (void)_createDirectoriesIfNeededAtPath:(id)arg1;
+ (void)cleanUpOutOfScopeDocumentFilesExceptForUser:(id)arg1;
+ (id)cacheURL;
@property(readonly, copy, nonatomic) NSString *lagunaId; // @synthesize lagunaId=_lagunaId;
@property(readonly, copy, nonatomic) NSString *username; // @synthesize username=_username;
@property(readonly, copy, nonatomic) NSString *userId; // @synthesize userId=_userId;
- (id).cxx_construct;
- (void).cxx_destruct;
- (void)invalidate;
- (id)objectForKey:(id)arg1 initializer:(CDUnknownBlockType)arg2;
- (void)dealloc;
- (id)initWithUserId:(id)arg1 username:(id)arg2 lagunaId:(id)arg3;
@property(readonly, nonatomic) SCGallerySearchIndexer *gallerySearchIndexer;
- (id)lagunaGalleryWifiController;
- (id)lagunaFirmwareManager;
- (id)lagunaManager;
- (_Bool)isLagunaInitialized;
- (id)updatesResponseAnnouncer;
- (id)lagunaContentDataSource;
- (id)reverseAudioCache;
@property(readonly, copy, nonatomic) id <SCCustomStickerOwner> customStickerOwner;
@property(readonly, nonatomic) SCCustomStickerManager *customStickerManager;
@property(readonly, nonatomic) SCGalleryFilePathManager *filePathManager;
- (id)mediaDownload;
- (id)lowresMediaFilePreloader;
- (id)networker;
@property(readonly, nonatomic) SCGalleryLogger *galleryLogger;
@property(readonly, nonatomic) SCGalleryEncryptedDatabase *galleryEncryptedDatabase;
- (id)cloudFS;
@property(readonly, nonatomic) SCGalleryUserSettingsManager *galleryUserSettingsManager;
@property(readonly, copy, nonatomic) NSString *userScopedDocumentFilePath;
@property(readonly, nonatomic) SCGallerySearch *gallerySearch;
@property(readonly, nonatomic) SCGalleryPrivateGalleryManager *privateGalleryManager;
@property(readonly, nonatomic) SCGallerySearchTagUploader *gallerySearchTagUploader;
@property(readonly, nonatomic) SCGallerySearchDataSynchronizer *gallerySearchDataSynchronizer;
@property(readonly, nonatomic) SCGallerySearchQueryResultsCollector *searchQueryResultsCollector;
@property(readonly, nonatomic) SCGalleryDataMutator *galleryDataMutator;
- (id)cachingMediaManager;
@property(readonly, nonatomic) SCGallerySuggestedQueryUpdater *suggestedQueryUpdater;
- (id)mediaFileRestorer;
@property(readonly, nonatomic) SCGalleryOnboardingManager *galleryOnboardingManager;
- (id)cloudSync;
- (id)searchFriendsDataProvider;
- (id)mediaSendTaskManager;
- (id)optionalMediaUploader;
- (void)_databaseUpgradeCheck:(id)arg1;
@property(readonly, copy, nonatomic) id <SCGalleryProfile> profile;
@property(readonly, nonatomic) SCGalleryVisualConceptThresholdUpdater *visualConceptUpdater;
@property(readonly, nonatomic) SCGeofilterPassportGalleryDataSynchronizer *geofilterPassportGalleryDataSynchronizer;
@property(readonly, nonatomic) SCGalleryUserDefaultsManager *galleryUserDefaultsManager;
@property(readonly, nonatomic) SCKeyService *keyService;
- (id)encryptedContentManager;
@property(readonly, nonatomic) SCMergedGalleryDataSource *mergedGalleryDataSource;
- (id)statusCoordinator;
- (id)experimentManager;

@end

