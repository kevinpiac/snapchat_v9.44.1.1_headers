//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class SCGalleryFooterBar;

@protocol SCGalleryFooterBarDelegate <NSObject>
- (void)galleryFooterBarDidPressGetHDButton:(SCGalleryFooterBar *)arg1;
- (void)galleryFooterBarDidPressSendButton:(SCGalleryFooterBar *)arg1;
- (void)galleryFooterBarDidPressUnlockButton:(SCGalleryFooterBar *)arg1;
- (void)galleryFooterBarDidPressLockButton:(SCGalleryFooterBar *)arg1;
- (void)galleryFooterBarDidPressTrashButton:(SCGalleryFooterBar *)arg1;
- (void)galleryFooterBarDidPressShareButton:(SCGalleryFooterBar *)arg1;
- (void)galleryFooterBarDidPressStoryButton:(SCGalleryFooterBar *)arg1;
@end

