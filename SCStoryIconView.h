//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCSingleIconViewBase.h"

@class SCStoryLoadingIndicatorView, UIImageView;

@interface SCStoryIconView : SCSingleIconViewBase
{
    _Bool _isAnimating;
    SCStoryLoadingIndicatorView *_loadingView;
    UIImageView *_replayView;
    UIImageView *_checkmarkImageView;
    _Bool _selected;
}

- (void).cxx_destruct;
- (void)layoutSubviews;
- (void)_stopAnimating;
- (void)_startAnimatingWithShrinkCompletion:(CDUnknownBlockType)arg1;
- (void)_resetIconView;
- (void)showGrayLoadingCircle;
- (void)setImageAlpha:(double)arg1;
- (void)hideSpinner;
- (void)showSpinner;
- (void)setSelected:(_Bool)arg1 animated:(_Bool)arg2;
- (void)setupCheckmarkView;
- (void)setReplayViewHidden:(_Bool)arg1;
- (id)initWithFrame:(struct CGRect)arg1;

@end

