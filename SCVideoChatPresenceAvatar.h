//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class UIImageView, UIView;

@interface SCVideoChatPresenceAvatar : NSObject
{
    UIImageView *_avatarFaceImageView;
    UIImageView *_avatarHandsImageView;
    UIView *_containerView;
    UIView *_inputView;
    unsigned long long _bubbleState;
    CDUnknownBlockType _toPeekingBlock;
    _Bool _isShown;
    id <SCVideoChatPresenceAvatarDelegate> _delegate;
}

@property(nonatomic) __weak id <SCVideoChatPresenceAvatarDelegate> delegate; // @synthesize delegate=_delegate;
- (void).cxx_destruct;
- (void)remove;
@property(readonly, nonatomic) _Bool isAvatarReady;
@property(nonatomic, getter=isHidden) _Bool hidden;
- (void)_releaseToPeekingBlock;
- (void)_showTorsoAnimationWithDuration:(double)arg1 completion:(CDUnknownBlockType)arg2;
- (void)_showAvatarAnimationWithDuration:(double)arg1 completion:(CDUnknownBlockType)arg2;
- (id)_resizeImage:(id)arg1 isHands:(_Bool)arg2 toSize:(double)arg3;
- (void)_initHandsImageViewAutoLayoutConstraints;
- (void)_initFaceImageViewAutoLayoutConstraints;
- (double)_scaleOffsetForSize:(double)arg1;
- (void)_animateAvatarShowing:(_Bool)arg1 animated:(_Bool)arg2 withCompletion:(CDUnknownBlockType)arg3;
- (void)hideAvatarAnimated:(_Bool)arg1 withCompletion:(CDUnknownBlockType)arg2;
- (void)showAvatarAnimated:(_Bool)arg1 withCompletion:(CDUnknownBlockType)arg2;
- (void)updateBubbleState:(unsigned long long)arg1 withCompletion:(CDUnknownBlockType)arg2;
- (void)dealloc;
- (id)initWithContainerView:(id)arg1 inputView:(id)arg2 avatarImage:(id)arg3 handsImage:(id)arg4;

@end

