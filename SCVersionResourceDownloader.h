//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSDictionary, NSString, SCQueuePerformer, SCRequestManager;

@interface SCVersionResourceDownloader : NSObject
{
    SCRequestManager *_requestManager;
    NSDictionary *_metadata;
    NSString *_sessionId;
    SCQueuePerformer *_queuePerformer;
    int _resourceDownloaderState;
    id <SCVersionResourceDownloaderStateChangeDelegate> _delegate;
}

+ (id)_encodeWithSHA1:(id)arg1;
@property(nonatomic) __weak id <SCVersionResourceDownloaderStateChangeDelegate> delegate; // @synthesize delegate=_delegate;
@property(readonly, nonatomic) int resourceDownloaderState; // @synthesize resourceDownloaderState=_resourceDownloaderState;
- (void).cxx_destruct;
- (void)_transitionToState:(int)arg1 fromState:(int)arg2;
- (void)startDownloadingUpdateToFilePath:(id)arg1;
- (void)startDownloadingUpdate;
- (void)startCheckingUpdateWithParameters:(id)arg1;
- (void)reset;
- (id)initWithRequestManager:(id)arg1 withQueue:(id)arg2;

@end

