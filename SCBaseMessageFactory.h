//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@interface SCBaseMessageFactory : NSObject
{
}

+ (long long)_mediaTypeFromSCChatMessageMediaType:(long long)arg1;
+ (id)_createDiscoverMediaMessageFromMediaProvider:(id)arg1 sender:(id)arg2 recipient:(id)arg3 messageMetadata:(id)arg4;
+ (id)_createSnapchatterMessageFromSender:(id)arg1 recipient:(id)arg2 messageMetadata:(id)arg3;
+ (id)_createStoryShareMessageFromSender:(id)arg1 recipient:(id)arg2 messageMetadata:(id)arg3;
+ (id)_createStoryMediaMessageFromMediaProvider:(id)arg1 sender:(id)arg2 recipient:(id)arg3 messageMetadata:(id)arg4;
+ (id)_createBatchedMediaMessageFromMediaProvider:(id)arg1 sender:(id)arg2 recipient:(id)arg3;
+ (id)_createBaseChatMediaWithMediaProvider:(id)arg1;
+ (id)_createMediaListWithMediaProviders:(id)arg1;
+ (id)_createChatMediaMessageFromMediaProvider:(id)arg1 sender:(id)arg2 recipient:(id)arg3;
+ (id)createMessageWithRecipientUsername:(id)arg1 uploadedMediaProviders:(id)arg2 messageMetadata:(id)arg3;

@end

