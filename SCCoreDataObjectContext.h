//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCDataObjectContext.h"

@class NSManagedObjectContext, NSMapTable, NSMutableDictionary, NSString;

@interface SCCoreDataObjectContext : SCDataObjectContext
{
    NSString *_contextName;
    long long _transactionCount;
    unsigned long long _status;
    NSManagedObjectContext *_managedObjectContext;
    NSMutableDictionary *_observeGraph;
    NSMapTable *_immutableObjectGraph;
}

+ (id)managedObjectID:(id)arg1 forContext:(id)arg2;
+ (void)setCurrentObjectContext:(id)arg1;
+ (id)currentObjectContext;
+ (id)sharedContextFor:(id)arg1;
- (void).cxx_destruct;
- (void)_managedObjectContextObjectsDidChange:(id)arg1;
- (void)_invalidateImmutableObject:(id)arg1;
- (id)immutableObjectForClass:(Class)arg1 managedObject:(id)arg2;
- (void)unobserve:(id)arg1 objectClass:(Class)arg2 objectID:(id)arg3;
- (id)observe:(Class)arg1 object:(id)arg2 queue:(id)arg3 changeHandler:(CDUnknownBlockType)arg4;
- (_Bool)performChangesAndWait:(CDUnknownBlockType)arg1 error:(id *)arg2;
- (_Bool)isInsidePerformChanges;
- (void)performChanges:(CDUnknownBlockType)arg1 queue:(id)arg2 completionHandler:(CDUnknownBlockType)arg3;
- (void)performAndWait:(CDUnknownBlockType)arg1;
- (void)perform:(CDUnknownBlockType)arg1;
- (void)installInMemoryStoreWithBundle:(id)arg1;
- (void)_installWithFileURL:(id)arg1 persistentStoreCoordinator:(id)arg2;
- (void)_installPersistentStoreIfNeeded;
- (void)installPersistentStore;
- (void)destroyPersistentStore;
- (_Bool)_migratePersistentStoreWithFileURL:(id)arg1 sourceModel:(id)arg2 destinationModel:(id)arg3;
- (void)_installWithPersistentStoreCoordinator:(id)arg1;
- (void)dealloc;
- (id)initWithContextName:(id)arg1;
- (id)contextName;

@end

