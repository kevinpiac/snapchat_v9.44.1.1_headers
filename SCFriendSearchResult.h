//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "NSCoding.h"

@class NSNumber, NSString;

@interface SCFriendSearchResult : NSObject <NSCoding>
{
    NSString *_userId;
    NSString *_username;
    NSString *_displayName;
    NSNumber *_score;
    NSString *_debugInfo;
}

@property(copy) NSString *debugInfo; // @synthesize debugInfo=_debugInfo;
@property(copy) NSNumber *score; // @synthesize score=_score;
@property(copy) NSString *displayName; // @synthesize displayName=_displayName;
@property(copy) NSString *username; // @synthesize username=_username;
@property(copy) NSString *userId; // @synthesize userId=_userId;
- (void).cxx_destruct;
- (id)initWithCoder:(id)arg1;
- (void)encodeWithCoder:(id)arg1;
- (id)initWithUserId:(id)arg1 username:(id)arg2 displayName:(id)arg3 score:(id)arg4 debugInfo:(id)arg5;

@end

