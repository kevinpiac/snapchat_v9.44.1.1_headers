//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "UIViewControllerAnimatedTransitioning.h"
#import "UIViewControllerInteractiveTransitioning.h"

@class NSString, UICollectionView, UIView;

@interface SCGalleryItemQuickActionDismissalAnimator : NSObject <UIViewControllerAnimatedTransitioning, UIViewControllerInteractiveTransitioning>
{
    UIView *_backgroundView;
    UIView *_destinationView;
    UICollectionView *_destinationCollectionView;
    UIView *_blackView;
    id <SCGalleryViewingItem> _viewingItem;
    id <UIViewControllerContextTransitioning> _transitionContext;
    _Bool _isStarted;
}

- (void).cxx_destruct;
- (struct CGSize)_finalMaskSizeForView:(id)arg1;
- (struct CGSize)_initialMaskSizeForView:(id)arg1;
- (double)_contentAspectRatio;
- (struct CGSize)_thumbnailRotationSize:(struct CGSize)arg1;
- (long long)_thumbnailOrientation;
- (struct CGAffineTransform)_scaleFromTranslation:(struct CGPoint)arg1 originalPosition:(struct CGPoint)arg2;
- (double)_minimumScale;
- (double)_ratio:(double)arg1;
- (void)_cancelTransition;
- (void)_dismissWithDuration:(double)arg1;
- (_Bool)_shouldDismissWithVelocity:(struct CGPoint)arg1 translation:(struct CGPoint)arg2;
- (_Bool)_finishPan:(id)arg1;
- (void)finishPan:(id)arg1;
- (void)_didPan:(struct CGPoint)arg1 currentPosition:(struct CGPoint)arg2;
- (void)didPan:(id)arg1;
- (double)transitionDuration:(id)arg1;
- (void)animateTransition:(id)arg1;
- (void)startAnimationWithDuration:(double)arg1 transitionContext:(id)arg2 completion:(CDUnknownBlockType)arg3;
- (void)startInteractiveTransition:(id)arg1;
- (_Bool)_isStory;
- (struct CGRect)_destinationRectForView:(id)arg1;
- (void)_scrollToItemIfNotInView:(id)arg1;
- (id)initWithCollectionView:(id)arg1 index:(id)arg2 item:(id)arg3 destinationView:(id)arg4;

// Remaining properties
@property(readonly, nonatomic) long long completionCurve;
@property(readonly, nonatomic) double completionSpeed;
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;
@property(readonly, nonatomic) _Bool wantsInteractiveStart;

@end

