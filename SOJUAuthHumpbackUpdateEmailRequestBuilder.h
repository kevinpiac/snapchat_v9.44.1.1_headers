//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSNumber, NSString;

@interface SOJUAuthHumpbackUpdateEmailRequestBuilder : NSObject
{
    NSString *_userId;
    NSString *_email;
    NSNumber *_emailVersion;
}

+ (id)withJUAuthHumpbackUpdateEmailRequest:(id)arg1;
- (void).cxx_destruct;
- (id)setEmailVersion:(id)arg1;
- (id)setEmail:(id)arg1;
- (id)setUserId:(id)arg1;
- (id)build;
- (id)setEmailVersionValue:(long long)arg1;

@end

