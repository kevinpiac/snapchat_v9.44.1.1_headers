//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCWebScriptHelperDelegate.h"

@class NSMutableDictionary, NSString, SCWebScriptHelper;

@interface SCWebImageScraperClient : NSObject <SCWebScriptHelperDelegate>
{
    SCWebScriptHelper *_scriptHelper;
    NSMutableDictionary *_dummyImageCache;
    NSMutableDictionary *_websiteLinks;
    _Bool _loadingImages;
    NSString *_pageAbsoluteUrl;
    int _state;
    id <SCWebImageScraperClientDelegate> _delegate;
}

@property(nonatomic) int state; // @synthesize state=_state;
@property(nonatomic) __weak id <SCWebImageScraperClientDelegate> delegate; // @synthesize delegate=_delegate;
- (void).cxx_destruct;
- (id)imagesForUrl:(id)arg1;
- (void)webScriptHelper:(id)arg1 didNotEvaluate:(id)arg2;
- (void)webScriptHelper:(id)arg1 didEvaluate:(id)arg2 withResult:(id)arg3;
- (void)webScriptHelper:(id)arg1 isReadyWithState:(int)arg2;
- (void)_loadImage:(id)arg1 pageUrl:(id)arg2;
- (void)clearAll;
- (void)clearCacheForUrl:(id)arg1;
- (void)loadImagesFromUrl:(id)arg1;
- (id)init;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

