//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@interface SCStoriesStoryOperaParser : NSObject
{
}

+ (id)_pagePropertiesForChromeViewWithStory:(id)arg1;
+ (id)_pagePropertiesForMyStory:(id)arg1 offsetEnabled:(_Bool)arg2 showViewersTable:(_Bool)arg3;
+ (id)loadingPropertiesForStory:(id)arg1 pageProperties:(id)arg2;
+ (id)_loadingViewModelForStory:(id)arg1 pageProperties:(id)arg2;
+ (id)_pagePropertiesForShareableStory:(id)arg1;
+ (id)_pagePropertiesForSpectaclesStory;
+ (id)_pagePropertiesForImageStory:(id)arg1;
+ (id)_pagePropertiesForVideoStory:(id)arg1 loop:(_Bool)arg2;
+ (id)_sharedPagePropertiesForStory:(id)arg1;
+ (id)_sharedPagePropertiesForStory:(id)arg1 friendStories:(id)arg2 totalTimeLeft:(double)arg3 totalDuration:(double)arg4;
+ (id)dummyLoadingViewModel;
+ (id)viewModelForStory:(id)arg1 showViewersTable:(_Bool)arg2;
+ (id)viewModelsForStory:(id)arg1 friendStories:(id)arg2 totalTimeLeft:(double)arg3 totalDuration:(double)arg4;

@end

