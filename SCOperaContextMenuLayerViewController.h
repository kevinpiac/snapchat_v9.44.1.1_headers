//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCOperaLayerViewController.h"

@class SCOperaContextMenuLayer, SCOperaContextMenuLayerView;

@interface SCOperaContextMenuLayerViewController : SCOperaLayerViewController
{
    SCOperaContextMenuLayer *_layer;
    SCOperaContextMenuLayerView *_layerView;
}

- (void).cxx_destruct;
- (void)_headerTapped;
- (void)_reportPressed;
- (void)_editPressed;
- (void)_sendPressed;
- (void)_endContextMenuSession;
- (void)_tap:(id)arg1;
- (void)_updateGestures;
- (void)_setupGestures;
- (long long)pageabilityForRelativePosition:(unsigned long long)arg1;
- (void)didReceiveUpdateProperties:(id)arg1;
- (void)setLayer:(id)arg1;
- (void)loadView;
- (id)initWithLayer:(id)arg1 configuration:(id)arg2 eventAnnouncer:(id)arg3;

@end

