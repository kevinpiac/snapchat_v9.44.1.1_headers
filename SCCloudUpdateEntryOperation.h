//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCCloudSyncOperation.h"

#import "SCCloudSyncSnapOperation.h"

@class NSDictionary, NSString;

@interface SCCloudUpdateEntryOperation : SCCloudSyncOperation <SCCloudSyncSnapOperation>
{
    NSString *_requestID;
    id <SCGalleryProfile> _profile;
    NSString *_entryId;
    NSString *_deletedSnapId;
    NSString *_title;
    NSDictionary *_dataVaultEncryption;
    id <SCGallerySnap> _snapPlaceholder;
    id <SCGallerySnapDetail> _detailPlaceholder;
}

@property(readonly, nonatomic) id <SCGallerySnapDetail> detailPlaceholder; // @synthesize detailPlaceholder=_detailPlaceholder;
@property(readonly, nonatomic) id <SCGallerySnap> snapPlaceholder; // @synthesize snapPlaceholder=_snapPlaceholder;
- (void).cxx_destruct;
- (id)logParameters;
- (id)description;
- (void)_updateEntriesFromSequenceNumber:(long long)arg1 networker:(id)arg2 queue:(id)arg3 snapsUploadInfo:(id)arg4 resyncHandler:(CDUnknownBlockType)arg5 failureHandler:(CDUnknownBlockType)arg6 successHandler:(CDUnknownBlockType)arg7;
- (void)cleanupWithContext:(id)arg1 cloudFS:(id)arg2;
- (id)commitWithEntryUpdates:(id)arg1 dataObjectContext:(id)arg2;
- (void)remoteSyncFromSequenceNumber:(long long)arg1 cloudFS:(id)arg2 dataVault:(id)arg3 networker:(id)arg4 logger:(id)arg5 queue:(id)arg6 resyncHandler:(CDUnknownBlockType)arg7 failureHandler:(CDUnknownBlockType)arg8 successHandler:(CDUnknownBlockType)arg9;
- (_Bool)executeOptimisticallyWithDataObjectContext:(id)arg1;
- (id)detectAndResolveConflictsWithCloudFS:(id)arg1 dataObjectContext:(id)arg2;
- (unsigned long long)numberOfSnaps;
- (id)initWithSnapshot:(id)arg1 requestID:(id)arg2;
- (id)makeSnapshot;
- (id)requestID;
- (id)dataVaultEncryption;
- (id)entryPlaceholder;
- (_Bool)isPrivate;
- (unsigned long long)type;
- (id)initWithProfile:(id)arg1 entryId:(id)arg2 title:(id)arg3;
- (id)initWithProfile:(id)arg1 entryId:(id)arg2 deletedSnapId:(id)arg3;
- (id)initWithProfile:(id)arg1 entryId:(id)arg2 deletedSnapId:(id)arg3 snapPlaceholder:(id)arg4 detailPlaceholder:(id)arg5 dataVaultEncryption:(id)arg6;
- (id)initWithProfile:(id)arg1 entryId:(id)arg2 title:(id)arg3 deletedSnapId:(id)arg4 snapPlaceholder:(id)arg5 detailPlaceholder:(id)arg6 dataVaultEncryption:(id)arg7;

@end

