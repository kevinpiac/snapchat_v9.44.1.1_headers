//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "NSCoding.h"
#import "NSCopying.h"

@class NSDate, NSDictionary, NSString;

@interface SCCarouselConfiguration : NSObject <NSCoding, NSCopying>
{
    NSString *_configurationId;
    NSString *_versionId;
    NSDate *_expirationDate;
    NSDictionary *_baseOverrides;
    NSDictionary *_filterOverrides;
}

+ (id)filterNameFromSoju:(long long)arg1;
+ (id)filterTypeFromSoju:(long long)arg1;
@property(readonly, copy, nonatomic) NSDictionary *filterOverrides; // @synthesize filterOverrides=_filterOverrides;
@property(readonly, copy, nonatomic) NSDictionary *baseOverrides; // @synthesize baseOverrides=_baseOverrides;
@property(readonly, copy, nonatomic) NSDate *expirationDate; // @synthesize expirationDate=_expirationDate;
@property(readonly, copy, nonatomic) NSString *versionId; // @synthesize versionId=_versionId;
@property(readonly, copy, nonatomic) NSString *configurationId; // @synthesize configurationId=_configurationId;
- (void).cxx_destruct;
- (id)copyWithZone:(struct _NSZone *)arg1;
- (void)encodeWithCoder:(id)arg1;
- (id)initWithCoder:(id)arg1;
- (id)initWithSoju:(id)arg1;
- (id)init;

@end

