//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSArray, NSMutableArray, NSMutableDictionary, NSMutableSet, SCCounters, SCEventUploader;

@interface SCLogger : NSObject
{
    SCCounters *_counters;
    SCEventUploader *_eventUploader;
    NSMutableSet *_uniqueEvents;
    NSMutableDictionary *_timers;
    NSMutableDictionary *_timedEvents;
    CDUnknownBlockType _userProvider;
    CDUnknownBlockType _sendEventsBlock;
    CDUnknownBlockType _didLogBlizzardEventCallback;
    CDUnknownBlockType _didLogOpsDataPipelineEventCallback;
    CDUnknownBlockType _didEndPerfEventCallback;
    CDUnknownBlockType _didCancelTimerEventsCallback;
    id <SCLoggerAmplitudeProtocol> _blizzardLogger;
    CDUnknownBlockType _reachabilityProvider;
    CDUnknownBlockType _isLowPowerModeEnabledCallback;
    CDUnknownBlockType _isTravelModeEnabledCallback;
    CDUnknownBlockType _isProtectedDataAvailable;
    CDUnknownBlockType _mainThreadDelaysProvider;
    CDUnknownBlockType _currentHostProvider;
    CDUnknownBlockType _frameRatesProvider;
    NSArray *_performanceEvents;
    NSMutableArray *_savedEvents;
    id <SCPerforming> _performer;
}

+ (id)sharedInstance;
+ (id)perfMetricsAsString;
+ (id)perfMetrics;
+ (unsigned long long)currentThreadCount;
+ (double)cpuUsage;
+ (id)diskUsageParams;
+ (id)getTotalAndSubfolderSizesMBInDirectory:(id)arg1;
+ (unsigned long long)getFileSizeInDirectory:(id)arg1;
+ (unsigned long long)diskspaceFreeMB;
+ (unsigned long long)diskspaceUsedMB;
+ (double)mallocedMemoryUsedMB;
+ (double)memoryInactiveUsedMB;
+ (double)memoryActiveUsedMB;
+ (double)memoryWireUsedMB;
+ (double)memoryTotalUsedMB;
+ (double)memoryFreeMB;
+ (double)memoryVirtualMB;
+ (double)memoryUsedMB;
@property(retain, nonatomic) id <SCPerforming> performer; // @synthesize performer=_performer;
@property(retain, nonatomic) NSMutableArray *savedEvents; // @synthesize savedEvents=_savedEvents;
@property(retain, nonatomic) NSArray *performanceEvents; // @synthesize performanceEvents=_performanceEvents;
@property(copy, nonatomic) CDUnknownBlockType frameRatesProvider; // @synthesize frameRatesProvider=_frameRatesProvider;
@property(copy, nonatomic) CDUnknownBlockType currentHostProvider; // @synthesize currentHostProvider=_currentHostProvider;
@property(copy, nonatomic) CDUnknownBlockType mainThreadDelaysProvider; // @synthesize mainThreadDelaysProvider=_mainThreadDelaysProvider;
@property(copy, nonatomic) CDUnknownBlockType isProtectedDataAvailable; // @synthesize isProtectedDataAvailable=_isProtectedDataAvailable;
@property(copy, nonatomic) CDUnknownBlockType isTravelModeEnabledCallback; // @synthesize isTravelModeEnabledCallback=_isTravelModeEnabledCallback;
@property(copy, nonatomic) CDUnknownBlockType isLowPowerModeEnabledCallback; // @synthesize isLowPowerModeEnabledCallback=_isLowPowerModeEnabledCallback;
@property(copy, nonatomic) CDUnknownBlockType reachabilityProvider; // @synthesize reachabilityProvider=_reachabilityProvider;
@property(retain, nonatomic) id <SCLoggerAmplitudeProtocol> blizzardLogger; // @synthesize blizzardLogger=_blizzardLogger;
@property(copy, nonatomic) CDUnknownBlockType didCancelTimerEventsCallback; // @synthesize didCancelTimerEventsCallback=_didCancelTimerEventsCallback;
@property(copy, nonatomic) CDUnknownBlockType didEndPerfEventCallback; // @synthesize didEndPerfEventCallback=_didEndPerfEventCallback;
@property(copy, nonatomic) CDUnknownBlockType didLogOpsDataPipelineEventCallback; // @synthesize didLogOpsDataPipelineEventCallback=_didLogOpsDataPipelineEventCallback;
@property(copy, nonatomic) CDUnknownBlockType didLogBlizzardEventCallback; // @synthesize didLogBlizzardEventCallback=_didLogBlizzardEventCallback;
@property(copy, nonatomic) CDUnknownBlockType sendEventsBlock; // @synthesize sendEventsBlock=_sendEventsBlock;
@property(copy, nonatomic) CDUnknownBlockType userProvider; // @synthesize userProvider=_userProvider;
@property(retain, nonatomic) NSMutableDictionary *timedEvents; // @synthesize timedEvents=_timedEvents;
@property(retain, nonatomic) NSMutableDictionary *timers; // @synthesize timers=_timers;
@property(retain, nonatomic) NSMutableSet *uniqueEvents; // @synthesize uniqueEvents=_uniqueEvents;
@property(retain, nonatomic) SCEventUploader *eventUploader; // @synthesize eventUploader=_eventUploader;
@property(readonly, nonatomic) SCCounters *counters; // @synthesize counters=_counters;
- (void).cxx_destruct;
- (id)markEndSessionForType:(id)arg1;
- (_Bool)markStartSessionForType:(id)arg1 withStartTime:(double)arg2;
- (void)syncLogEventToEventLogger:(id)arg1 parameters:(id)arg2 secretParameters:(id)arg3 metrics:(id)arg4;
- (void)synchronouslyLogEvent:(id)arg1 parameters:(id)arg2 secretParameters:(id)arg3 metrics:(id)arg4;
- (void)logEndSessionForType:(id)arg1;
- (id)histogramBucketForTimeLapsed:(double)arg1 bucketSize:(double)arg2 numberBuckets:(int)arg3;
- (void)logStartSessionForType:(id)arg1;
- (void)updateBlizzardValidationServerUrl:(id)arg1;
- (void)logUserNotTrackedEvent:(id)arg1;
- (void)logUserTrackedEvent:(id)arg1;
- (void)getAndClearEventsWithBlock:(CDUnknownBlockType)arg1;
- (void)logJSONSerializationError:(id)arg1 endpoint:(id)arg2 parameters:(id)arg3;
- (void)logEventToEventLogger:(id)arg1 parameters:(id)arg2 secretParameters:(id)arg3 metrics:(id)arg4;
- (void)logEventToEventLogger:(id)arg1 parameters:(id)arg2 secretParameters:(id)arg3;
- (void)logEvent:(id)arg1 parameters:(id)arg2 secretParameters:(id)arg3 metrics:(id)arg4;
- (void)logEvent:(id)arg1 parameters:(id)arg2 secretParameters:(id)arg3;
- (void)logEvent:(id)arg1 parameters:(id)arg2;
- (void)logEvent:(id)arg1;
- (void)forceUploadEvents;
- (void)logSavedEvents;
- (void)attemptResumeBlizzardSession;
- (void)startServicesWithBlizzardLogger:(id)arg1 userProvider:(CDUnknownBlockType)arg2 sendEventsBlock:(CDUnknownBlockType)arg3 didLogBlizzardEventCallback:(CDUnknownBlockType)arg4 didLogOpsDataPipelineEventCallback:(CDUnknownBlockType)arg5 didEndPerfEventCallback:(CDUnknownBlockType)arg6 didCancelTimerEventsCallback:(CDUnknownBlockType)arg7 reachabilityProvider:(CDUnknownBlockType)arg8 isLowPowerModeEnabledCallBack:(CDUnknownBlockType)arg9 isTravelModeEnabledCallBack:(CDUnknownBlockType)arg10 isProtectedDataAvailable:(CDUnknownBlockType)arg11 mainThreadDelaysProvider:(CDUnknownBlockType)arg12 currentHostProvider:(CDUnknownBlockType)arg13 frameRateProvider:(CDUnknownBlockType)arg14;
- (id)init;
- (void)endOrCancelTimedEvent:(id)arg1 withUniqueId:(id)arg2 withParameters:(id)arg3 shouldEnd:(_Bool)arg4;
- (void)updateContentReadyAndFirstItemReadyEventswithNetworkStepName:(id)arg1;
- (void)updateContentReadyAndFirstItemReadyEventswithParameters:(id)arg1;
- (void)cancelPerfEventsWhenLoggedOut;
- (void)cancelPerfEventsWhenOpenAppFromQuickAction;
- (void)cancelPerfEventsWhenOpenAppFromDeepLink;
- (void)cancelPerfEventsWhenDiscoverEditionViewAppear;
- (void)cancelPerfEventsWhenChatViewAppear;
- (void)cancelPerfEventsForStartupToAllViews;
- (void)logCancellationPerfEventsForStartupToCameraView;
- (void)cancelPerfEventsForStartupToFeedStoryView;
- (void)cancelPerfEventsWhenVisibleVCIsNotMainVC;
- (void)cancelPerfEventsWhenPreviewViewAppear;
- (void)cancelPerfEventsWhenMyFriendsViewFullyDisappear;
- (void)cancelPerfEventsWhenFeedViewFullyDisappear;
- (void)startPerfEventsWhenAppStartupFromBackgroundWithNotification;
- (void)startPerfEventsWhenAppStartupFromBackgroundWithTimeLapse:(double)arg1;
- (void)startPerfEventsWhenAppStartupFromKilledStateWithNotification;
- (void)startPerfEventsWhenAppStartupFromKilledStateEarlyWithTimeLapse:(double)arg1;
- (void)startPerfEventsWhenAppStartupFromKilledState;
- (id)eventsWithDelaysAttached;
- (void)logEventWithAttachedDelays:(id)arg1;
- (void)setStartParameters:(id)arg1 withParameters:(id)arg2;
- (void)removeUniqueEvents;
- (void)cancelAllLogTimedEvents;
- (void)cancelLogTimedEvents:(id)arg1;
- (void)cancelLogTimedEventsWithName:(id)arg1;
- (void)cancelLogTimedEvent:(id)arg1 withUniqueId:(id)arg2;
- (id)onCancelEventDependencies;
- (void)updateLogTimedEvent:(id)arg1 withUniqueId:(id)arg2 withParameters:(id)arg3;
- (void)updateLogTimedEventStart:(id)arg1 withUniqueId:(id)arg2 withParameters:(id)arg3;
- (void)updateLogTimedEventStart:(id)arg1 withUniqueId:(id)arg2;
- (void)logTimedEventEnd:(id)arg1 withUniqueId:(id)arg2 withParameters:(id)arg3;
- (void)logNetworkStepToEvent:(id)arg1 withUniqueId:(id)arg2 withStepName:(id)arg3;
- (void)logStepToEvent:(id)arg1 withUniqueId:(id)arg2 withStepName:(id)arg3;
- (void)disableResetForEvent:(id)arg1 withUniqueId:(id)arg2;
- (void)useT0ForEvent:(id)arg1 withUniqueId:(id)arg2;
- (void)logTimedEventStart:(id)arg1 withUniqueId:(id)arg2 isUniqueEvent:(_Bool)arg3 withParameters:(id)arg4 shouldLogStartTime:(_Bool)arg5;
- (void)logTimedEventStart:(id)arg1 withUniqueId:(id)arg2 isUniqueEvent:(_Bool)arg3 withParameters:(id)arg4;
- (void)logTimedEventStart:(id)arg1 withUniqueId:(id)arg2 isUniqueEvent:(_Bool)arg3;
- (void)logCounterEvent:(id)arg1 withCount:(unsigned long long)arg2 parameters:(id)arg3;
- (void)logTimedEvent:(id)arg1 withInterval:(double)arg2 withParameters:(id)arg3 shouldSampleEvent:(_Bool)arg4;
- (void)logTimedEvent:(id)arg1 withInterval:(double)arg2 withParameters:(id)arg3;
- (void)logTimedEvent:(id)arg1 withInterval:(double)arg2;

@end

