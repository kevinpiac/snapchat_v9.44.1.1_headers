//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "NSCoding.h"

@class NSDictionary, SCFriendSearchTrie;

@interface SCFriendSearchDataProvider : NSObject <NSCoding>
{
    SCFriendSearchTrie *_trie;
    NSDictionary *_userResults;
}

+ (void)prefetchSearchResultsWithCompletion:(CDUnknownBlockType)arg1;
+ (id)path;
+ (_Bool)hasPrefetchedSearchResults;
+ (_Bool)initialized;
+ (id)shared;
- (void).cxx_destruct;
- (id)initWithCoder:(id)arg1;
- (void)encodeWithCoder:(id)arg1;
- (id)debugInfoForUserId:(id)arg1;
- (id)searchResultsForQuery:(id)arg1;
- (void)updateWithWords:(id)arg1 users:(id)arg2;
- (void)clear;
- (_Bool)saveState;

@end

