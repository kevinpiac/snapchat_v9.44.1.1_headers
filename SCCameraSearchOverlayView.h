//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCCameraOverlayView.h"

#import "SCSearchBarDelegate.h"

@class NSString, SCSearchBar;

@interface SCCameraSearchOverlayView : SCCameraOverlayView <SCSearchBarDelegate>
{
    SCSearchBar *_searchBar;
    _Bool _tooltipInitialized;
    double _topOffset;
}

- (void).cxx_destruct;
- (void)searchBarDidBeginEditing:(id)arg1;
- (void)toggleCameraPageButtonVisibility:(_Bool)arg1 animated:(_Bool)arg2;
- (void)showProfileTooltip:(_Bool)arg1 withText:(id)arg2 withExpireInSecs:(unsigned long long)arg3;
- (id)profileButtonTooltip;
- (void)initializeNightModeButton;
- (void)initializeAndAddCameraFlipButton;
- (void)initializeAndAddFlashButton;
- (void)initializeAndAddAddFriendButton;
- (id)initWithFrame:(struct CGRect)arg1 withDelegate:(id)arg2;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(nonatomic) __weak id <SCCameraSearchOverlayDelegate><SCFriendProfileCellViewDelegate> delegate; // @dynamic delegate;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

