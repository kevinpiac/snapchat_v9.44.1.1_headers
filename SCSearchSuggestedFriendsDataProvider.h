//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCSearchFriendsDataProviderListener.h"
#import "SCVerifiedUserSearcherDelegate.h"

@class NSCache, NSString, SCFriendSearchDataProvider, SCSearchFriendsDataProvider, SCVerifiedUserSearcher;

@interface SCSearchSuggestedFriendsDataProvider : NSObject <SCVerifiedUserSearcherDelegate, SCSearchFriendsDataProviderListener>
{
    SCSearchFriendsDataProvider *_friendsDataProvider;
    SCFriendSearchDataProvider *_userNameSearchDataProvider;
    SCVerifiedUserSearcher *_verifiedUserSearcher;
    NSCache *_suggestedFriendCache;
    NSCache *_verifiedUserCache;
    NSCache *_exactMatchCache;
    NSString *_currentQuery;
    id <SCSearchAddFriendDataProviderDelegate> _delegate;
}

@property(nonatomic) __weak id <SCSearchAddFriendDataProviderDelegate> delegate; // @synthesize delegate=_delegate;
- (void).cxx_destruct;
- (void)searchFriendsDataProviderDidUpdateFriends:(id)arg1;
- (void)friendSearcherDidUpdateResultsForQuery:(id)arg1 notFollowingOfficialStories:(id)arg2 followingOfficialStories:(id)arg3;
- (id)_cachedResultsForQuery:(id)arg1;
- (void)_buildExactMatchFriend;
- (void)_buildSuggestedFriendsFromVerifiedUsersDataProvider;
- (void)_buildSuggestedFriendsFromFriendsDataProvider;
- (id)searchSuggestedFriendsForQuery:(id)arg1;
- (id)initWithSearchSession:(id)arg1;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

