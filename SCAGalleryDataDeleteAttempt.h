//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCAUserTrackedEvent.h"

#import "NamedEvent.h"

@class NSNumber, NSString;

@interface SCAGalleryDataDeleteAttempt : SCAUserTrackedEvent <NamedEvent>
{
    NSNumber *withUserTrigger;
    NSNumber *snapPendingBackupCount;
    NSNumber *snapDeleteCount;
    long long actionTaken;
    long long context;
    NSString *previousUsername;
}

+ (id)copy:(id)arg1;
- (void).cxx_destruct;
- (id)asDictionary;
- (id)getPreviousUsername;
- (void)setPreviousUsername:(id)arg1;
- (long long)getContext;
- (void)setContext:(long long)arg1;
- (long long)getActionTaken;
- (void)setActionTaken:(long long)arg1;
- (long long)getSnapDeleteCount;
- (void)setSnapDeleteCount:(long long)arg1;
- (long long)getSnapPendingBackupCount;
- (void)setSnapPendingBackupCount:(long long)arg1;
- (_Bool)getWithUserTrigger;
- (void)setWithUserTrigger:(_Bool)arg1;
- (id)getEventName;
- (id)init;

@end

