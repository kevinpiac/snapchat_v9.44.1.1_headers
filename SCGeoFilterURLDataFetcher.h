//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class SCCache;

@interface SCGeoFilterURLDataFetcher : NSObject
{
    SCCache *_cache;
}

+ (id)sharedInstance;
@property(retain, nonatomic) SCCache *cache; // @synthesize cache=_cache;
- (void).cxx_destruct;
- (void)clearCache;
- (id)cacheKeyWithURL:(id)arg1 params:(id)arg2;
- (void)fetchExpiringURLData:(id)arg1 params:(id)arg2 completion:(CDUnknownBlockType)arg3 expiration:(double)arg4 resetCachedObjectExpiration:(_Bool)arg5;
- (void)fetchURLData:(id)arg1 params:(id)arg2 completion:(CDUnknownBlockType)arg3;
- (void)dealloc;
- (id)init;

@end

