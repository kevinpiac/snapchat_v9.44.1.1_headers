//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCGalleryCellTransitioning.h"
#import "SCGalleryViewCellSelecting.h"
#import "SCGalleryViewCellUpdating.h"

@class SCGalleryDebugSyncStatusChecker, SCUserSession;

@protocol SCGalleryEntryViewCellProtocol <SCGalleryViewCellSelecting, SCGalleryCellTransitioning, SCGalleryViewCellUpdating>
- (void)toggleDebugSyncStatusView;
- (void)setEntry:(id <SCGalleryEntry>)arg1 format:(unsigned long long)arg2 mediaScenePath:(CDStruct_bac8f6e9)arg3 delayOfUpdatingStoryThumbnail:(double)arg4 contentsUnloaded:(_Bool)arg5 selectMode:(unsigned long long)arg6 debugSyncStatusChecker:(SCGalleryDebugSyncStatusChecker *)arg7 userSession:(SCUserSession *)arg8;
@end

