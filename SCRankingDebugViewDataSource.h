//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSArray;

@interface SCRankingDebugViewDataSource : NSObject
{
    NSArray *_sections;
}

@property(readonly, copy, nonatomic) NSArray *sections; // @synthesize sections=_sections;
- (void).cxx_destruct;
- (void)_parseResponse:(id)arg1;
- (id)initWithProto:(id)arg1;

@end

