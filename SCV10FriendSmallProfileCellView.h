//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UIView.h"

#import "SCAddFriendButtonV2Delegate.h"

@class Friend, NSString, SCAddFriendButtonV2, SCSingleIconConfigurer, SCStoryIconView, SCV10FriendProfileCellTextView, SCV10ProfilePictureThumbnail;

@interface SCV10FriendSmallProfileCellView : UIView <SCAddFriendButtonV2Delegate>
{
    UIView *_bitmojiContainer;
    UIView *_storyThumbnailContainer;
    SCV10ProfilePictureThumbnail *_profilePictureThumbnail;
    SCV10FriendProfileCellTextView *_textViewV3;
    SCStoryIconView *_storyThumbnailView;
    SCSingleIconConfigurer *_singleIconConfigurer;
    NSString *_curPublicFriendStoriesUsername;
    UIView *_buttonContainer;
    SCAddFriendButtonV2 *_button;
    id <SCV10FriendProfileCellViewDelegate> _delegate;
    Friend *_aFriend;
}

@property(readonly, nonatomic) Friend *aFriend; // @synthesize aFriend=_aFriend;
- (void).cxx_destruct;
- (void)_thumnailStatusDidChange:(id)arg1 forPublicFriendStories:(id)arg2;
- (void)_playStoryPressed;
- (void)buttonV2Pressed:(id)arg1 friend:(id)arg2;
- (_Bool)didDisplayProfilePicture;
- (void)_updateWithMainLabel:(id)arg1 subLabel:(id)arg2 thirdLabel:(id)arg3 fourthLabel:(id)arg4 style:(long long)arg5;
- (void)_updateBitmojiWithFriend:(id)arg1 publicFriendStories:(id)arg2 contexts:(id)arg3 style:(long long)arg4;
- (void)prepareForReuse;
- (void)updateButtonWithState:(long long)arg1 friend:(id)arg2 style:(long long)arg3;
- (void)updateCellViewWithFriend:(id)arg1 isBlocked:(_Bool)arg2 publicFriendStories:(id)arg3 contexts:(id)arg4 thumbnailStyle:(long long)arg5 mainLabel:(id)arg6 subLabel:(id)arg7 thirdLabel:(id)arg8 fourthLabel:(id)arg9 textViewStyle:(long long)arg10 addButtonState:(long long)arg11 addButtonStyle:(long long)arg12 backgroundColor:(id)arg13 delegate:(id)arg14;
- (id)init;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

