//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCLagunaApplicationLifecycleListenerDelegate.h"
#import "SCLagunaDeviceEventListener.h"
#import "SCLagunaDeviceStoreDelegate.h"
#import "SCLagunaNetworkClientDelegate.h"
#import "SCLagunaTaskManagerDelegate.h"

@class NSString, SCLagunaApplicationLifecycleListener, SCLagunaBluetoothClient, SCLagunaCache, SCLagunaDevice, SCLagunaDeviceManagerIdleWatch, SCLagunaDeviceSessionEndNotifier, SCLagunaDeviceStore, SCLagunaEventListenerAnnouncer, SCLagunaTaskManager, SCLagunaWeakTimer, SCLagunaWiFiClient, SCQueuePerformer;

@interface SCLagunaDeviceManager : NSObject <SCLagunaDeviceEventListener, SCLagunaNetworkClientDelegate, SCLagunaTaskManagerDelegate, SCLagunaApplicationLifecycleListenerDelegate, SCLagunaDeviceStoreDelegate>
{
    _Bool _enteredSpecsTabDuringTransfer;
    _Bool _sawSpecsTabSinceForegrounded;
    unsigned long long _state;
    id <SCLagunaProfile> _profile;
    SCLagunaCache *_cache;
    SCLagunaTaskManager *_taskManager;
    SCLagunaDeviceStore *_deviceStore;
    SCLagunaDevice *_selectedWifiDevice;
    SCLagunaDevice *_previousDevice;
    SCLagunaDevice *_selectedDevice;
    SCQueuePerformer *_performer;
    SCLagunaBluetoothClient *_bluetoothClient;
    SCLagunaWiFiClient *_wifiClient;
    long long _wifiRestartCount;
    SCLagunaWeakTimer *_ambaWatchdogTimer;
    SCLagunaWeakTimer *_connectionWatchdogTimer;
    SCLagunaEventListenerAnnouncer *_announcer;
    SCLagunaDeviceManagerIdleWatch *_idleWatch;
    SCLagunaApplicationLifecycleListener *_applicationLifecycleListener;
    SCLagunaDeviceSessionEndNotifier *_deviceSessionEndNotifier;
    id <SCLagunaLibraryLogger> _analyticsLogger;
    long long _backgroundDownloadCount;
}

@property(nonatomic) _Bool sawSpecsTabSinceForegrounded; // @synthesize sawSpecsTabSinceForegrounded=_sawSpecsTabSinceForegrounded;
@property(nonatomic) _Bool enteredSpecsTabDuringTransfer; // @synthesize enteredSpecsTabDuringTransfer=_enteredSpecsTabDuringTransfer;
@property(nonatomic) long long backgroundDownloadCount; // @synthesize backgroundDownloadCount=_backgroundDownloadCount;
@property(retain, nonatomic) id <SCLagunaLibraryLogger> analyticsLogger; // @synthesize analyticsLogger=_analyticsLogger;
@property(retain, nonatomic) SCLagunaDeviceSessionEndNotifier *deviceSessionEndNotifier; // @synthesize deviceSessionEndNotifier=_deviceSessionEndNotifier;
@property(retain, nonatomic) SCLagunaApplicationLifecycleListener *applicationLifecycleListener; // @synthesize applicationLifecycleListener=_applicationLifecycleListener;
@property(retain, nonatomic) SCLagunaDeviceManagerIdleWatch *idleWatch; // @synthesize idleWatch=_idleWatch;
@property(nonatomic) __weak SCLagunaEventListenerAnnouncer *announcer; // @synthesize announcer=_announcer;
@property(retain, nonatomic) SCLagunaWeakTimer *connectionWatchdogTimer; // @synthesize connectionWatchdogTimer=_connectionWatchdogTimer;
@property(retain, nonatomic) SCLagunaWeakTimer *ambaWatchdogTimer; // @synthesize ambaWatchdogTimer=_ambaWatchdogTimer;
@property(nonatomic) long long wifiRestartCount; // @synthesize wifiRestartCount=_wifiRestartCount;
@property(retain, nonatomic) SCLagunaWiFiClient *wifiClient; // @synthesize wifiClient=_wifiClient;
@property(retain, nonatomic) SCLagunaBluetoothClient *bluetoothClient; // @synthesize bluetoothClient=_bluetoothClient;
@property(retain, nonatomic) SCQueuePerformer *performer; // @synthesize performer=_performer;
@property(retain, nonatomic) SCLagunaDevice *selectedDevice; // @synthesize selectedDevice=_selectedDevice;
@property(retain, nonatomic) SCLagunaDevice *previousDevice; // @synthesize previousDevice=_previousDevice;
@property(retain, nonatomic) SCLagunaDevice *selectedWifiDevice; // @synthesize selectedWifiDevice=_selectedWifiDevice;
@property(retain, nonatomic) SCLagunaDeviceStore *deviceStore; // @synthesize deviceStore=_deviceStore;
@property(retain, nonatomic) SCLagunaTaskManager *taskManager; // @synthesize taskManager=_taskManager;
@property(retain, nonatomic) SCLagunaCache *cache; // @synthesize cache=_cache;
@property(nonatomic) __weak id <SCLagunaProfile> profile; // @synthesize profile=_profile;
- (void).cxx_destruct;
- (void)networkClientDidTimeOut:(id)arg1;
- (void)networkClientDidOpenConnection:(id)arg1;
- (void)networkClient:(id)arg1 didReceiveAmbaResponse:(id)arg2;
- (void)networkClient:(id)arg1 didError:(id)arg2;
- (void)deviceStoreDidClearDevices;
- (void)deviceStore:(id)arg1 didAddDevice:(id)arg2;
- (void)_updateDeviceManagerForDeviceUpdate:(id)arg1;
- (void)device:(id)arg1 didMarkCorruptContent:(id)arg2;
- (void)deviceDidStartRecording:(id)arg1;
- (void)deviceDidUpdateTaskQueue:(id)arg1;
- (void)device:(id)arg1 didUpdateInfo:(unsigned long long)arg2;
- (void)deviceDidUpdateState:(id)arg1;
- (void)taskManager:(id)arg1 didDownloadContent:(id)arg2 component:(unsigned long long)arg3;
- (void)taskManagerHandledResponse:(id)arg1;
- (void)taskManager:(id)arg1 sawError:(id)arg2;
- (void)taskManagerComplete:(id)arg1;
- (void)taskManager:(id)arg1 sendAmbaRequest:(id)arg2 withDelay:(double)arg3;
- (void)deviceSessionDidEnd;
- (void)_stopAmbaWatchdog;
- (void)_startAmbaWatchdog;
- (void)_stopClients;
- (void)_restartWifiClient;
- (void)_startWifiClient;
- (void)_ensureNetworkClientActive;
- (void)_cancelEnsureTaskManagerConnected;
- (void)_startBluetoothClient;
- (void)_sendAmbaRequest:(id)arg1;
- (id)_networkClient;
- (void)_handleNetworkConnectionOpen;
- (void)_transitionToState:(unsigned long long)arg1;
@property(nonatomic) unsigned long long state; // @synthesize state=_state;
- (void)_selectDeviceForDownload;
- (_Bool)_shouldSelectDevice:(id)arg1;
- (_Bool)isContentPartOfCurrentTransferBatch:(id)arg1 component:(unsigned long long)arg2;
- (void)prioritize:(id)arg1 context:(unsigned long long)arg2;
- (void)cancelConnectingWifiForHDFlow;
- (void)startConnectingWifiForHDFlow:(id)arg1 hdContentUUIDs:(id)arg2;
- (void)dealloc;
- (void)applicationWillEnterForeground;
- (_Bool)isBackgrounded;
- (id)initWithDeviceStore:(id)arg1 announcer:(id)arg2 cache:(id)arg3 profile:(id)arg4 analyticsLogger:(id)arg5;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

