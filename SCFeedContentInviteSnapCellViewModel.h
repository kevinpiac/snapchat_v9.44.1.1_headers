//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCFeedBaseComponentViewModel.h"
#import "SCFeedCellViewModel.h"
#import "SCFeedSnapActionViewModel.h"
#import "SCFilterableProtocol.h"

@class NSString, SCFeedItem;

@interface SCFeedContentInviteSnapCellViewModel : NSObject <SCFeedCellViewModel, SCFeedBaseComponentViewModel, SCFeedSnapActionViewModel, SCFilterableProtocol>
{
    SCFeedItem *_feedItem;
}

@property(retain, nonatomic) SCFeedItem *feedItem; // @synthesize feedItem=_feedItem;
- (void).cxx_destruct;
- (_Bool)isSentEvent;
- (_Bool)matches:(id)arg1;
- (id)messageToHandle;
- (_Bool)shouldShowSnapSubstituteSubLabelBriefly;
- (id)snapToHandle;
- (long long)type;
- (_Bool)shouldShowActivityIndicator;
- (id)feedIconImageName;
- (id)timestamp;
- (id)subLabelText;
- (id)displayNameFontKey;
- (id)displayNameWithWidth:(double)arg1;
- (id)displayName;
- (_Bool)hasUnreadMessages;
- (_Bool)hasFailedMessages;
- (id)identifier;
- (id)reusableCellIdentifier;
- (id)initWithFeedItem:(id)arg1;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

