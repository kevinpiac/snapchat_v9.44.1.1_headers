//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCChatMetadata.h"

@class NSDictionary, NSSet, NSString, SCChatSnapchatter, SCLiveStoryPublisher, SCStorySnapMediaContent, SCStorySnapMetadata;

@interface SCChatMetadata : NSObject <SCChatMetadata>
{
    NSDictionary *_urlMediaCardContents;
    NSDictionary *_addressMediaCardContents;
    NSDictionary *_mediaMetadataDicts;
    SCStorySnapMetadata *_storySnapMetadata;
    SCStorySnapMediaContent *_storySnapMediaContent;
    SCChatSnapchatter *_snapchatter;
    SCLiveStoryPublisher *_publisher;
    NSSet *_tappedSnapchatterIds;
}

@property(readonly, copy, nonatomic) NSSet *tappedSnapchatterIds; // @synthesize tappedSnapchatterIds=_tappedSnapchatterIds;
@property(readonly, copy, nonatomic) SCLiveStoryPublisher *publisher; // @synthesize publisher=_publisher;
@property(readonly, copy, nonatomic) SCChatSnapchatter *snapchatter; // @synthesize snapchatter=_snapchatter;
@property(readonly, copy, nonatomic) SCStorySnapMediaContent *storySnapMediaContent; // @synthesize storySnapMediaContent=_storySnapMediaContent;
@property(readonly, copy, nonatomic) SCStorySnapMetadata *storySnapMetadata; // @synthesize storySnapMetadata=_storySnapMetadata;
@property(readonly, copy, nonatomic) NSDictionary *mediaMetadataDicts; // @synthesize mediaMetadataDicts=_mediaMetadataDicts;
@property(readonly, copy, nonatomic) NSDictionary *addressMediaCardContents; // @synthesize addressMediaCardContents=_addressMediaCardContents;
@property(readonly, copy, nonatomic) NSDictionary *urlMediaCardContents; // @synthesize urlMediaCardContents=_urlMediaCardContents;
- (void).cxx_destruct;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
- (_Bool)isEqual:(id)arg1;
- (void)encodeWithCoder:(id)arg1;
- (id)initWithCoder:(id)arg1;
- (id)copyWithZone:(struct _NSZone *)arg1;
- (id)initWithUrlMediaCardContents:(id)arg1 addressMediaCardContents:(id)arg2 mediaMetadataDicts:(id)arg3 storySnapMetadata:(id)arg4 storySnapMediaContent:(id)arg5 snapchatter:(id)arg6 publisher:(id)arg7 tappedSnapchatterIds:(id)arg8;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly) Class superclass;

@end

