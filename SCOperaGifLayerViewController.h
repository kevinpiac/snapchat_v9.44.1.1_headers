//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCOperaLayerViewController.h"

@class SCOperaGifLayer, SCOperaGifLayerView;

@interface SCOperaGifLayerViewController : SCOperaLayerViewController
{
    SCOperaGifLayer *_layer;
    SCOperaGifLayerView *_layerView;
}

- (void).cxx_destruct;
- (void)_loadImage;
- (void)loadView;
- (long long)pageabilityForRelativePosition:(unsigned long long)arg1;
- (id)initWithLayer:(id)arg1 configuration:(id)arg2 eventAnnouncer:(id)arg3;

@end

