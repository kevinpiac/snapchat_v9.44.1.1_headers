//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSArray, NSString, SCUserSession;

@interface SCGalleryDeleteItemsOperation : NSObject
{
    SCUserSession *_userSession;
    NSArray *_galleryItems;
    _Bool _running;
    CDUnknownBlockType _completionBlock;
    NSArray *_galleryEntries;
    NSArray *_photoAssets;
    _Bool _deletedGalleryEntries;
    _Bool _deletedPhotoAssets;
    NSString *_callerContext;
}

- (void).cxx_destruct;
- (void)_complete;
- (void)_deletePhotoAssetsWithCompletion:(CDUnknownBlockType)arg1;
- (void)_deleteGalleryEntriesWithCompletion:(CDUnknownBlockType)arg1;
- (void)runWithCompletionBlock:(CDUnknownBlockType)arg1;
- (void)dealloc;
- (id)initWithGalleryItems:(id)arg1 context:(id)arg2 userSession:(id)arg3;

@end

