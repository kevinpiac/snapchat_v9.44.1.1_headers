//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UIView.h"

@class NSArray, SCGradientView, SCPreviewToolBarButtonItem, UIScrollView;

@interface SCPreviewToolBar : UIView
{
    NSArray *_items;
    SCPreviewToolBarButtonItem *_selectedItem;
    UIScrollView *_scrollView;
    SCGradientView *_gradientMaskView;
    struct CGPoint _previousContentOffset;
}

@property(retain, nonatomic) SCGradientView *gradientMaskView; // @synthesize gradientMaskView=_gradientMaskView;
@property(nonatomic) struct CGPoint previousContentOffset; // @synthesize previousContentOffset=_previousContentOffset;
@property(retain, nonatomic) UIScrollView *scrollView; // @synthesize scrollView=_scrollView;
@property(retain, nonatomic) SCPreviewToolBarButtonItem *selectedItem; // @synthesize selectedItem=_selectedItem;
@property(copy, nonatomic) NSArray *items; // @synthesize items=_items;
- (void).cxx_destruct;
- (void)_adjustScrollViewContentHeight:(double)arg1;
- (void)_adjustContentOffset;
- (void)_adjustItemsAlpha;
- (void)_addAccessoryItemsForItem:(id)arg1;
- (void)_removeAccessoryItemsForItem:(id)arg1;
- (void)setSelectedItem:(id)arg1 animated:(_Bool)arg2;
- (void)layoutSubviews;
- (_Bool)pointInside:(struct CGPoint)arg1 withEvent:(id)arg2;
- (id)hitTest:(struct CGPoint)arg1 withEvent:(id)arg2;
- (id)accessoryViewForPoint:(struct CGPoint)arg1;
- (id)initWithFrame:(struct CGRect)arg1;

@end

