//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCAlertGearDelegate.h"
#import "SCEditNameAlertDelegate.h"
#import "SCMiniProfileViewDelegate.h"
#import "SCSendViewControllerSendingDelegate.h"
#import "SendSnapNavigationControllerDelegate.h"

@class Friend, NSArray, NSString, SCAlertGear, SCEditNameAlert, SCMiniProfileView, SCUserSession, SendViewController, UIViewController;

@interface SCMiniProfileController : NSObject <SCMiniProfileViewDelegate, SendSnapNavigationControllerDelegate, SCAlertGearDelegate, SCEditNameAlertDelegate, SCSendViewControllerSendingDelegate>
{
    id <SCMiniProfileControllerDelegate> _delegate;
    UIViewController *_parentViewController;
    long long _page;
    long long _viewLocation;
    SCMiniProfileView *_miniProfileView;
    NSArray *_contexts;
    SCUserSession *_userSession;
    SendViewController *_sendVC;
    Friend *_friend;
    SCAlertGear *_alertGear;
    SCEditNameAlert *_editNameAlert;
}

+ (id)_stringForMiniProfilePageType:(long long)arg1;
@property(retain, nonatomic) SCEditNameAlert *editNameAlert; // @synthesize editNameAlert=_editNameAlert;
@property(retain, nonatomic) SCAlertGear *alertGear; // @synthesize alertGear=_alertGear;
@property(retain, nonatomic) Friend *friend; // @synthesize friend=_friend;
- (void).cxx_destruct;
- (void)_logMiniProfileView;
- (id)getPageName;
- (void)_sendSnapchatterShareToRecipients:(id)arg1 mischiefs:(id)arg2;
- (void)sendViewController:(id)arg1 sendToRecipients:(id)arg2 invitedRecipients:(id)arg3 postToMyStory:(_Bool)arg4 officialStories:(id)arg5 sharedStories:(id)arg6 mobStories:(id)arg7 mischiefs:(id)arg8 gallery:(_Bool)arg9 inviteRecipientShown:(long long)arg10;
- (void)customOperationForUnBlock:(id)arg1;
- (void)customOperationForBlock:(id)arg1 blockReasonId:(id)arg2;
- (void)customOperationForDeleteorIgnore:(id)arg1;
- (void)handleSetDisplay:(id)arg1 friend:(id)arg2;
- (void)_dismissPreviewIfPresented;
- (void)didSendToGallery;
- (void)didPostStory;
- (void)didSendSnap:(id)arg1;
- (void)didCancelFromPreview:(id)arg1;
- (void)displayNamePressed:(id)arg1;
- (void)didPressAddFriendButton:(id)arg1 friend:(id)arg2 onProgress:(CDUnknownBlockType)arg3 onSuccess:(CDUnknownBlockType)arg4 onFailure:(CDUnknownBlockType)arg5;
- (void)didPressSendAccountButton:(id)arg1 friend:(id)arg2;
- (void)didPressShareButton:(id)arg1 friend:(id)arg2;
- (void)didPressSettingsGearButton:(id)arg1 friend:(id)arg2;
- (void)didPressSnapButton:(id)arg1 friend:(id)arg2;
- (void)didPressChatButton:(id)arg1 friend:(id)arg2;
- (void)miniProfileWasDismissed:(id)arg1;
- (void)setViewLocation:(long long)arg1;
- (void)dismissMiniProfileForFriend:(id)arg1;
- (void)unhideMiniProfile;
- (_Bool)inMiniProfile;
- (void)presentMiniProfile;
- (id)_getUpdatedFriendForName:(id)arg1;
- (void)_reloadData;
- (void)dealloc;
- (id)initWithDelegate:(id)arg1 parentViewController:(id)arg2 page:(long long)arg3 friend:(id)arg4 contexts:(id)arg5;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

