//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSManagedObject.h"

@class NSDate, NSNumber, NSString, SCCDGalleryEntry, SCCDGalleryProfile, SCCDGallerySnapDetail, SCCDGallerySnapID;

@interface _SCCDGallerySnap : NSManagedObject
{
}

+ (id)keyPathsForValuesAffectingValueForKey:(id)arg1;
+ (id)entityInManagedObjectContext:(id)arg1;
+ (id)entityName;
+ (id)insertInManagedObjectContext:(id)arg1;
- (void)setPrimitiveWidthValue:(int)arg1;
- (int)primitiveWidthValue;
@property int widthValue;
- (void)setPrimitiveSourceValue:(int)arg1;
- (int)primitiveSourceValue;
@property int sourceValue;
- (void)setPrimitiveOrientationValue:(int)arg1;
- (int)primitiveOrientationValue;
@property int orientationValue;
- (void)setPrimitiveMediaTypeValue:(int)arg1;
- (int)primitiveMediaTypeValue;
@property int mediaTypeValue;
- (void)setPrimitiveInterestingnessScoreValue:(float)arg1;
- (float)primitiveInterestingnessScoreValue;
@property float interestingnessScoreValue;
- (void)setPrimitiveHeightValue:(int)arg1;
- (int)primitiveHeightValue;
@property int heightValue;
- (void)setPrimitiveHdMediaStateValue:(int)arg1;
- (int)primitiveHdMediaStateValue;
@property int hdMediaStateValue;
- (void)setPrimitiveHasThumbnailValue:(_Bool)arg1;
- (_Bool)primitiveHasThumbnailValue;
@property _Bool hasThumbnailValue;
- (void)setPrimitiveHasSyncedValue:(_Bool)arg1;
- (_Bool)primitiveHasSyncedValue;
@property _Bool hasSyncedValue;
- (void)setPrimitiveHasOverlayImageValue:(_Bool)arg1;
- (_Bool)primitiveHasOverlayImageValue;
@property _Bool hasOverlayImageValue;
- (void)setPrimitiveHasLocationValue:(_Bool)arg1;
- (_Bool)primitiveHasLocationValue;
@property _Bool hasLocationValue;
- (void)setPrimitiveHasInterestingnessScoreValue:(_Bool)arg1;
- (_Bool)primitiveHasInterestingnessScoreValue;
@property _Bool hasInterestingnessScoreValue;
- (void)setPrimitiveDurationValue:(float)arg1;
- (float)primitiveDurationValue;
@property float durationValue;
- (void)setPrimitiveCloudMediaStateValue:(int)arg1;
- (int)primitiveCloudMediaStateValue;
@property int cloudMediaStateValue;
@property(readonly, nonatomic) SCCDGallerySnapID *objectID;

// Remaining properties
@property(retain, nonatomic) id attribution; // @dynamic attribution;
@property(retain, nonatomic) NSString *cameraRollId; // @dynamic cameraRollId;
@property(retain, nonatomic) NSNumber *cloudMediaState; // @dynamic cloudMediaState;
@property(retain, nonatomic) NSDate *createTimeUtc; // @dynamic createTimeUtc;
@property(retain, nonatomic) SCCDGallerySnapDetail *detail; // @dynamic detail;
@property(retain, nonatomic) NSString *deviceFirmwareInfo; // @dynamic deviceFirmwareInfo;
@property(retain, nonatomic) NSString *deviceId; // @dynamic deviceId;
@property(retain, nonatomic) NSNumber *duration; // @dynamic duration;
@property(retain, nonatomic) SCCDGalleryEntry *entry; // @dynamic entry;
@property(retain, nonatomic) SCCDGalleryEntry *entryHighlighted; // @dynamic entryHighlighted;
@property(retain, nonatomic) id framing; // @dynamic framing;
@property(retain, nonatomic) NSNumber *hasInterestingnessScore; // @dynamic hasInterestingnessScore;
@property(retain, nonatomic) NSNumber *hasLocation; // @dynamic hasLocation;
@property(retain, nonatomic) NSNumber *hasOverlayImage; // @dynamic hasOverlayImage;
@property(retain, nonatomic) NSNumber *hasSynced; // @dynamic hasSynced;
@property(retain, nonatomic) NSNumber *hasThumbnail; // @dynamic hasThumbnail;
@property(retain, nonatomic) NSNumber *hdMediaState; // @dynamic hdMediaState;
@property(retain, nonatomic) id hdMediaURI; // @dynamic hdMediaURI;
@property(retain, nonatomic) NSNumber *height; // @dynamic height;
@property(retain, nonatomic) NSNumber *interestingnessScore; // @dynamic interestingnessScore;
@property(retain, nonatomic) NSString *mediaId; // @dynamic mediaId;
@property(retain, nonatomic) NSNumber *mediaType; // @dynamic mediaType;
@property(retain, nonatomic) id mediaURI; // @dynamic mediaURI;
@property(retain, nonatomic) NSNumber *orientation; // @dynamic orientation;
@property(retain, nonatomic) id overlayImageURI; // @dynamic overlayImageURI;
@property(retain, nonatomic) SCCDGalleryProfile *owner; // @dynamic owner;
@property(retain, nonatomic) SCCDGalleryProfile *ownerDeleted; // @dynamic ownerDeleted;
@property(retain, nonatomic) NSString *snapId; // @dynamic snapId;
@property(retain, nonatomic) NSNumber *source; // @dynamic source;
@property(retain, nonatomic) SCCDGalleryEntry *syncedEntry; // @dynamic syncedEntry;
@property(retain, nonatomic) SCCDGalleryEntry *syncedEntryHighlighted; // @dynamic syncedEntryHighlighted;
@property(retain, nonatomic) id thumbnailURI; // @dynamic thumbnailURI;
@property(retain, nonatomic) NSString *timeZoneName; // @dynamic timeZoneName;
@property(retain, nonatomic) NSString *transferBatchId; // @dynamic transferBatchId;
@property(retain, nonatomic) NSNumber *width; // @dynamic width;

@end

