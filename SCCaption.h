//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@interface SCCaption : NSObject
{
}

+ (_Bool)isBigTextPlusMode:(long long)arg1;
+ (_Bool)isDefaultMode:(long long)arg1;
+ (id)captionForState:(id)arg1 delegate:(id)arg2 isLagunaMedia:(_Bool)arg3 initialTransform:(struct CGAffineTransform)arg4 originalContentBounds:(struct CGRect)arg5 orientation:(long long)arg6 superviewBounds:(struct CGRect)arg7 superviewContentBounds:(struct CGRect)arg8;

@end

