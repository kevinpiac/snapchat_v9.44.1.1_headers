//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@interface SCStaleCacheEvaluator : NSObject
{
}

+ (void)attemptUploadStaleCacheEvent;
+ (id)staleCacheStatistics;
+ (void)getStaleFilesCountAndSizesInDirectory:(id)arg1 filesCount:(out id *)arg2 filesTotalSize:(out id *)arg3;
+ (id)dateComponentsBetweenModificationAndNow:(id)arg1;

@end

