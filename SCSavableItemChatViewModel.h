//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCMessageChatViewModel.h"

#import "SCSavableItemChatCellViewConfig.h"

@class NSString;

@interface SCSavableItemChatViewModel : SCMessageChatViewModel <SCSavableItemChatCellViewConfig>
{
    _Bool _isSaved;
    _Bool _savedByCurrentUser;
    id <SCChatMessageState> _messageState;
}

+ (id)unsavedColorForBackground;
+ (id)savedTextLabelFont;
+ (id)savedTimeLabelFont;
+ (id)timeLabelFont;
@property(readonly, copy, nonatomic) id <SCChatMessageState> messageState; // @synthesize messageState=_messageState;
@property(readonly, nonatomic) _Bool savedByCurrentUser; // @synthesize savedByCurrentUser=_savedByCurrentUser;
@property(readonly, nonatomic) _Bool isSaved; // @synthesize isSaved=_isSaved;
- (void).cxx_destruct;
- (id)savedColorForBackground;
- (id)saveOrUnsaveString;
- (_Bool)containsAllSavedMessages;
- (_Bool)shouldShowSaveOrUnsaveAnimation;
- (_Bool)shouldShowChatLabel;
- (_Bool)shouldShowSavedLabel;
- (double)widthForSenderLine;
- (id)colorForSavedBackground;
- (id)fontForTimeLabel;
- (void)updateMessageState:(id)arg1;
- (_Bool)isSavedBy:(id)arg1;
- (_Bool)isEqual:(id)arg1;
- (id)initWithMessage:(id)arg1 metadata:(id)arg2 props:(id)arg3;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

