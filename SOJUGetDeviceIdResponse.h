//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SOJUGetDeviceIdResponse.h"

@class NSString;

@interface SOJUGetDeviceIdResponse : NSObject <SOJUGetDeviceIdResponse>
{
    NSString *_dtoken1i;
    NSString *_dtoken1v;
}

@property(readonly, copy, nonatomic) NSString *dtoken1v; // @synthesize dtoken1v=_dtoken1v;
@property(readonly, copy, nonatomic) NSString *dtoken1i; // @synthesize dtoken1i=_dtoken1i;
- (void).cxx_destruct;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
- (_Bool)isEqual:(id)arg1;
- (void)encodeWithCoder:(id)arg1;
- (id)initWithCoder:(id)arg1;
- (id)copyWithZone:(struct _NSZone *)arg1;
- (id)initWithDtoken1i:(id)arg1 dtoken1v:(id)arg2;
- (id)toJson;
- (id)toDictionary;
- (id)initWithJSONDictionary:(id)arg1;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly) Class superclass;

@end

