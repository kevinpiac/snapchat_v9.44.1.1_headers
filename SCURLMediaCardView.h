//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCTextMediaCardView.h"

#import "SCBrowserViewControllerDelegate.h"
#import "SFSafariViewControllerDelegate.h"

@class NSString, NSURL, UILongPressGestureRecognizer, UITapGestureRecognizer, UIViewController<SKStoreProductViewControllerDelegate><SCChatCellMessageStateUpdateDelegate><SCChatCellURLLinkGestureDelegate>;

@interface SCURLMediaCardView : SCTextMediaCardView <SCBrowserViewControllerDelegate, SFSafariViewControllerDelegate>
{
    UITapGestureRecognizer *_tapGestureRecognizer;
    UILongPressGestureRecognizer *_longPressGestureRecognizer;
    NSURL *_url;
    UIViewController<SKStoreProductViewControllerDelegate><SCChatCellMessageStateUpdateDelegate><SCChatCellURLLinkGestureDelegate> *_parentVC;
    id <SCURLMediaCardViewDelegate> _delegate;
}

@property(retain, nonatomic) NSURL *url; // @synthesize url=_url;
- (void).cxx_destruct;
- (void)safariViewControllerDidFinish:(id)arg1;
- (void)browserViewIsClosing;
- (id)_copyActionForUrl:(id)arg1;
- (id)_webViewControllerWithURL:(id)arg1 cachedWebViewController:(id)arg2;
- (void)_openURL:(id)arg1 cachedWebViewController:(id)arg2;
- (void)onLongPress:(id)arg1;
- (void)onTap:(id)arg1;
- (void)dealloc;
- (id)urlMediaCardViewModel;
- (id)initWithViewModel:(id)arg1 parentVC:(id)arg2 delegate:(id)arg3 backgroundColor:(id)arg4;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

