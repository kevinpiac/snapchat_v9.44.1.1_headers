//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UIViewController.h"

#import "SCGalleryFullSearchResultViewControllerDelegate.h"
#import "SCGalleryHighlightGridControllerDelegate.h"
#import "SCGalleryItemEditActionControllerDelegate.h"
#import "SCGalleryItemEditViewCellDelegate.h"
#import "SCGalleryLagunaStoryStatusObserverDelegate.h"
#import "SCGalleryPreviewControllerDelegate.h"
#import "SCGalleryPrivateGallerySetupFlowDelegate.h"
#import "SCGallerySelectEntriesViewControllerDelegate.h"
#import "SCHydraViewControllerDelegate.h"
#import "SCTimeProfilable.h"
#import "UICollectionViewDataSource.h"
#import "UICollectionViewDelegate.h"
#import "UIGestureRecognizerDelegate.h"
#import "UINavigationControllerDelegate.h"
#import "UIViewControllerTransitioningDelegate.h"

@class NSArray, NSMutableSet, NSString, PHAsset, SCGalleryHighlightGridController, SCGalleryItemEditActionController, SCGalleryItemEditCollectionLayout, SCGalleryItemEditTimelineBar, SCGalleryItemEditViewCell, SCGalleryLagunaStoryStatusObserver, SCGalleryPreviewController, SCGalleryPrivateGallerySetupFlow, SCGallerySendController, SCUserSession, UICollectionView, UIPanGestureRecognizer, UITapGestureRecognizer, UIView;

@interface SCGalleryItemEditViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate, SCGalleryPreviewControllerDelegate, SCGalleryItemEditActionControllerDelegate, SCGalleryItemEditViewCellDelegate, SCGallerySelectEntriesViewControllerDelegate, SCGalleryPrivateGallerySetupFlowDelegate, SCGalleryFullSearchResultViewControllerDelegate, SCHydraViewControllerDelegate, SCTimeProfilable, SCGalleryHighlightGridControllerDelegate, SCGalleryLagunaStoryStatusObserverDelegate>
{
    NSArray *_snaps;
    id <SCGalleryEntry><SCGalleryItem> _entry;
    NSMutableSet *_highlightedSnapIdSet;
    unsigned long long _viewingSnapIndex;
    PHAsset *_photoAsset;
    UICollectionView *_collectionView;
    SCGalleryItemEditViewCell *_focusedView;
    SCGalleryItemEditCollectionLayout *_collectionLayout;
    UIPanGestureRecognizer *_upDownPanRecognizer;
    UIPanGestureRecognizer *_footerPanRecognizer;
    UITapGestureRecognizer *_tapRecognizer;
    SCGalleryItemEditActionController *_actionController;
    SCGalleryPreviewController *_previewController;
    SCGalleryPrivateGallerySetupFlow *_privateGallerySetupFlow;
    SCGallerySendController *_sendController;
    SCGalleryItemEditTimelineBar *_timelineBar;
    id <SCDataObjectObserveContext> _observeContext;
    double _pagingWidth;
    struct CGSize _itemSize;
    double _sideInset;
    _Bool _isDraggingTimelineBar;
    id <SCKeyServiceRequest> _makePrivateRequest;
    SCGalleryHighlightGridController *_highlightGridController;
    SCGalleryLagunaStoryStatusObserver *_lagunaLoadingStatusObserver;
    _Bool _showsHighlightedGrid;
    _Bool _highlightsUpdated;
    id <UIViewControllerAnimatedTransitioning> _staticDismissalAnimator;
    id <SCUIViewControllerAnimatedInteractiveTransitioning> _interactiveDismissalAnimator;
    _Bool _viewAppeared;
    _Bool _shouldPopToQuickActionForPullDownGesture;
    SCUserSession *_userSession;
    UIView *_footerProgressView;
    unsigned long long _editType;
    unsigned long long _editViewType;
    id <SCGalleryItemEditViewControllerMutationHandler> _mutationHandler;
    id <SCGalleryItemEditViewControllerDelegate> _delegate;
    id <SCStartChatDelegate> _startChatDelegate;
}

+ (long long)context;
+ (id)profiledSelectorNames;
+ (double)footerHeightForEditViewType:(unsigned long long)arg1;
+ (double)spaceBetweenContentAndHeaderForEditViewType:(unsigned long long)arg1;
@property(nonatomic) __weak id <SCStartChatDelegate> startChatDelegate; // @synthesize startChatDelegate=_startChatDelegate;
@property(nonatomic) __weak id <SCGalleryItemEditViewControllerDelegate> delegate; // @synthesize delegate=_delegate;
@property(nonatomic) __weak id <SCGalleryItemEditViewControllerMutationHandler> mutationHandler; // @synthesize mutationHandler=_mutationHandler;
@property(readonly, nonatomic) unsigned long long editViewType; // @synthesize editViewType=_editViewType;
@property(readonly, nonatomic) unsigned long long editType; // @synthesize editType=_editType;
@property(nonatomic) _Bool shouldPopToQuickActionForPullDownGesture; // @synthesize shouldPopToQuickActionForPullDownGesture=_shouldPopToQuickActionForPullDownGesture;
@property(readonly, nonatomic) UIView *footerProgressView; // @synthesize footerProgressView=_footerProgressView;
@property(readonly, nonatomic) SCGalleryItemEditActionController *actionController; // @synthesize actionController=_actionController;
@property(readonly, nonatomic) UICollectionView *collectionView; // @synthesize collectionView=_collectionView;
@property(readonly, nonatomic) SCUserSession *userSession; // @synthesize userSession=_userSession;
- (void).cxx_destruct;
- (void)_updateLagunaManagerTransferPriority;
- (id)_getAllSnapsMediaIds;
- (void)highlightGridController:(id)arg1 didIntendToZoomInSnapId:(id)arg2;
- (void)highlightGridController:(id)arg1 didUpdateHighlightedStateOfSnapId:(id)arg2 toBeHighlighted:(_Bool)arg3;
- (id)excludedProfiledSelectorNames;
- (void)_hideHydraController;
- (void)hydraViewControllerDismissed:(id)arg1 prefersAnimated:(_Bool)arg2;
- (unsigned long long)_lagunaActionOptions;
- (_Bool)_isHDContentTransferInProgress;
- (_Bool)_isHDContentAvailableForTransferFromLagunaDevice;
- (_Bool)_isHDContentAvailableLocallyOrCanBeTransferredFromMemoriesCloudStorage;
- (void)_updateCollectionViewAndTimelineForSnaps:(id)arg1;
- (void)_setupActionControllerAndFooterForEntry;
- (id)_attributionNameForSnap:(id)arg1;
- (void)_setupActionControllerAndFooterForPhotoAsset;
- (void)_setupActionControllerAndFooter;
- (id)_popToThumbnailViewAnimated:(_Bool)arg1;
- (void)_popToThumbnailViewWhenLocked:(_Bool)arg1 underlyingView:(id)arg2;
- (void)_popToQuickActionInteractive:(_Bool)arg1;
- (void)_popViewControllerAnimated:(_Bool)arg1 interactive:(_Bool)arg2;
- (void)_scrollTimelineBarBasedOnContentOffset;
- (_Bool)_shouldHideFooterBar;
- (long long)_currentCenterCellIndex;
- (void)_scrollToClosestCellAnimated:(_Bool)arg1 completionBlock:(CDUnknownBlockType)arg2;
- (void)_updateCellsBasedOnContentOffset;
- (void)_promptToConfirmAndSetItemToPrivate;
- (double)_angleOfPanRecognizer:(id)arg1 inView:(id)arg2;
- (void)_transitionToEntry:(id)arg1 snaps:(id)arg2;
- (void)_handleFooterPan:(id)arg1;
- (void)_handleTap:(id)arg1;
- (void)_handleDownPan:(id)arg1;
- (_Bool)gestureRecognizerShouldBegin:(id)arg1;
- (_Bool)gestureRecognizer:(id)arg1 shouldRecognizeSimultaneouslyWithGestureRecognizer:(id)arg2;
- (_Bool)gestureRecognizer:(id)arg1 shouldReceiveTouch:(id)arg2;
- (void)_setCollectionViewAndFooterMovedDown:(_Bool)arg1 animated:(_Bool)arg2;
- (void)galleryItemEditActionViewControllerDidPressSetItemToPublic:(id)arg1;
- (void)galleryItemEditActionViewControllerDidPressSetItemToPrivate:(id)arg1;
- (void)galleryItemEditActionViewControllerDidPressHydraSearchStory:(id)arg1;
- (void)galleryItemEditActionViewControllerDidPressCreateStory:(id)arg1;
- (void)galleryFullSearchResultViewControllerDidPressBackButton:(id)arg1;
- (void)storyLoadingStatusObserver:(id)arg1 didUpdateContentLoadingProgress:(double)arg2;
- (void)storyLoadingStatusObserver:(id)arg1 didUpdateContentLoadingStatus:(long long)arg2;
- (void)_unobserveLagunaLoadingStates;
- (void)_observeLagunaLoadingStates;
- (void)galleryItemEditActionViewControllerDidPressGetInHD:(id)arg1;
- (void)dismissHighlightGridViewAnimated:(_Bool)arg1 completion:(CDUnknownBlockType)arg2;
- (void)presentHighlightGridViewAnimated:(_Bool)arg1 completion:(CDUnknownBlockType)arg2;
- (void)galleryItemEditActionViewControllerDidPressEditHighlights:(id)arg1;
- (void)galleryItemEditActionViewControllerDidPressAddToSaps:(id)arg1;
- (void)galleryItemEditActionViewControllerDidRenameSaps:(id)arg1 withName:(id)arg2;
- (void)galleryItemEditActionViewControllerDidPressAddToStory:(id)arg1;
- (void)galleryItemEditActionViewControllerDidPressDeleteStory:(id)arg1;
- (void)galleryItemEditActionViewControllerDidPressShareItem:(id)arg1;
- (_Bool)areEntryAndSnapsPersistedLocally:(id)arg1;
- (_Bool)isEntryPersistedLocally:(id)arg1;
- (void)galleryItemEditActionViewControllerDidPressSendStory:(id)arg1;
- (void)galleryItemEditActionViewControllerDidRenameStory:(id)arg1;
- (void)galleryItemEditActionViewControllerDidTapBackButton:(id)arg1;
- (void)galleryItemEditActionViewControllerWillCollapseDropdownMenu:(id)arg1;
- (void)galleryItemEditActionViewControllerWillExpandDropdownMenu:(id)arg1;
- (void)gallerySelectEntriesViewControllerDidPressDone:(id)arg1 selectedItems:(id)arg2;
- (void)privateGallerySetupFlowDidFinish:(id)arg1;
- (void)privateGallerySetupFlowDidCancel:(id)arg1;
- (void)galleryPreviewController:(id)arg1 createdEntry:(id)arg2 createdSnap:(id)arg3 updatedEntry:(id)arg4 didDetachSnap:(id)arg5 fromEntry:(id)arg6;
- (void)galleryPreviewController:(id)arg1 updatedEntry:(id)arg2 didDeleteSnap:(id)arg3 fromEntry:(id)arg4;
- (void)itemEditViewCellDidPressHighlight:(id)arg1;
- (void)itemEditViewCellDidPressSend:(id)arg1;
- (void)itemEditViewCellDidPressEdit:(id)arg1;
- (void)itemEditViewCellDidPressDelete:(id)arg1;
- (void)itemEditViewCellDidPressExport:(id)arg1;
- (id)navigationController:(id)arg1 interactionControllerForAnimationController:(id)arg2;
- (id)navigationController:(id)arg1 animationControllerForOperation:(long long)arg2 fromViewController:(id)arg3 toViewController:(id)arg4;
- (id)animationControllerForDismissedController:(id)arg1;
- (id)animationControllerForPresentedController:(id)arg1 presentingController:(id)arg2 sourceController:(id)arg3;
- (void)scrollViewDidEndScrollingAnimation:(id)arg1;
- (void)scrollViewDidEndDecelerating:(id)arg1;
- (void)scrollViewDidEndDragging:(id)arg1 willDecelerate:(_Bool)arg2;
- (void)scrollViewDidScroll:(id)arg1;
- (id)collectionView:(id)arg1 cellForItemAtIndexPath:(id)arg2;
- (long long)collectionView:(id)arg1 numberOfItemsInSection:(long long)arg2;
- (void)collectionView:(id)arg1 didEndDisplayingCell:(id)arg2 forItemAtIndexPath:(id)arg3;
- (void)collectionView:(id)arg1 willDisplayCell:(id)arg2 forItemAtIndexPath:(id)arg3;
- (_Bool)prefersStatusBarHidden;
- (void)viewDidDisappear:(_Bool)arg1;
- (void)viewWillDisappear:(_Bool)arg1;
- (void)viewDidAppear:(_Bool)arg1;
- (id)viewingSnapCell;
- (id)actionHeaderView;
- (void)viewWillAppear:(_Bool)arg1;
- (void)viewDidLoad;
- (void)dealloc;
- (void)_updateEntry:(id)arg1 changedKeys:(id)arg2;
- (void)_unobserveChanges;
- (void)_observeChanges;
- (id)sendController;
- (id)previewController;
- (id)initWithUserSession:(id)arg1 item:(id)arg2 subItems:(id)arg3 selectedSubItem:(id)arg4 editType:(unsigned long long)arg5 editViewType:(unsigned long long)arg6;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

