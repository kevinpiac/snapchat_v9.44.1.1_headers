//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "FBTweakObserver.h"

@class NSDate, NSString;

@interface ContextRankingTweaks : NSObject <FBTweakObserver>
{
    NSDate *_requestTime;
}

+ (id)shared;
- (void).cxx_destruct;
- (void)tweakDidChange:(id)arg1;
- (void)setupTweaksObserver;
- (void)tweaks;
- (id)getCustomRequestTime;
- (void)setCustomRequestTime:(id)arg1;
- (id)overrideLocationFBTweak;
- (void)addOverrideLocation:(double)arg1 longitude:(double)arg2 name:(id)arg3;
- (void)updateOverrideLocationTweaks:(id)arg1;
- (void)updateTweaks:(id)arg1;
- (void)prepare;
- (id)init;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

