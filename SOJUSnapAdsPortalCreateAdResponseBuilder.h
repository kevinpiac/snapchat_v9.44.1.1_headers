//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSNumber, NSString, SOJUSnapAdsPortalAd;

@interface SOJUSnapAdsPortalCreateAdResponseBuilder : NSObject
{
    NSNumber *_success;
    NSString *_errorReason;
    SOJUSnapAdsPortalAd *_ad;
}

+ (id)withJUSnapAdsPortalCreateAdResponse:(id)arg1;
- (void).cxx_destruct;
- (id)setAd:(id)arg1;
- (id)setErrorReason:(id)arg1;
- (id)setSuccess:(id)arg1;
- (id)build;
- (id)setErrorReasonEnum:(long long)arg1;
- (id)setSuccessValue:(_Bool)arg1;

@end

