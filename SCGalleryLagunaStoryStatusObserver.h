//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCGalleryLagunaContentListener.h"

@class NSArray, NSString, SCUserSession;

@interface SCGalleryLagunaStoryStatusObserver : NSObject <SCGalleryLagunaContentListener>
{
    id <SCGalleryEntry> _entry;
    SCUserSession *_userSession;
    id <SCGalleryLagunaStoryStatusObserverDelegate> _delegate;
    NSArray *_loaders;
    id <SCDataObjectObserveContext> _entryObserveContext;
    _Bool _observingStatus;
    unsigned long long _observeComponents;
    long long _currentStatus;
}

+ (id)dispatchQueue;
- (void).cxx_destruct;
- (id)_logStringForStatus:(long long)arg1;
- (void)dealloc;
- (void)didInterruptDownloadForContentComponent:(unsigned long long)arg1;
- (void)didFinishDownloadForContentComponent:(unsigned long long)arg1;
- (void)didPauseForContentComponent:(unsigned long long)arg1;
- (void)didReceiveDataForContentComponent:(unsigned long long)arg1;
- (void)_calculateCurrentState;
- (void)_calculateCurrentStateWithLatestEntry:(id)arg1 latestLoaders:(id)arg2;
- (void)stopObservingStatus;
- (void)startObservingStatus;
- (void)setDelegate:(id)arg1;
- (id)initWithEntry:(id)arg1 observeComponents:(unsigned long long)arg2 userSession:(id)arg3;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

