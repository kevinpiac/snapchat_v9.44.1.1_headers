//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSNumber, NSString;

@interface SOJUSecurityRetrieveKeyResponseBuilder : NSObject
{
    NSString *_key;
    NSNumber *_rateLimitExpiration;
    NSNumber *_currentTimestamp;
}

+ (id)withJUSecurityRetrieveKeyResponse:(id)arg1;
- (void).cxx_destruct;
- (id)setCurrentTimestamp:(id)arg1;
- (id)setRateLimitExpiration:(id)arg1;
- (id)setKey:(id)arg1;
- (id)build;
- (id)setCurrentTimestampValue:(long long)arg1;
- (id)setRateLimitExpirationValue:(long long)arg1;

@end

