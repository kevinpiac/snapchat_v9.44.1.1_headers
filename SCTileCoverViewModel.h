//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSString, UIColor, UIImage;

@interface SCTileCoverViewModel : NSObject
{
    _Bool _isFullyViewed;
    NSString *_coverId;
    NSString *_hashIdentifier;
    NSString *_headline;
    UIImage *_image;
    NSString *_imageURLString;
    double _cropPercentage;
    UIColor *_color;
}

@property(readonly, nonatomic) UIColor *color; // @synthesize color=_color;
@property(readonly, nonatomic) double cropPercentage; // @synthesize cropPercentage=_cropPercentage;
@property(readonly, nonatomic) _Bool isFullyViewed; // @synthesize isFullyViewed=_isFullyViewed;
@property(readonly, copy, nonatomic) NSString *imageURLString; // @synthesize imageURLString=_imageURLString;
@property(readonly, nonatomic) UIImage *image; // @synthesize image=_image;
@property(readonly, copy, nonatomic) NSString *headline; // @synthesize headline=_headline;
@property(readonly, copy, nonatomic) NSString *hashIdentifier; // @synthesize hashIdentifier=_hashIdentifier;
@property(readonly, copy, nonatomic) NSString *coverId; // @synthesize coverId=_coverId;
- (void).cxx_destruct;
- (unsigned long long)hash;
- (void)clearImage;
- (id)initWithCoverId:(id)arg1 headline:(id)arg2 image:(id)arg3 imageURLString:(id)arg4 isFullyViewed:(_Bool)arg5 cropPercentage:(double)arg6 color:(id)arg7;

@end

