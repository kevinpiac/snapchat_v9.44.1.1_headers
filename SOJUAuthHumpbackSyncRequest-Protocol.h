//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSCoding.h"
#import "NSCopying.h"
#import "NSObject.h"

@class NSNumber, NSString;

@protocol SOJUAuthHumpbackSyncRequest <NSObject, NSCoding, NSCopying>
@property(readonly, copy, nonatomic) NSNumber *fsnAuthTokenVersion;
@property(readonly, copy, nonatomic) NSString *authToken;
@property(readonly, copy, nonatomic) NSNumber *oldPasswordVersion;
@property(readonly, copy, nonatomic) NSString *oldPasswordHash;
@property(readonly, copy, nonatomic) NSNumber *passwordVersion;
@property(readonly, copy, nonatomic) NSString *passwordHash;
@property(readonly, copy, nonatomic) NSNumber *emailVersion;
@property(readonly, copy, nonatomic) NSString *email;
@property(readonly, copy, nonatomic) NSNumber *usernameVersion;
@property(readonly, copy, nonatomic) NSString *username;
@property(readonly, copy, nonatomic) NSString *userId;
@end

