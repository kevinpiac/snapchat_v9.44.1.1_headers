//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSCoding.h"
#import "NSCopying.h"
#import "NSObject.h"

@class NSArray, NSNumber, NSString;

@protocol SOJULagunaBackwardCompatResponse <NSObject, NSCoding, NSCopying>
@property(readonly, copy, nonatomic) NSArray *lagunaIds;
@property(readonly, copy, nonatomic) NSArray *enableGeofenceIds;
@property(readonly, copy, nonatomic) NSNumber *profileTimeoutSecs;
@property(readonly, copy, nonatomic) NSString *updateToastIconUrl;
@property(readonly, copy, nonatomic) NSString *updateToastSecondaryLabel;
@property(readonly, copy, nonatomic) NSString *updateToastPrimaryLabel;
@property(readonly, copy, nonatomic) NSString *settingsButton;
@property(readonly, copy, nonatomic) NSString *settingsUpdateDescription;
@property(readonly, copy, nonatomic) NSString *settingsImageUrl;
@property(readonly, copy, nonatomic) NSString *settingsTitle;
@property(readonly, copy, nonatomic) NSString *settingsRowAccessibilityLabel;
@property(readonly, copy, nonatomic) NSString *settingsRowLabel;
@property(readonly, copy, nonatomic) NSNumber *enabled;
@end

