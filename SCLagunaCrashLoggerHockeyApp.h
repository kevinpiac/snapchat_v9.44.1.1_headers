//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCLagunaCrashLogger.h"

@class Manager, NSString, SCRequestManager;

@interface SCLagunaCrashLoggerHockeyApp : NSObject <SCLagunaCrashLogger>
{
    Manager *_manager;
    SCRequestManager *_requestManager;
}

- (void).cxx_destruct;
- (id)appendRequestBody:(id)arg1 filePath:(id)arg2 fileParam:(id)arg3 boundaryConstant:(id)arg4;
- (void)deleteCrashFile:(id)arg1;
- (_Bool)saveCrashInfoToFile:(id)arg1 filePath:(id)arg2;
- (id)assembleCrashInformationWithPackage:(id)arg1 version:(id)arg2 OSVersion:(id)arg3 model:(id)arg4 crashDate:(id)arg5 deviceId:(id)arg6 crashReasons:(id)arg7 crashStacks:(id)arg8;
- (void)uploadCrashInformation:(id)arg1 descriptionFilePath:(id)arg2 attachmentFilePaths:(id)arg3 userId:(id)arg4 contact:(id)arg5;
- (id)zipFiles:(id)arg1;
- (void)sendCrashWithPackage:(id)arg1 version:(id)arg2 OSVersion:(id)arg3 model:(id)arg4 crashDate:(id)arg5 deviceId:(id)arg6 crashReasons:(id)arg7 crashStacks:(id)arg8 description:(id)arg9 attachmentFilePaths:(id)arg10 userId:(id)arg11 contact:(id)arg12;
- (void)sendCrashWithCrashInfo:(id)arg1;
- (id)initWithManager:(id)arg1 requestManager:(id)arg2;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

