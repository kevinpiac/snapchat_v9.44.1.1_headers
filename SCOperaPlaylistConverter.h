//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSDictionary;

@interface SCOperaPlaylistConverter : NSObject
{
    NSDictionary *_mediaTypeConfigurations;
}

- (void).cxx_destruct;
- (void)unresolvePlaylistItemGroup:(id)arg1;
- (void)resolvePlaylistItemGroupIfNecessary:(id)arg1;
- (void)teardownViewModelForItem:(id)arg1;
- (id)viewModelForItem:(id)arg1;
- (id)initWithMediaTypeConfigurations:(id)arg1;

@end

