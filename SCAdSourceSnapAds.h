//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCAdSource.h"

#import "SCAdSourceImpl.h"

@class NSMutableDictionary, NSOperationQueue, NSString, NSTimer, SCAdResolutionLatencyTracker, SCUnusedAdTracker;

@interface SCAdSourceSnapAds : SCAdSource <SCAdSourceImpl>
{
    SCAdResolutionLatencyTracker *_latencyTracker;
    SCUnusedAdTracker *_unusedAdTracker;
    NSTimer *_initTimeToLiveTimer;
    _Bool _isActive;
    _Bool _fireInitOnActive;
    NSString *_adRequestBaseUrl;
    NSString *_adBatchRequestBaseUrl;
    NSString *_adTrackingBaseUrl;
    NSString *_timeoutTrackingBaseUrl;
    NSString *_initializeRequestBaseUrl;
    NSOperationQueue *_networkRequestQueue;
    NSMutableDictionary *_snapAdRequests;
    NSMutableDictionary *_snapAdResponses;
    NSMutableDictionary *_gclbCookies;
}

@property(retain, nonatomic) NSMutableDictionary *gclbCookies; // @synthesize gclbCookies=_gclbCookies;
@property(retain, nonatomic) NSMutableDictionary *snapAdResponses; // @synthesize snapAdResponses=_snapAdResponses;
@property(retain, nonatomic) NSMutableDictionary *snapAdRequests; // @synthesize snapAdRequests=_snapAdRequests;
@property(retain, nonatomic) NSOperationQueue *networkRequestQueue; // @synthesize networkRequestQueue=_networkRequestQueue;
@property(retain, nonatomic) NSString *initializeRequestBaseUrl; // @synthesize initializeRequestBaseUrl=_initializeRequestBaseUrl;
@property(retain, nonatomic) NSString *timeoutTrackingBaseUrl; // @synthesize timeoutTrackingBaseUrl=_timeoutTrackingBaseUrl;
@property(retain, nonatomic) NSString *adTrackingBaseUrl; // @synthesize adTrackingBaseUrl=_adTrackingBaseUrl;
@property(retain, nonatomic) NSString *adBatchRequestBaseUrl; // @synthesize adBatchRequestBaseUrl=_adBatchRequestBaseUrl;
@property(retain, nonatomic) NSString *adRequestBaseUrl; // @synthesize adRequestBaseUrl=_adRequestBaseUrl;
- (void).cxx_destruct;
- (id)description;
- (void)fireInitRequest;
- (void)onApplicationDidBecomeActive;
- (void)onApplicationWillResignActive;
- (void)initTimeToLiveExpired;
- (void)handleInitFailure:(id)arg1 error:(id)arg2;
- (void)handleInitSuccess:(id)arg1 data:(id)arg2;
- (void)handleNetworkCompletion:(id)arg1 responseCode:(long long)arg2;
- (id)adTrackURL;
- (id)adRequestURL;
- (id)prepareAdRequestDataWithAdController:(id)arg1 contextualTargetingParams:(id)arg2;
- (void)fireAdResolutionBatchRequest:(id)arg1 userTargetingParams:(id)arg2 contextualTargetingParams:(id)arg3;
- (void)fireAdResolutionRequest:(id)arg1 userTargetingParams:(id)arg2 contextualTargetingParams:(id)arg3;
- (void)adRequestErrorHandlerOnMainThreadWithSCAdTransformResponse:(id)arg1 adController:(id)arg2;
- (void)adBatchRequestErrorHandlerOnMainThread:(id)arg1 networkError:(id)arg2 adControllerList:(id)arg3 snapAdBatchRequest:(id)arg4;
- (void)adRequestErrorHandlerOnMainThread:(id)arg1 networkError:(id)arg2 adController:(id)arg3 snapAdRequest:(id)arg4;
- (void)adRequestCompletionHandlerOnMainThreadWithResponseData:(id)arg1 adController:(id)arg2;
- (void)adBatchRequestCompletionHandlerOnMainThread:(id)arg1 networkResponse:(id)arg2 adControllerList:(id)arg3 snapAdBatchRequest:(id)arg4;
- (void)adRequestCompletionHandlerOnMainThread:(id)arg1 networkResponse:(id)arg2 adController:(id)arg3 snapAdRequest:(id)arg4;
- (id)getGCLBCookieHeaderByUrl:(id)arg1;
- (id)getGCLBCookieFromHTTPResponse:(id)arg1;
- (void)updateGCLBCookieWithHTTPURLResponse:(id)arg1;
- (id)getStoryMessage:(id)arg1;
- (id)getLocalWebViewMessage:(id)arg1;
- (id)getRemoteWebViewMessage:(id)arg1;
- (id)getLongformVideoImpressionMessage:(id)arg1;
- (id)getAppInstallImpressionMessage:(id)arg1;
- (id)getThreeVImpressionMessage:(id)arg1;
- (void)recordImpression:(id)arg1 impressionData:(id)arg2;
- (void)adTrackErrorHandlerOnMainThread:(id)arg1 networkError:(id)arg2 adRequestClientId:(id)arg3;
- (void)adTrackCompletionHandlerOnMainThread:(id)arg1 networkResponse:(id)arg2 adRequestClientId:(id)arg3;
- (void)handleImpressionTrackBeforeAdResponseReady:(id)arg1;
- (void)cleanUpAfterRequestComplete:(id)arg1;
- (void)recordAbandonedAd:(id)arg1;
- (_Bool)canRecordImpression:(id)arg1;
- (void)saveEndpoints;
- (void)updateFromSource:(id)arg1;
- (void)delayInit;
- (id)initWithEndpoints:(id)arg1 adBatchRequestBaseUrl:(id)arg2 adTrackingBaseUrl:(id)arg3 timeoutTrackingBaseUrl:(id)arg4 initRequestBaseUrl:(id)arg5 supportsBatchRequest:(_Bool)arg6 name:(id)arg7 type:(long long)arg8 adManager:(id)arg9;

@end

