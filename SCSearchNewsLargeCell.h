//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UICollectionViewCell.h"

@class SCSearchNewsViewModel, UIImageView, UILabel;

@interface SCSearchNewsLargeCell : UICollectionViewCell
{
    UIImageView *_logoImageView;
    UIImageView *_imageView;
    UILabel *_headline;
    SCSearchNewsViewModel *_viewModel;
}

@property(retain, nonatomic) SCSearchNewsViewModel *viewModel; // @synthesize viewModel=_viewModel;
- (void).cxx_destruct;
- (void)setupViewModel:(id)arg1;
- (id)initWithFrame:(struct CGRect)arg1;

@end

