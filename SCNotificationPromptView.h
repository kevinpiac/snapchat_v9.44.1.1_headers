//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UIButton.h"

@class UILabel;

@interface SCNotificationPromptView : UIButton
{
    UILabel *_iconLabel;
    UILabel *_titleLabel;
    UILabel *_descriptionLabel;
    UIButton *_dismissButton;
    id <SCNotificationPromptDelegate> _delegate;
}

+ (double)_heightForWidthConstraint:(double)arg1;
@property(nonatomic) __weak id <SCNotificationPromptDelegate> delegate; // @synthesize delegate=_delegate;
- (void).cxx_destruct;
- (void)_initDismissButton;
- (void)_initDescriptionLabel;
- (void)_initTitleLabel;
- (void)_initIconLabel;
- (void)dismissPressed;
- (void)headerPressed;
- (id)initWithWidth:(double)arg1;

@end

