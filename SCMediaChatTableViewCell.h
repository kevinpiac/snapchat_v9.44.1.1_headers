//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCSavableItemChatTableViewCell.h"

#import "SCActionMenuRenderableCell.h"
#import "SCExpandableMediaChatTableCell.h"

@class NSString, UIView, UIView<SCChatSingleMediaThumbnailView>;

@interface SCMediaChatTableViewCell : SCSavableItemChatTableViewCell <SCActionMenuRenderableCell, SCExpandableMediaChatTableCell>
{
    UIView *_mediaContainerView;
    UIView<SCChatSingleMediaThumbnailView> *_mediaThumbnailView;
}

@property(readonly, nonatomic) UIView<SCChatSingleMediaThumbnailView> *mediaThumbnailView; // @synthesize mediaThumbnailView=_mediaThumbnailView;
- (void).cxx_destruct;
- (void)resetWithOriginalContent;
- (id)actionMenuContentView;
- (void)configureWithActionMenuVC:(id)arg1;
- (_Bool)fullScreenIsShown;
- (void)dismissFullScreenView;
- (void)loadVideoIfNecessary;
- (void)renderMetadata;
- (void)_updateThumbnailSizeBaseOnViewModel;
- (void)renderPayload;
- (void)_initMediaViewWithParentVC:(id)arg1 delegate:(id)arg2 thumbnailType:(long long)arg3;
- (id)initWithStyle:(long long)arg1 reuseIdentifier:(id)arg2 parentVC:(id)arg3 delegate:(id)arg4 thumbnailType:(long long)arg5;
- (id)mediaViewModel;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

