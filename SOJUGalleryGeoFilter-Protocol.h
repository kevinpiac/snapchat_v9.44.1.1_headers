//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSCoding.h"
#import "NSCopying.h"
#import "NSObject.h"

@class NSArray, NSDictionary, NSNumber, NSString;

@protocol SOJUGalleryGeoFilter <NSObject, NSCoding, NSCopying>
@property(readonly, copy, nonatomic) NSNumber *isSponsored;
@property(readonly, copy, nonatomic) NSNumber *belowDrawingLayer;
@property(readonly, copy, nonatomic) NSString *positionSetting;
@property(readonly, copy, nonatomic) NSString *scaleSetting;
@property(readonly, copy, nonatomic) NSArray *dynamicContent;
@property(readonly, copy, nonatomic) NSDictionary *imageUrlParams;
@property(readonly, copy, nonatomic) NSString *imageUrl;
@property(readonly, copy, nonatomic) NSString *idValue;
@property(readonly, copy, nonatomic) NSString *unlockableContentType;
@property(readonly, copy, nonatomic) NSString *type;
@end

