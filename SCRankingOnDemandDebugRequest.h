//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class AFHTTPRequestOperation, NSString, SCRGetOnDemandDebugRequest;

@interface SCRankingOnDemandDebugRequest : NSObject
{
    NSString *_requestId;
    SCRGetOnDemandDebugRequest *_prequest;
    AFHTTPRequestOperation *_directRankingServiceRequestOperation;
}

- (void).cxx_destruct;
- (CDUnknownBlockType)_getSuccessBlockWithCompletion:(CDUnknownBlockType)arg1;
- (void)executeWithCompletion:(CDUnknownBlockType)arg1;
- (id)initWithSession:(id)arg1 type:(int)arg2 storyId:(id)arg3 suggestId:(id)arg4;

@end

