//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCAUserTrackedEvent.h"

#import "MapSerializable.h"

@class NSNumber, NSString;

@interface SCADiscoverSnapViewBase : SCAUserTrackedEvent <MapSerializable>
{
    NSNumber *snapIndexCount;
    NSNumber *snapIndexPos;
    NSNumber *timeViewed;
    NSNumber *playbackVolume;
    long long exitEvent;
    long long source;
    long long playbackAudio;
    NSString *editionId;
    NSString *publisherId;
    NSString *dsnapId;
    NSString *scanActionId;
    NSString *deepLinkId;
}

+ (id)copy:(id)arg1;
- (void).cxx_destruct;
- (id)asDictionary;
- (id)getDeepLinkId;
- (void)setDeepLinkId:(id)arg1;
- (id)getScanActionId;
- (void)setScanActionId:(id)arg1;
- (id)getDsnapId;
- (void)setDsnapId:(id)arg1;
- (id)getPublisherId;
- (void)setPublisherId:(id)arg1;
- (id)getEditionId;
- (void)setEditionId:(id)arg1;
- (long long)getPlaybackAudio;
- (void)setPlaybackAudio:(long long)arg1;
- (long long)getSource;
- (void)setSource:(long long)arg1;
- (long long)getExitEvent;
- (void)setExitEvent:(long long)arg1;
- (double)getPlaybackVolume;
- (void)setPlaybackVolume:(double)arg1;
- (double)getTimeViewed;
- (void)setTimeViewed:(double)arg1;
- (long long)getSnapIndexPos;
- (void)setSnapIndexPos:(long long)arg1;
- (long long)getSnapIndexCount;
- (void)setSnapIndexCount:(long long)arg1;
- (id)init;

@end

