//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UICollectionViewCell.h"

#import "SCGalleryEntryThumbnailGeneratorDelegate.h"
#import "SCGalleryEntryViewCellProtocol.h"
#import "SCGalleryLagunaStoryCellTransitioning.h"
#import "SCGalleryLagunaStoryStatusObserverDelegate.h"

@class NSString, SCGalleryDebugSyncStatusChecker, SCGalleryDebugSyncStatusView, SCGalleryEntryThumbnailGenerator, SCGalleryLagunaStoryStatusObserver, SCLoadingIndicatorView, SCSentinel, SCUserSession, UIImageView, UIView;

@interface SCGalleryLagunaStoryViewCell : UICollectionViewCell <SCGalleryEntryThumbnailGeneratorDelegate, SCGalleryLagunaStoryStatusObserverDelegate, SCGalleryLagunaStoryCellTransitioning, SCGalleryEntryViewCellProtocol>
{
    UIView *_wrapperView;
    UIView *_highlightCornerView;
    UIView *_dayStorySelectedOverlayView;
    UIImageView *_dayStoryImageView;
    id <SCGalleryEntry> _lagunaStoryEntry;
    SCLoadingIndicatorView *_spinner;
    double _delayOfUpdatingStoryThumbnail;
    _Bool _contentsUnloaded;
    _Bool _selecting;
    SCSentinel *_sentinel;
    SCUserSession *_userSession;
    CDStruct_bac8f6e9 _mediaScenePath;
    SCGalleryEntryThumbnailGenerator *_thumbnailGenerator;
    unsigned long long _selectionMode;
    struct CGAffineTransform _dayStorySelectionScale;
    struct CGAffineTransform _dayStoryLongPressScale;
    SCGalleryLagunaStoryStatusObserver *_lagunaStoryObserver;
    long long _loadingStatus;
    SCGalleryDebugSyncStatusChecker *_debugSyncStatusChecker;
    SCGalleryDebugSyncStatusView *_debugSyncStatusView;
}

- (void).cxx_destruct;
- (void)_adjustLoadingIndicatorBasedOnStatus:(long long)arg1;
- (void)_adjustImageViewAlphaBasedOnStatus:(long long)arg1;
- (id)transitionImageViewForGalleryBrowseType:(unsigned long long)arg1;
- (void)storyLoadingStatusObserver:(id)arg1 didUpdateContentLoadingProgress:(double)arg2;
- (void)storyLoadingStatusObserver:(id)arg1 didUpdateContentLoadingStatus:(long long)arg2;
- (struct CGAffineTransform)_dayStoryImageViewTransform;
- (void)_updateDayStoryImages:(id)arg1;
- (void)thumbnailGenerator:(id)arg1 didUpdateSnapThumbnailWithImageGenerating:(id)arg2 duration:(double)arg3 fromCache:(_Bool)arg4 isFramed:(_Bool)arg5;
- (void)thumbnailGenerator:(id)arg1 didUpdateStoryThumbnailWithImage:(id)arg2 fromCache:(_Bool)arg3 isFramed:(_Bool)arg4;
- (void)thumbnailGenerator:(id)arg1 didUpdateStoryThumbnailWithImage:(id)arg2 highlightedImage:(id)arg3 fromCache:(_Bool)arg4 isFramed:(_Bool)arg5;
- (void)exitSelectionMode;
- (void)enterSelectionMode:(unsigned long long)arg1;
- (void)animateLongTapForTouchLocation:(struct CGPoint)arg1 reverse:(_Bool)arg2;
- (void)setSelectedWithMode:(unsigned long long)arg1 selectOverlayImage:(id)arg2;
- (unsigned long long)interactionMode;
- (unsigned long long)selectionModeAtTouchLocation:(struct CGPoint)arg1;
- (void)toggleDebugSyncStatusView;
- (void)stopGeneratingUpdates;
- (void)startGeneratingUpdates;
- (void)setEntry:(id)arg1 format:(unsigned long long)arg2 mediaScenePath:(CDStruct_bac8f6e9)arg3 delayOfUpdatingStoryThumbnail:(double)arg4 contentsUnloaded:(_Bool)arg5 selectMode:(unsigned long long)arg6 debugSyncStatusChecker:(id)arg7 userSession:(id)arg8;
- (id)expandingViewForMode:(unsigned long long)arg1;
- (id)imageForMode:(unsigned long long)arg1;
- (void)layoutSubviews;
- (void)dealloc;
- (void)prepareForReuse;
- (id)initWithFrame:(struct CGRect)arg1;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

