//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UIViewController.h"

#import "SCLagunaOnboardingScrollViewDelegate.h"
#import "SCTimeProfilable.h"
#import "UIScrollViewDelegate.h"

@class AVPlayer, AVPlayerLayer, NSArray, NSString, SCLagunaOnboardingScrollView, UIImageView, UILabel;

@interface SCLagunaOnboardingViewController : UIViewController <SCLagunaOnboardingScrollViewDelegate, UIScrollViewDelegate, SCTimeProfilable>
{
    SCLagunaOnboardingScrollView *_scrollView;
    long long _lastPage;
    UIImageView *_fallbackImageView;
    NSArray *_fallbackImages;
    UILabel *_nextLabel;
    AVPlayer *_avPlayer;
    AVPlayerLayer *_avPlayerLayer;
    _Bool _scrollInProgress;
}

+ (long long)context;
+ (id)profiledSelectorNames;
+ (void)_downloadOnboardingVideo:(id)arg1 completionBlock:(CDUnknownBlockType)arg2;
+ (_Bool)_isOnboardingVideoDownloaded;
+ (id)_resourceDirectory;
+ (id)_onboardingVideoUrl;
+ (void)warmup;
@property(nonatomic) _Bool scrollInProgress; // @synthesize scrollInProgress=_scrollInProgress;
- (void).cxx_destruct;
- (void)_updateNextLabel;
- (void)_updateFallbackImage:(_Bool)arg1;
- (void)_adjustVideoWhileScrubbing;
- (void)_updateAVPlayer:(_Bool)arg1;
- (void)_rollFilm;
- (void)handleScrollViewTap:(id)arg1;
- (void)scrollViewDidScroll:(id)arg1;
- (void)scrollViewDidEndDecelerating:(id)arg1;
- (void)scrollViewWillBeginDragging:(id)arg1;
- (void)handleDoneButton;
- (double)_determineMetric:(double [3])arg1 forHeight:(double)arg2;
- (unsigned long long)supportedInterfaceOrientations;
- (void)appEnteredForeground;
- (_Bool)prefersStatusBarHidden;
- (void)viewDidLoad;
- (void)viewWillDisappear:(_Bool)arg1;
- (void)viewWillAppear:(_Bool)arg1;
- (void)loadView;
- (id)init;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

