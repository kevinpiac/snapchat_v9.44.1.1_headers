//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCChunkItem.h"

@class NSDictionary, NSURL;

@interface SCChunkRemoteWebpageItem : SCChunkItem
{
    _Bool _allowJavascriptInjection;
    NSURL *_url;
    NSDictionary *_properties;
}

@property(readonly, copy, nonatomic) NSDictionary *properties; // @synthesize properties=_properties;
@property(readonly, nonatomic) _Bool allowJavascriptInjection; // @synthesize allowJavascriptInjection=_allowJavascriptInjection;
@property(readonly, copy, nonatomic) NSURL *url; // @synthesize url=_url;
- (void).cxx_destruct;
- (void)saveMediaToCache;
- (id)cacheMediaIds;
- (void)encodeWithCoder:(id)arg1;
- (id)initWithCoder:(id)arg1;
- (id)initWithJSONDictionary:(id)arg1 chunkDictionary:(id)arg2;

@end

