//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "WKScriptMessageHandler.h"

@class NSString, SCOperaUserInfoProvider, SCOperaWebView, SCPromptView, WKUserContentController;

@interface SCOperaRemoteWebJavascriptBridge : NSObject <WKScriptMessageHandler>
{
    SCPromptView *_promptView;
    SCOperaUserInfoProvider *_userInfoProvider;
    WKUserContentController *_userContentController;
    SCOperaWebView *_webView;
    unsigned long long _promptCount;
    unsigned long long _promptAllowedCount;
}

@property(nonatomic) unsigned long long promptAllowedCount; // @synthesize promptAllowedCount=_promptAllowedCount;
@property(nonatomic) unsigned long long promptCount; // @synthesize promptCount=_promptCount;
@property(retain, nonatomic) SCOperaWebView *webView; // @synthesize webView=_webView;
@property(retain, nonatomic) WKUserContentController *userContentController; // @synthesize userContentController=_userContentController;
@property(retain, nonatomic) SCOperaUserInfoProvider *userInfoProvider; // @synthesize userInfoProvider=_userInfoProvider;
- (void).cxx_destruct;
- (void)didStartUrlLoad;
- (void)launchPrompt:(id)arg1;
- (void)userContentController:(id)arg1 didReceiveScriptMessage:(id)arg2;
- (id)initWithUserInfoProvider:(id)arg1;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

