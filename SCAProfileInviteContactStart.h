//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCAUserTrackedEvent.h"

#import "NamedEvent.h"

@class NSString;

@interface SCAProfileInviteContactStart : SCAUserTrackedEvent <NamedEvent>
{
    long long inviteType;
    NSString *inviteUrl;
}

+ (id)copy:(id)arg1;
- (void).cxx_destruct;
- (id)asDictionary;
- (id)getInviteUrl;
- (void)setInviteUrl:(id)arg1;
- (long long)getInviteType;
- (void)setInviteType:(long long)arg1;
- (id)getEventName;
- (id)init;

@end

