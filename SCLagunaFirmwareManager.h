//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCLagunaEventListener.h"
#import "SCLagunaFirmwareDownloaderDelegate.h"

@class NSObject<OS_dispatch_queue>, NSString, SCLagunaDevice, SCLagunaFirmwareDownloader, SCLagunaFirmwareUpdateEventListenerAnnouncer, SCLagunaManager;

@interface SCLagunaFirmwareManager : NSObject <SCLagunaFirmwareDownloaderDelegate, SCLagunaEventListener>
{
    NSObject<OS_dispatch_queue> *_dispatchQueue;
    id <SCPerforming> _performer;
    SCLagunaManager *_lagunaManager;
    SCLagunaFirmwareUpdateEventListenerAnnouncer *_eventAnnouncer;
    SCLagunaFirmwareDownloader *_firmwareDownloader;
    SCLagunaDevice *_targetDevice;
    unsigned long long _currentState;
    unsigned long long _currentUpdateProgress;
    NSString *_sessionId;
    NSString *_newFirmwareVersion;
    NSString *_newFirmwareHash;
    _Bool _newFirmwareUpdateRequired;
    unsigned long long _numRevertDevice;
    _Bool _deviceStartedRebooting;
}

- (void).cxx_destruct;
- (void)_logFirmwareUpdateFinished:(_Bool)arg1 error:(id)arg2;
- (void)_logFirmwareUpdateFailure:(id)arg1;
- (void)_logFirmwareUpdateSuccess;
- (_Bool)_targetDeviceTransferringOrPatching;
- (_Bool)_targetDeviceDownloadingOrTransferring;
- (_Bool)_targetDeviceUpdating;
- (unsigned long long)_fallbackState:(id)arg1;
- (id)_firmwareFilePath;
- (void)_attemptRemoveFirmwareFile;
- (void)_startUpdatingPatch:(id)arg1;
- (void)_startDownloadingPatch;
- (void)_startCheckingForUpdate:(id)arg1;
- (void)_getUpdateMetadata:(_Bool)arg1;
- (void)_prepareUpdateDidTimeout;
- (void)_checkUpdateDidTimeout;
- (void)_tranferUpdateDidTimeout;
- (void)_startProgressTimer;
- (void)_advanceProgess;
- (void)_setTargetDevice:(id)arg1;
- (void)_changeToState:(unsigned long long)arg1;
- (void)_sendFailureIfNecessary:(unsigned long long)arg1 device:(id)arg2;
- (void)dealloc;
- (void)lagunaOnDeviceFirmwareUpdate:(id)arg1 updateType:(unsigned long long)arg2 progress:(float)arg3;
- (void)lagunaOnDeviceFirmwareDigestFetched:(id)arg1 digest:(id)arg2;
- (void)lagunaOnDeviceFirmwareBinaryRevert:(id)arg1;
- (void)lagunaOnDeviceInfoUpdate:(id)arg1 updateType:(unsigned long long)arg2;
- (void)lagunaOnDeviceStateUpdate:(id)arg1;
- (void)firmwareDownloader:(id)arg1 didDownloadFirmware:(id)arg2 filePath:(id)arg3 downloadUpdateSucceeded:(_Bool)arg4;
- (void)firmwareDownloader:(id)arg1 didFetchMetadata:(id)arg2 hasNewVersion:(_Bool)arg3 checkUpdateSucceeded:(_Bool)arg4;
- (void)lagunaOnDevicePaired:(id)arg1;
- (_Bool)updateRequiredForDevice:(id)arg1;
- (_Bool)updateAvailableForDevice:(id)arg1;
- (void)prefetchNewFirmwareVersion;
- (void)reset;
- (void)removeListener:(id)arg1;
- (void)addListener:(id)arg1;
- (_Bool)startUpdatingDevice:(id)arg1;
- (_Bool)checkUpdateForDevice:(id)arg1;
- (id)updateFirmwareVersionForDevice:(id)arg1;
- (unsigned long long)updateProgressForDevice:(id)arg1;
- (unsigned long long)stateForDevice:(id)arg1;
- (id)initWithRequestManager:(id)arg1 withLagunaManager:(id)arg2;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

