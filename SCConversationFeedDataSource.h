//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCFeedDataSource.h"
#import "SCFeedSearchDataSourceDelegate.h"
#import "SCTimeProfilable.h"

@class NSArray, NSDictionary, NSObject<OS_dispatch_queue>, NSString, SCFeedSearchDataSource;

@interface SCConversationFeedDataSource : NSObject <SCFeedDataSource, SCFeedSearchDataSourceDelegate, SCTimeProfilable>
{
    NSObject<OS_dispatch_queue> *_feedDataSourceQueue;
    id <SCPerforming> _performer;
    NSArray *_recentViewModels;
    NSDictionary *_recentViewModelIndexes;
    NSArray *_cachedRecentViewModels;
    NSDictionary *_cachedRecentViewModelIndexes;
    NSArray *_filteredViewModels;
    NSString *_searchText;
    SCFeedSearchDataSource *_searchDataSource;
    _Bool _searching;
    long long _unreadConversationCount;
    id <SCFeedDataSourceDelegate> _delegate;
}

+ (long long)context;
+ (id)profiledSelectorNames;
@property(nonatomic) __weak id <SCFeedDataSourceDelegate> delegate; // @synthesize delegate=_delegate;
@property(nonatomic) _Bool searching; // @synthesize searching=_searching;
@property(nonatomic) long long unreadConversationCount; // @synthesize unreadConversationCount=_unreadConversationCount;
- (void).cxx_destruct;
- (id)excludedProfiledSelectorNames;
- (void)logFeedItem:(id)arg1;
- (void)reloadFeed;
- (void)inactiveViewModelsDidChange;
- (void)itemInfoDidChange;
- (void)doSearchWithCompletion:(CDUnknownBlockType)arg1;
- (id)viewModelForFeedItem:(id)arg1;
- (void)feedItemsDidChange:(id)arg1;
- (void)resetReplayStateInSnaps;
- (void)searchWithText:(id)arg1;
- (id)viewModels;
- (id)cachedViewModels;
- (void)feedWillReload;
- (void)dealloc;
- (id)init;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

