//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "NSCoding.h"

@class NSMutableArray;

@interface SCRecentStickerHistory : NSObject <NSCoding>
{
    NSMutableArray *_recentlyUsedStickers;
}

- (void).cxx_destruct;
- (void)encodeWithCoder:(id)arg1;
- (id)initWithCoder:(id)arg1;
- (void)_updateGeostickers;
- (void)_updateBitmojiStickers;
- (void)_updateCustomStickers:(id)arg1;
- (id)recentlyUsedStickerList:(id)arg1;
- (void)addRecentlyUsedSticker:(id)arg1;
- (id)init;

@end

