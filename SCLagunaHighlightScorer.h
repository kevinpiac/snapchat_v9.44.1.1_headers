//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCLagunaEventListener.h"

@class NSString, SCLagunaEventListenerAnnouncer, SCQueuePerformer;

@interface SCLagunaHighlightScorer : NSObject <SCLagunaEventListener>
{
    SCLagunaEventListenerAnnouncer *_announcer;
    id <SCLagunaHighlightAnalyzer> _highlightAnalyzer;
    SCQueuePerformer *_scoringPerformer;
    id <SCLagunaHighlightScorerDelegate> _delegate;
}

@property(nonatomic) __weak id <SCLagunaHighlightScorerDelegate> delegate; // @synthesize delegate=_delegate;
@property(retain, nonatomic) SCQueuePerformer *scoringPerformer; // @synthesize scoringPerformer=_scoringPerformer;
@property(retain, nonatomic) id <SCLagunaHighlightAnalyzer> highlightAnalyzer; // @synthesize highlightAnalyzer=_highlightAnalyzer;
@property(retain, nonatomic) SCLagunaEventListenerAnnouncer *announcer; // @synthesize announcer=_announcer;
- (void).cxx_destruct;
- (void)lagunaOnContentUpdate:(id)arg1 updateType:(unsigned long long)arg2 contentComponent:(unsigned long long)arg3;
- (void)_scoreContent:(id)arg1;
- (void)lagunaOnDeviceFileIndexUpdate:(id)arg1;
- (id)initWithAnnouncer:(id)arg1 highlightAnalyzer:(id)arg2 delegate:(id)arg3 content:(id)arg4;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

