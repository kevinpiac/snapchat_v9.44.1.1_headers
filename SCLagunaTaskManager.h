//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSDate, NSMutableArray, NSNumber, NSUUID, SCLagunaCache, SCLagunaDevice, SCLagunaDeviceManagerIdleWatch, SCLagunaEventListenerAnnouncer, SCLagunaTask;

@interface SCLagunaTaskManager : NSObject
{
    long long _numberOfContentAlreadyTransferred[3];
    long long _transferChannel;
    id <SCLagunaTaskManagerDelegate> _delegate;
    SCLagunaEventListenerAnnouncer *_announcer;
    SCLagunaDevice *_device;
    SCLagunaCache *_cache;
    SCLagunaTask *_currentTask;
    double _lastRequestSentTime;
    double _lastTransferSpeed;
    NSMutableArray *_completedTasks;
    unsigned long long _backgroundTaskIdentifier;
    SCLagunaDeviceManagerIdleWatch *_idleWatch;
    id <SCLagunaLibraryLogger> _analyticsLogger;
    NSUUID *_transferBatchID;
    NSDate *_taskManagerStartTime;
    NSNumber *_deviceBatteryLevelAtStart;
}

@property(retain, nonatomic) NSNumber *deviceBatteryLevelAtStart; // @synthesize deviceBatteryLevelAtStart=_deviceBatteryLevelAtStart;
@property(retain, nonatomic) NSDate *taskManagerStartTime; // @synthesize taskManagerStartTime=_taskManagerStartTime;
@property(retain, nonatomic) NSUUID *transferBatchID; // @synthesize transferBatchID=_transferBatchID;
@property(retain, nonatomic) id <SCLagunaLibraryLogger> analyticsLogger; // @synthesize analyticsLogger=_analyticsLogger;
@property(nonatomic) __weak SCLagunaDeviceManagerIdleWatch *idleWatch; // @synthesize idleWatch=_idleWatch;
@property(nonatomic) unsigned long long backgroundTaskIdentifier; // @synthesize backgroundTaskIdentifier=_backgroundTaskIdentifier;
@property(retain, nonatomic) NSMutableArray *completedTasks; // @synthesize completedTasks=_completedTasks;
@property(nonatomic) double lastTransferSpeed; // @synthesize lastTransferSpeed=_lastTransferSpeed;
@property(nonatomic) double lastRequestSentTime; // @synthesize lastRequestSentTime=_lastRequestSentTime;
@property(retain, nonatomic) SCLagunaTask *currentTask; // @synthesize currentTask=_currentTask;
@property(retain, nonatomic) SCLagunaCache *cache; // @synthesize cache=_cache;
@property(retain, nonatomic) SCLagunaDevice *device; // @synthesize device=_device;
@property(retain, nonatomic) SCLagunaEventListenerAnnouncer *announcer; // @synthesize announcer=_announcer;
@property(nonatomic) __weak id <SCLagunaTaskManagerDelegate> delegate; // @synthesize delegate=_delegate;
@property(nonatomic) long long transferChannel; // @synthesize transferChannel=_transferChannel;
- (void).cxx_destruct;
- (_Bool)_createLocalThumbnailFile:(id)arg1 fromMetadataTask:(id)arg2;
- (_Bool)_createLocalHDVideoFile:(id)arg1 fromMetadataTask:(id)arg2;
- (_Bool)_createLocalSDVideoFile:(id)arg1 fromMetadataTask:(id)arg2;
- (void)_createEmptySDVideoFile:(id)arg1;
- (void)_createEmptyThumbnailFile:(id)arg1;
- (_Bool)_populateContentMetadata:(id)arg1 fromMetadataTask:(id)arg2;
- (_Bool)_checkIfNewContentIsAvailable:(id)arg1;
- (void)_updateFilesizeIfNecessaryForMedia:(id)arg1 content:(id)arg2;
- (void)_reconcileManifest:(id)arg1;
- (void)_enqueueTaskForContent:(id)arg1 component:(unsigned long long)arg2;
- (void)_generateContentFromMetadataTask:(id)arg1;
- (void)_logTransferEventWithResponse:(id)arg1;
- (void)_logTransferBatchTerminated;
- (void)_handleFinishedTask:(id)arg1;
- (void)_handleTaskInterrupted;
- (void)_handleTaskHasBeenPaused;
- (void)_handleTaskProcessing;
- (void)_updateListenerForTask:(id)arg1 contentUpdateType:(unsigned long long)arg2 transferingUpdateType:(unsigned long long)arg3;
- (void)_handleTaskNotHandlingResponse:(id)arg1;
- (void)processNextRequest;
- (void)tearDown;
- (_Bool)isContentPartOfCurrentTransferBatch:(id)arg1 component:(unsigned long long)arg2;
- (void)handleAmbaResponse:(id)arg1;
- (void)dealloc;
- (CDUnknownBlockType)_taskExpirationHandler;
- (id)initWithDevice:(id)arg1 delegate:(id)arg2 announcer:(id)arg3 cache:(id)arg4 transferChannel:(long long)arg5 idleWatch:(id)arg6 analyticsLogger:(id)arg7;

@end

