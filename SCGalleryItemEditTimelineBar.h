//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UIView.h"

@interface SCGalleryItemEditTimelineBar : UIView
{
    UIView *_indicatorView;
    long long _segmentCount;
    double _percent;
    double _indicatorViewWidth;
}

@property(readonly, nonatomic) double indicatorViewWidth; // @synthesize indicatorViewWidth=_indicatorViewWidth;
@property(readonly, nonatomic) double percent; // @synthesize percent=_percent;
- (void).cxx_destruct;
- (void)_updateLayoutAnimated:(_Bool)arg1;
- (void)setSegmentCount:(long long)arg1 index:(long long)arg2 animated:(_Bool)arg3;
- (void)scrollToPercent:(double)arg1;
- (void)layoutSubviews;
- (id)initWithFrame:(struct CGRect)arg1;

@end

