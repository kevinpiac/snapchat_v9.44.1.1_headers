//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UIView.h"

#import "SCAddFriendButtonV2Delegate.h"
#import "SCProfilePictureThumbnailDelegate.h"

@class Friend, NSString, SCAddFriendButtonV2, SCFriendProfileCellTextView, SCFriendProfileCellTextViewV2, SCFriendmojiView, SCProfilePictureThumbnail, SCSingleIconConfigurer, SCStoryIconView, UIButton;

@interface SCFriendProfileCellView : UIView <SCProfilePictureThumbnailDelegate, SCAddFriendButtonV2Delegate>
{
    UIView *_thumbnailContainer;
    UIView *_emojiContainer;
    UIView *_buttonContainer;
    UIView *_xButtonContainer;
    UIView *_buttonEmojiContainer;
    UIButton *_xButton;
    SCProfilePictureThumbnail *_profilePictureThumbnail;
    SCFriendProfileCellTextView *_textView;
    SCFriendProfileCellTextViewV2 *_textViewV2;
    SCFriendmojiView *_friendMojiView;
    SCAddFriendButtonV2 *_button;
    SCStoryIconView *_storyThumnailView;
    SCSingleIconConfigurer *_singleIconConfigurer;
    NSString *_curPublicFriendStoriesUsername;
    id <SCFriendProfileCellViewDelegate> _delegate;
    Friend *_friend;
}

@property(readonly, nonatomic) Friend *friend; // @synthesize friend=_friend;
- (void).cxx_destruct;
- (void)_thumnailStatusDidChange:(id)arg1 forPublicFriendStories:(id)arg2;
- (void)didDisplayProfilePictureOnThumbnail:(id)arg1 friend:(id)arg2;
- (void)snapButtonV2Pressed:(id)arg1 friend:(id)arg2;
- (void)chatButtonV2Pressed:(id)arg1 friend:(id)arg2;
- (void)buttonV2Pressed:(id)arg1 friend:(id)arg2;
- (_Bool)didDisplayProfilePicture;
- (void)setDefaultRightOffset;
- (void)setTextLeftPadding:(double)arg1;
- (void)setRightOffset:(double)arg1;
- (void)setLeftOffset:(double)arg1;
- (void)_updateWithMainLabel:(id)arg1 subLabel:(id)arg2 thirdLabel:(id)arg3 style:(long long)arg4;
- (void)updateButtonWithState:(long long)arg1 friend:(id)arg2 style:(long long)arg3;
- (void)xButtonPressed;
- (void)_updateWithXButton:(_Bool)arg1;
- (void)_updateWithMainLabel:(id)arg1 subLabel:(id)arg2 style:(long long)arg3;
- (void)_updateEmojiViewWithFriend:(id)arg1;
- (void)_updateThumbnailWithFriend:(id)arg1 publicFriendStories:(id)arg2 contexts:(id)arg3 style:(long long)arg4 shouldShowBitmoji:(_Bool)arg5;
- (void)prepareForReuse;
@property(readonly, nonatomic) UIView *thumbnailBaseView;
- (void)updateCellViewIncludingBitmojiWithFriend:(id)arg1 contexts:(id)arg2 thumbnailStyle:(long long)arg3 mainLabel:(id)arg4 subLabel:(id)arg5 thirdLabel:(id)arg6 textViewStyle:(long long)arg7 addButtonState:(long long)arg8 addButtonStyle:(long long)arg9;
- (void)updateCellViewWithFriend:(id)arg1 isBlocked:(_Bool)arg2 publicFriendStories:(id)arg3 contexts:(id)arg4 thumbnailStyle:(long long)arg5 textViewV2:(_Bool)arg6 mainLabel:(id)arg7 subLabel:(id)arg8 thirdLabel:(id)arg9 textViewStyle:(long long)arg10 addButtonState:(long long)arg11 addButtonStyle:(long long)arg12 backgroundColor:(id)arg13 hasXButton:(_Bool)arg14 shouldShowBitmoji:(_Bool)arg15;
- (id)initWithDelegate:(id)arg1;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

