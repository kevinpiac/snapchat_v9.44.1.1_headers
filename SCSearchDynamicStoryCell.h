//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCSearchGenericCellView.h"

@class UIImageView, UILabel;

@interface SCSearchDynamicStoryCell : SCSearchGenericCellView
{
    UILabel *_titleLabel;
    UIImageView *_categoryIconView;
    UILabel *_subtitleLabel;
    UILabel *_additionalLabel;
    UILabel *_descriptionLabel;
    UIImageView *_storyImageView;
}

+ (id)reuseIdentifier;
@property(readonly, nonatomic) UIImageView *storyImageView; // @synthesize storyImageView=_storyImageView;
- (void).cxx_destruct;
- (void)setupWithViewModel:(id)arg1;
- (void)prepareForReuse;
- (void)layoutSubviews;
- (id)initWithFrame:(struct CGRect)arg1;

@end

