//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCEventGenerator.h"

@interface SCChatEventGenerator : SCEventGenerator
{
    id <SCChatMessage> _highPrioirytSentMessage;
    id <SCChatMessage> _normalPriorityUnreadMessage;
    id <SCChatMessage> _normalPriorityLatestMessage;
}

- (void).cxx_destruct;
- (id)_highPriorityInteractionEvent;
- (id)_normalPriorityInteractionEvent;
- (id)toInteractionEvent;
- (void)_updateWithLastMessages;
- (void)updateWithMessage:(id)arg1;

@end

