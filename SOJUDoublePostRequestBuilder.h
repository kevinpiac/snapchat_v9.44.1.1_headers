//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSArray, NSData, NSNumber, NSString, SOJUStoryFrame;

@interface SOJUDoublePostRequestBuilder : NSObject
{
    NSString *_captionTextDisplay;
    NSString *_storyTimestamp;
    NSString *_captureTimestamp;
    NSString *_myStory;
    NSString *_sharedIds;
    NSString *_verifiedUserIds;
    NSNumber *_lat;
    NSNumber *_longValue;
    NSData *_thumbnailData;
    NSData *_rawThumbnailData;
    NSString *_clientId;
    NSString *_connection;
    NSNumber *_type;
    NSData *_data;
    NSString *_uploadUrl;
    NSString *_key;
    NSString *_iv;
    NSNumber *_isEnc;
    SOJUStoryFrame *_framing;
    NSArray *_mobIds;
    NSNumber *_horizontalAccuracy;
    NSNumber *_altitude;
    NSNumber *_verticalAccuracy;
    NSString *_mediaId;
    NSNumber *_orientation;
    NSString *_zipped;
    NSString *_encGeoData;
    NSString *_filterId;
    NSString *_lensId;
    NSString *_cameraFrontFacing;
    NSNumber *_time;
    NSString *_timestamp;
    NSString *_reqToken;
    NSString *_username;
    NSArray *_recipients;
    NSArray *_recipientIds;
    NSArray *_invitedRecipients;
}

+ (id)withJUDoublePostRequest:(id)arg1;
- (void).cxx_destruct;
- (id)setInvitedRecipients:(id)arg1;
- (id)setRecipientIds:(id)arg1;
- (id)setRecipients:(id)arg1;
- (id)setUsername:(id)arg1;
- (id)setReqToken:(id)arg1;
- (id)setTimestamp:(id)arg1;
- (id)setTime:(id)arg1;
- (id)setCameraFrontFacing:(id)arg1;
- (id)setLensId:(id)arg1;
- (id)setFilterId:(id)arg1;
- (id)setEncGeoData:(id)arg1;
- (id)setZipped:(id)arg1;
- (id)setOrientation:(id)arg1;
- (id)setMediaId:(id)arg1;
- (id)setVerticalAccuracy:(id)arg1;
- (id)setAltitude:(id)arg1;
- (id)setHorizontalAccuracy:(id)arg1;
- (id)setMobIds:(id)arg1;
- (id)setFraming:(id)arg1;
- (id)setIsEnc:(id)arg1;
- (id)setIv:(id)arg1;
- (id)setKey:(id)arg1;
- (id)setUploadUrl:(id)arg1;
- (id)setData:(id)arg1;
- (id)setType:(id)arg1;
- (id)setConnection:(id)arg1;
- (id)setClientId:(id)arg1;
- (id)setRawThumbnailData:(id)arg1;
- (id)setThumbnailData:(id)arg1;
- (id)setLongValue:(id)arg1;
- (id)setLat:(id)arg1;
- (id)setVerifiedUserIds:(id)arg1;
- (id)setSharedIds:(id)arg1;
- (id)setMyStory:(id)arg1;
- (id)setCaptureTimestamp:(id)arg1;
- (id)setStoryTimestamp:(id)arg1;
- (id)setCaptionTextDisplay:(id)arg1;
- (id)build;
- (id)setTimeValue:(double)arg1;
- (id)setOrientationValue:(int)arg1;
- (id)setVerticalAccuracyValue:(double)arg1;
- (id)setAltitudeValue:(double)arg1;
- (id)setHorizontalAccuracyValue:(double)arg1;
- (id)setIsEncValue:(_Bool)arg1;
- (id)setTypeValue:(int)arg1;
- (id)setLongValueValue:(double)arg1;
- (id)setLatValue:(double)arg1;

@end

