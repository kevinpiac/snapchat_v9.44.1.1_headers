//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UICollectionViewCell.h"

@class SCAttachmentSearchResult, UIImageView, UILabel;

@interface SCAttachmentSearchResultCell : UICollectionViewCell
{
    _Bool _isTopCell;
    _Bool _isBottomCell;
    double _width;
    SCAttachmentSearchResult *_searchResult;
    id <SCAttachmentSearchResultCellDelegate> _delegate;
    UIImageView *_thumbnailImage;
    UILabel *_titleLabel;
    UILabel *_subtitleLabel;
}

@property(retain, nonatomic) UILabel *subtitleLabel; // @synthesize subtitleLabel=_subtitleLabel;
@property(retain, nonatomic) UILabel *titleLabel; // @synthesize titleLabel=_titleLabel;
@property(retain, nonatomic) UIImageView *thumbnailImage; // @synthesize thumbnailImage=_thumbnailImage;
@property(nonatomic) __weak id <SCAttachmentSearchResultCellDelegate> delegate; // @synthesize delegate=_delegate;
@property(retain, nonatomic) SCAttachmentSearchResult *searchResult; // @synthesize searchResult=_searchResult;
- (void).cxx_destruct;
- (void)layoutSubviews;
- (void)setup:(id)arg1 isTopCell:(_Bool)arg2 isBottomCell:(_Bool)arg3 withWidth:(double)arg4;
- (void)didPerformTap:(id)arg1;
- (void)prepareForReuse;
- (void)setupView;
- (id)initWithFrame:(struct CGRect)arg1;

@end

