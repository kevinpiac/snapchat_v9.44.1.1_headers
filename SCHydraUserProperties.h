//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "NSCoding.h"

@interface SCHydraUserProperties : NSObject <NSCoding>
{
    _Bool _canShareLocationTextImageForSearch;
    long long _igsaAdvertisingCounter;
}

@property(nonatomic) long long igsaAdvertisingCounter; // @synthesize igsaAdvertisingCounter=_igsaAdvertisingCounter;
@property(nonatomic, getter=canShareLocationTextImageForSearch) _Bool canShareLocationTextImageForSearch; // @synthesize canShareLocationTextImageForSearch=_canShareLocationTextImageForSearch;
- (void)setHydraUserPropertiesWithDictionary:(id)arg1;
- (void)_updateServerWithHydraProperties:(id)arg1;
- (void)setIGSAAdvertisingCounter:(long long)arg1;
- (id)initWithCoder:(id)arg1;
- (void)encodeWithCoder:(id)arg1;

@end

