//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@interface SCFideliusLogger : NSObject
{
}

+ (id)shared;
- (void)logTimedEventEnd:(id)arg1 withUniqueId:(id)arg2 withParameters:(id)arg3;
- (void)logTimedEventStart:(id)arg1 withUniqueId:(id)arg2 isUniqueEvent:(_Bool)arg3;
- (void)logEvent:(id)arg1 parameters:(id)arg2;
- (void)logEvent:(id)arg1;
- (void)displayToastForEvent:(id)arg1 params:(id)arg2;

@end

