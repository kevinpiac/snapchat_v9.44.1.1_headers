//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SOJUBroadcastGroupApiFetchPublicationRequest.h"

@class NSString, SOJUBroadcastGroupModelLocation;

@interface SOJUBroadcastGroupApiFetchPublicationRequest : NSObject <SOJUBroadcastGroupApiFetchPublicationRequest>
{
    SOJUBroadcastGroupModelLocation *_position;
}

@property(readonly, copy, nonatomic) SOJUBroadcastGroupModelLocation *position; // @synthesize position=_position;
- (void).cxx_destruct;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
- (_Bool)isEqual:(id)arg1;
- (void)encodeWithCoder:(id)arg1;
- (id)initWithCoder:(id)arg1;
- (id)copyWithZone:(struct _NSZone *)arg1;
- (id)initWithPosition:(id)arg1;
- (id)toJson;
- (id)toDictionary;
- (id)initWithJSONDictionary:(id)arg1;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly) Class superclass;

@end

