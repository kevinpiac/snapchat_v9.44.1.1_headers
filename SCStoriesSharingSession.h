//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCOperaSession.h"
#import "SCSendViewControllerSendingDelegate.h"

@class NSString, SCOperaViewController, SCStoryUsageLogger, SCUserSession, SendViewController, Story, User;

@interface SCStoriesSharingSession : NSObject <SCSendViewControllerSendingDelegate, SCOperaSession>
{
    Story *_story;
    SCUserSession *_userSession;
    User *_user;
    long long _viewLocation;
    SCStoryUsageLogger *_usageLogger;
    SCOperaViewController *_operaViewController;
    id <SCStoriesOperaPageProvider> _operaPageProvider;
    SendViewController *_sendViewController;
}

- (void).cxx_destruct;
- (id)_selectRecipientConfigurationForStory:(id)arg1;
- (void)_sendStoryShareToRecipients:(id)arg1 mischiefs:(id)arg2;
- (void)sendViewController:(id)arg1 sendToRecipients:(id)arg2 invitedRecipients:(id)arg3 postToMyStory:(_Bool)arg4 officialStories:(id)arg5 sharedStories:(id)arg6 mobStories:(id)arg7 mischiefs:(id)arg8 gallery:(_Bool)arg9 inviteRecipientShown:(long long)arg10;
- (_Bool)_shouldLogIdentifyingParametersForStory:(id)arg1;
- (void)_logContextMenuSendWithStory:(id)arg1 recipientCount:(unsigned long long)arg2;
- (void)_logContextMenuViewWithStory:(id)arg1;
- (void)_fetchFriendScore;
- (void)operaViewDidSendEvent:(id)arg1 context:(id)arg2 params:(id)arg3;
- (id)registeredEventsForOperaSession;
- (id)initWithUserSession:(id)arg1 user:(id)arg2 viewLocation:(long long)arg3 operaViewController:(id)arg4 operaPageProvider:(id)arg5;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

