//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSString, UIImage;

@interface SCGalleryItemEditDropdownCellConfig : NSObject
{
    _Bool _enabled;
    UIImage *_image;
    NSString *_text;
    CDUnknownBlockType _handler;
}

@property(readonly, nonatomic) CDUnknownBlockType handler; // @synthesize handler=_handler;
@property(readonly, nonatomic) _Bool enabled; // @synthesize enabled=_enabled;
@property(readonly, nonatomic) NSString *text; // @synthesize text=_text;
@property(readonly, nonatomic) UIImage *image; // @synthesize image=_image;
- (void).cxx_destruct;
- (id)initWithImage:(id)arg1 text:(id)arg2 handler:(CDUnknownBlockType)arg3;
- (id)initWithImage:(id)arg1 text:(id)arg2 enabled:(_Bool)arg3 handler:(CDUnknownBlockType)arg4;

@end

