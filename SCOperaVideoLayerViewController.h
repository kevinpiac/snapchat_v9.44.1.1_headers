//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCOperaLayerViewController.h"

#import "SCOperaVideoControlsViewDataSource.h"
#import "SCOperaVideoControlsViewDelegate.h"
#import "SCTimeProfilable.h"

@class AVAsset, FBKVOController, NSNotificationCenter, NSString, NSTimer, SCFrameableContainerView, SCOperaSharedResourceManager, SCOperaVideoLayer, SCOperaVideoLayerView, SCStopwatch, UIGestureRecognizer;

@interface SCOperaVideoLayerViewController : SCOperaLayerViewController <SCOperaVideoControlsViewDelegate, SCOperaVideoControlsViewDataSource, SCTimeProfilable>
{
    SCOperaVideoLayer *_layer;
    SCOperaVideoLayerView *_layerView;
    SCFrameableContainerView *_containerView;
    FBKVOController *_kvoController;
    NSNotificationCenter *_notificationCenter;
    SCOperaSharedResourceManager *_sharedResourceManager;
    _Bool _fullyViewed;
    _Bool _isLooping;
    _Bool _hasResetPlayerItem;
    double _durationSec;
    SCStopwatch *_mediaDisplayStopWatch;
    id _videoTimeObserver;
    _Bool _isSeeking;
    NSTimer *_controlsFadeTimer;
    long long _virtualOrientation;
    UIGestureRecognizer *_tapGesture;
    double _delayedTargetVolume;
    AVAsset *_videoAsset;
    id <SCOperaPropertyUpdateModerator> _propertyUpdateModerator;
}

+ (long long)context;
+ (id)profiledSelectorNames;
+ (id)layerViewControllerWithLayer:(id)arg1 configuration:(id)arg2 kvoController:(id)arg3 mediaDisplayStopwatch:(id)arg4 eventAnnouncer:(id)arg5 sharedResourceManager:(id)arg6 notificationCenter:(id)arg7;
@property(nonatomic) __weak id <SCOperaPropertyUpdateModerator> propertyUpdateModerator; // @synthesize propertyUpdateModerator=_propertyUpdateModerator;
- (void).cxx_destruct;
- (id)excludedProfiledSelectorNames;
- (CDStruct_1b6d18a9)duration;
- (CDStruct_1b6d18a9)currentTime;
- (void)setProgress:(double)arg1 forIndex:(unsigned long long)arg2;
- (void)seekToTime:(double)arg1 toleranceBefore:(CDStruct_1b6d18a9)arg2 toleranceAfter:(CDStruct_1b6d18a9)arg3 completion:(CDUnknownBlockType)arg4;
- (void)seekToTime:(double)arg1;
- (void)seekToMediaStartTimeWithCompletion:(CDUnknownBlockType)arg1;
- (void)videoControlsView:(id)arg1 didSeekToTime:(double)arg2;
- (void)videoControlsViewDidPressExit:(id)arg1;
- (void)videoControlsView:(id)arg1 didEndSeekingWithPlayButtonToggled:(_Bool)arg2;
- (void)videoControlsViewDidBeginSeeking:(id)arg1;
- (void)videoControlsView:(id)arg1 didTogglePlay:(_Bool)arg2;
- (void)_rotateVideoWithTransform:(struct CGAffineTransform)arg1;
- (void)_setTargetOrientation:(long long)arg1 andRotateView:(_Bool)arg2;
- (void)videoControlsView:(id)arg1 didToggleRotateLeft:(_Bool)arg2;
- (void)videoControlsView:(id)arg1 didToggleCaption:(_Bool)arg2;
- (void)videoControlsView:(id)arg1 didToggleVolume:(_Bool)arg2;
- (void)_toggleVideoControlsView;
- (void)handleTap:(id)arg1;
- (void)_setupControlsFadeTimer;
- (void)_fadeOutControlsWithCompletion:(CDUnknownBlockType)arg1;
- (void)_fadeOutControls;
- (void)_fadeInControls;
- (void)_resetVideoAsset;
- (id)_videoAsset;
- (void)playerItemDidReachEnd:(id)arg1;
- (void)_removeTimeObserver;
- (void)_attachTimeObserver;
- (double)_heightToWidthAspectRatio;
- (struct CGSize)_contentSize;
- (void)loadView;
- (id)shareableMedia;
- (id)currentViewParameters;
- (void)_updateResumeTime;
- (void)didReceiveUpdateProperties:(id)arg1;
- (long long)pageabilityForRelativePosition:(unsigned long long)arg1;
- (void)setVolume:(double)arg1;
- (void)resume;
- (void)pause;
- (void)viewDidFullyDisappear;
- (_Bool)_shouldShowVideoControls;
- (void)_setupControlsIfNecessary;
- (void)_startPlayingItem:(id)arg1;
- (void)_startPlayingFromMediaStartTimeForItem:(id)arg1;
- (void)_restartPlayer;
- (void)_updatePlayerStatusBasedOnPlayerItemStatus:(id)arg1;
- (void)neighborViewDidFullyAppearWithCurrentViewRelativePosition:(unsigned long long)arg1;
- (void)viewDidFullyAppear;
- (void)dealloc;
- (id)initWithLayer:(id)arg1 configuration:(id)arg2 kvoController:(id)arg3 mediaDisplayStopwatch:(id)arg4 eventAnnouncer:(id)arg5 sharedResourceManager:(id)arg6 notificationCenter:(id)arg7;
- (id)initWithLayer:(id)arg1 configuration:(id)arg2 eventAnnouncer:(id)arg3;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

