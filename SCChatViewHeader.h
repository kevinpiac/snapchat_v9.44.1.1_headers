//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCChatConversationUpdaterListener.h"
#import "SCChatViewLifeCycleListener.h"
#import "SCHeaderDataSource.h"

@class NSString, SCHeader, UILabel, UIView;

@interface SCChatViewHeader : NSObject <SCChatViewLifeCycleListener, SCChatConversationUpdaterListener, SCHeaderDataSource>
{
    UIView *_parentView;
    UIView *_rightButtonCircleView;
    UIView *_recipientBar;
    id <SCChatViewHeaderDataSource> _dataSource;
    id <SCChatConversationViewModel> _chatViewModel;
    SCHeader *_header;
    UILabel *_sccpActiveLabel;
}

+ (id)rightButtonCircleBorderColor;
+ (id)headerBorderColor;
@property(retain, nonatomic) UILabel *sccpActiveLabel; // @synthesize sccpActiveLabel=_sccpActiveLabel;
@property(retain, nonatomic) SCHeader *header; // @synthesize header=_header;
@property(retain, nonatomic) id <SCChatConversationViewModel> chatViewModel; // @synthesize chatViewModel=_chatViewModel;
- (void).cxx_destruct;
- (void)_cleanupForRecipientBarRemoval;
- (void)_updateLayoutForRecipientBarRemoval;
- (void)removeRecipientBar:(_Bool)arg1 completion:(CDUnknownBlockType)arg2;
- (void)addRecipientBar:(id)arg1;
- (void)_updateRightButtonCircleWithVerticalTranslationUp:(double)arg1;
- (void)_updateScaleWithProgress:(double)arg1;
- (void)_updateAlphaWithProgress:(double)arg1;
- (void)displayWithVerticalTranslationUp:(double)arg1;
- (void)setHeaderViewsScale:(double)arg1 includeContentView:(_Bool)arg2;
- (void)setHeaderViewsAlpha:(double)arg1 includeRightButton:(_Bool)arg2;
- (void)showRightButton;
- (void)hideRightButton;
- (id)imageForRightButtonInState:(unsigned long long)arg1;
- (id)imageForLeftButtonInState:(unsigned long long)arg1;
- (id)textColorForHeader:(id)arg1;
- (id)titleForHeader:(id)arg1;
- (id)backgroundColorForHeader;
- (void)didConversationViewModelChange:(id)arg1;
- (void)reloadHeader;
- (id)rightButtonCircleView;
- (void)updateSCCPLabelWithState:(id)arg1;
- (void)_initSCCPStatusLabel;
- (id)headerSuperview;
- (id)headerContentView;
- (struct CGRect)headerFrame;
- (id)headerBottom;
- (void)_initHeaderWithDelegate:(id)arg1;
- (void)initHeader;
- (id)initWithHeaderDelegate:(id)arg1 headerDataSource:(id)arg2 parentView:(id)arg3;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

