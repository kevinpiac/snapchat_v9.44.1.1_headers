//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSMutableDictionary.h"

@interface NSMutableDictionary (SCGalleryItemSelectionModeValueDictionary)
- (unsigned long long)selectionModeForIndexPath:(id)arg1;
- (void)removeSelectionModeForIndexPath:(id)arg1;
- (void)setSelectionMode:(unsigned long long)arg1 forIndexPath:(id)arg2;
@end

