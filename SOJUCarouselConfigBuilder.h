//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSDictionary, NSNumber, NSString;

@interface SOJUCarouselConfigBuilder : NSObject
{
    NSString *_configurationId;
    NSString *_versionId;
    NSNumber *_clientCacheExpirationDateTime;
    NSDictionary *_baseOverrides;
    NSDictionary *_filterOverride;
}

+ (id)withJUCarouselConfig:(id)arg1;
- (void).cxx_destruct;
- (id)setFilterOverride:(id)arg1;
- (id)setBaseOverrides:(id)arg1;
- (id)setClientCacheExpirationDateTime:(id)arg1;
- (id)setVersionId:(id)arg1;
- (id)setConfigurationId:(id)arg1;
- (id)build;
- (id)setClientCacheExpirationDateTimeValue:(long long)arg1;

@end

