//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UICollectionViewCell.h"

@class NSString, UIImageView;

@interface GeofilterPassportCell : UICollectionViewCell
{
    NSString *_imageURL;
    UIImageView *_imageView;
    id <GeofilterPassportImageCache> _imageDelegate;
}

@property(nonatomic) __weak id <GeofilterPassportImageCache> imageDelegate; // @synthesize imageDelegate=_imageDelegate;
@property(retain, nonatomic) NSString *imageURL; // @synthesize imageURL=_imageURL;
- (void).cxx_destruct;
- (void)_loadImage;
- (void)_setup;
- (id)expandingView;
- (id)init;
- (id)initWithFrame:(struct CGRect)arg1;
- (id)initWithCoder:(id)arg1;

@end

