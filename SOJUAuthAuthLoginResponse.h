//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SOJUAuthAuthLoginResponse.h"

@class NSArray, NSString, SOJUAuthRefreshToken;

@interface SOJUAuthAuthLoginResponse : NSObject <SOJUAuthAuthLoginResponse>
{
    NSString *_nonce;
    NSString *_username;
    NSString *_userId;
    SOJUAuthRefreshToken *_refreshToken;
    NSArray *_authTokens;
}

@property(readonly, copy, nonatomic) NSArray *authTokens; // @synthesize authTokens=_authTokens;
@property(readonly, copy, nonatomic) SOJUAuthRefreshToken *refreshToken; // @synthesize refreshToken=_refreshToken;
@property(readonly, copy, nonatomic) NSString *userId; // @synthesize userId=_userId;
@property(readonly, copy, nonatomic) NSString *username; // @synthesize username=_username;
@property(readonly, copy, nonatomic) NSString *nonce; // @synthesize nonce=_nonce;
- (void).cxx_destruct;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
- (_Bool)isEqual:(id)arg1;
- (void)encodeWithCoder:(id)arg1;
- (id)initWithCoder:(id)arg1;
- (id)copyWithZone:(struct _NSZone *)arg1;
- (id)initWithNonce:(id)arg1 username:(id)arg2 userId:(id)arg3 refreshToken:(id)arg4 authTokens:(id)arg5;
- (id)toJson;
- (id)toDictionary;
- (id)initWithJSONDictionary:(id)arg1;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly) Class superclass;

@end

