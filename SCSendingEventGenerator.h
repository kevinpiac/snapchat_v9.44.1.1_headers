//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCEventGenerator.h"

@interface SCSendingEventGenerator : SCEventGenerator
{
    id <SCChatMessage> _sendingMessage;
}

- (void).cxx_destruct;
- (id)toInteractionEvent;
- (void)updateWithMessage:(id)arg1;

@end

