//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@interface SCMonetizationTweaks : NSObject
{
}

+ (id)shared;
- (id)requestSpecificAdID;
- (id)testUserGroup;
- (int)presetAdSlotMode;
- (_Bool)canPeekRightIntoAd;
- (_Bool)onlyPostRollInPlaylist;
- (_Bool)canShowAdsInSingleView;
- (_Bool)canShowAdsInPlaylist;
- (unsigned long long)minStoriesBeforeSessionEnd;
- (unsigned long long)minSnapsFromSessionStart;
- (unsigned long long)minStoriesFromSessionStart;
- (unsigned long long)minTimeFromSessionStart;
- (unsigned long long)minSnapsBetweenAds;
- (unsigned long long)minStoriesBetweenAds;
- (unsigned long long)minTimeBetweenAds;
- (long long)maxSilentSnaps;
- (_Bool)isAdsRulesTweaksEnabled;
- (_Bool)alwaysInjectJSInterface;
- (_Bool)isAdsInAutoAdvanceEnabled;
- (_Bool)useStagingHost;
- (_Bool)useOperaForAdsInAutoAdvance;
- (id)init;

@end

