//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSNumber, NSString;

@interface SOJUGalleryServletSnapTagsResultBuilder : NSObject
{
    NSString *_snapId;
    NSNumber *_statusCode;
}

+ (id)withJUGalleryServletSnapTagsResult:(id)arg1;
- (void).cxx_destruct;
- (id)setStatusCode:(id)arg1;
- (id)setSnapId:(id)arg1;
- (id)build;
- (id)setStatusCodeEnum:(long long)arg1;
- (id)setStatusCodeValue:(int)arg1;

@end

