//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCSearchGenericCellView.h"

@class UIImageView, UILabel;

@interface SCSearchAroundMeCell : SCSearchGenericCellView
{
    UILabel *_titleLabel;
    UIImageView *_categoryIconView;
    UILabel *_descriptionLabel;
    UIImageView *_avatarView;
}

+ (id)reuseIdentifier;
@property(retain, nonatomic) UIImageView *avatarView; // @synthesize avatarView=_avatarView;
- (void).cxx_destruct;
- (void)setupWithViewModel:(id)arg1;
- (void)prepareForReuse;
- (id)initWithFrame:(struct CGRect)arg1;

@end

