//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCModalChatViewController.h"

@interface SCSearchModalChatViewController : SCModalChatViewController
{
}

- (double)dismissalDuration;
- (double)presentationDuration;
- (void)setLayoutForPresented;
- (void)setLayoutForDismissed;
- (double)dismissalPercentageForPanTranslation:(struct CGPoint)arg1;
- (_Bool)panGestureShouldReceiveTouch:(id)arg1;
- (id)initWithUserSession:(id)arg1;

@end

