//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SOJUAuthAuthSyncRequest.h"

@class NSNumber, NSString;

@interface SOJUAuthAuthSyncRequest : NSObject <SOJUAuthAuthSyncRequest>
{
    NSString *_userId;
    NSString *_username;
    NSString *_email;
    NSNumber *_emailVersion;
    NSString *_passwordHash;
    NSString *_oldPasswordHash;
    NSString *_authToken;
    NSNumber *_userCredentialsVersion;
}

@property(readonly, copy, nonatomic) NSNumber *userCredentialsVersion; // @synthesize userCredentialsVersion=_userCredentialsVersion;
@property(readonly, copy, nonatomic) NSString *authToken; // @synthesize authToken=_authToken;
@property(readonly, copy, nonatomic) NSString *oldPasswordHash; // @synthesize oldPasswordHash=_oldPasswordHash;
@property(readonly, copy, nonatomic) NSString *passwordHash; // @synthesize passwordHash=_passwordHash;
@property(readonly, copy, nonatomic) NSNumber *emailVersion; // @synthesize emailVersion=_emailVersion;
@property(readonly, copy, nonatomic) NSString *email; // @synthesize email=_email;
@property(readonly, copy, nonatomic) NSString *username; // @synthesize username=_username;
@property(readonly, copy, nonatomic) NSString *userId; // @synthesize userId=_userId;
- (void).cxx_destruct;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
- (_Bool)isEqual:(id)arg1;
- (void)encodeWithCoder:(id)arg1;
- (id)initWithCoder:(id)arg1;
- (id)copyWithZone:(struct _NSZone *)arg1;
- (id)initWithUserId:(id)arg1 username:(id)arg2 email:(id)arg3 emailVersion:(id)arg4 passwordHash:(id)arg5 oldPasswordHash:(id)arg6 authToken:(id)arg7 userCredentialsVersion:(id)arg8;
- (id)toJson;
- (id)toDictionary;
- (id)initWithJSONDictionary:(id)arg1;
- (long long)userCredentialsVersionValue;
- (long long)emailVersionValue;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly) Class superclass;

@end

