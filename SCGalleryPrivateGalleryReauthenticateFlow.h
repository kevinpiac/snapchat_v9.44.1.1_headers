//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSString, SCGalleryPrivateGalleryReauthenticateView, SCUserSession, UIView, UIViewController;

@interface SCGalleryPrivateGalleryReauthenticateFlow : NSObject
{
    SCUserSession *_userSession;
    UIViewController *_fromViewController;
    UIView *_containerView;
    UIView *_overlayView;
    SCGalleryPrivateGalleryReauthenticateView *_reauthenticateView;
    NSString *_defaultMessage;
    double _keyboardHeight;
    _Bool _followsKeyboardHeight;
    _Bool _showingErrorMessage;
    id <SCGalleryPrivateGalleryReauthenticateFlowDelegate> _delegate;
}

@property(nonatomic) __weak id <SCGalleryPrivateGalleryReauthenticateFlowDelegate> delegate; // @synthesize delegate=_delegate;
- (void).cxx_destruct;
- (void)_reauthenticateWithPassword:(id)arg1 completion:(CDUnknownBlockType)arg2;
- (void)_clearErrorMessage;
- (void)_showErrorMessage:(id)arg1;
- (void)_didPressCancelButton;
- (void)_didPressNextButton;
- (void)_textFieldDidChange;
- (void)_didPressQuestionMarkButton;
- (void)_keyboardWillChangeFrame:(id)arg1;
- (void)_updateReauthenticateViewLayoutConstraints;
- (void)_dismissWithCompletion:(CDUnknownBlockType)arg1;
- (void)_showWithPresentationAnimationType:(unsigned long long)arg1;
- (void)_teardown;
- (void)_setup;
- (void)startWithPresentationAnimationType:(unsigned long long)arg1;
- (void)dealloc;
- (id)initWithUserSession:(id)arg1 fromViewController:(id)arg2;

@end

