//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSArray, UIView, UIViewController;

@protocol SCGalleryTabControllerDelegate <NSObject>
- (void)tabController:(id <SCGalleryTabController>)arg1 didPopViewController:(UIViewController *)arg2;
- (void)tabController:(id <SCGalleryTabController>)arg1 didPushViewController:(UIViewController *)arg2;
- (void)tabControllerDidEndEditing:(id <SCGalleryTabController>)arg1;
- (void)tabControllerDidBeginEditing:(id <SCGalleryTabController>)arg1;
- (void)tabControllerDidEndDecelerating:(id <SCGalleryTabController>)arg1;
- (void)tabControllerDidEndDragging:(id <SCGalleryTabController>)arg1 willDecelerate:(_Bool)arg2;
- (void)tabControllerWillBeginDragging:(id <SCGalleryTabController>)arg1;
- (CDStruct_bac8f6e9)mediaScenePathForTabController:(id <SCGalleryTabController>)arg1 withMediaScenePathComponent:(unsigned long long)arg2;
- (void)tabController:(id <SCGalleryTabController>)arg1 didChangeSelectionMode:(unsigned long long)arg2 forGalleryItem:(id <SCGalleryItem>)arg3;
- (_Bool)tabController:(id <SCGalleryTabController>)arg1 shouldDeselectGalleryItem:(id <SCGalleryItem>)arg2;
- (void)tabControllerDidChangeScrollContentOffset:(id <SCGalleryTabController>)arg1;
- (void)tabController:(id <SCGalleryTabController>)arg1 requestsSelectMode:(unsigned long long)arg2;
- (void)tabControllerRequestsUpdateDisplay:(id <SCGalleryTabController>)arg1;
- (void)tabController:(id <SCGalleryTabController>)arg1 editSelected:(long long)arg2 selectionMode:(unsigned long long)arg3 items:(NSArray *)arg4 fromView:(UIView *)arg5;
- (void)tabController:(id <SCGalleryTabController>)arg1 browseSelected:(long long)arg2 selectionMode:(unsigned long long)arg3 items:(NSArray *)arg4 fromView:(UIView *)arg5;
@end

