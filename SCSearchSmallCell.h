//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCSearchGenericCellView.h"

@class UIImage, UIImageView, UILabel;

@interface SCSearchSmallCell : SCSearchGenericCellView
{
    UIImageView *_logoImageView;
    UILabel *_titleLabel;
    UIImageView *_categoryImageView;
    UILabel *_descriptionLabel;
    UIImage *_image;
}

+ (id)reuseIdentifier;
@property(readonly, nonatomic) UIImage *image; // @synthesize image=_image;
@property(readonly, nonatomic) UILabel *descriptionLabel; // @synthesize descriptionLabel=_descriptionLabel;
@property(readonly, nonatomic) UIImageView *categoryImageView; // @synthesize categoryImageView=_categoryImageView;
@property(readonly, nonatomic) UILabel *titleLabel; // @synthesize titleLabel=_titleLabel;
@property(readonly, nonatomic) UIImageView *logoImageView; // @synthesize logoImageView=_logoImageView;
- (void).cxx_destruct;
- (id)initWithFrame:(struct CGRect)arg1;

@end

