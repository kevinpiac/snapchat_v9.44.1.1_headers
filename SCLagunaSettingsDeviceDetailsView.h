//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UIView.h"

#import "UITableViewDataSource.h"
#import "UITableViewDelegate.h"

@class NSString, SCLagunaDevice, UITableView;

@interface SCLagunaSettingsDeviceDetailsView : UIView <UITableViewDataSource, UITableViewDelegate>
{
    UITableView *_tableView;
    NSString *_title;
    long long _state;
    id <SCLagunaSettingsDeviceDetailsViewDelegate> _delegate;
    SCLagunaDevice *_device;
}

+ (double)height;
@property(retain, nonatomic) SCLagunaDevice *device; // @synthesize device=_device;
- (void).cxx_destruct;
- (void)_alertRestartError;
- (void)_alertStorageFull;
- (void)_alertDownloadingLogs;
- (void)_alertTransferInterrupted;
- (void)_alertPairNow;
- (void)_alertFactoryResetNeeded;
- (void)_statusCellPressed;
- (void)_updateMangeCellStatusState;
- (void)_updateBatteryCell:(id)arg1;
- (void)_updateBatteryCellStatusState;
- (void)_updateStatusCell:(id)arg1;
- (void)_updateStatusCellStatusState;
- (id)tableView:(id)arg1 viewForHeaderInSection:(long long)arg2;
- (double)tableView:(id)arg1 heightForHeaderInSection:(long long)arg2;
- (void)tableView:(id)arg1 didSelectRowAtIndexPath:(id)arg2;
- (id)tableView:(id)arg1 cellForRowAtIndexPath:(id)arg2;
- (double)tableView:(id)arg1 heightForRowAtIndexPath:(id)arg2;
- (long long)tableView:(id)arg1 numberOfRowsInSection:(long long)arg2;
- (void)updateViewWithStatusState:(long long)arg1;
- (void)refresh;
- (id)initWithFrame:(struct CGRect)arg1 device:(id)arg2 statusState:(long long)arg3 title:(id)arg4 delegate:(id)arg5;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

