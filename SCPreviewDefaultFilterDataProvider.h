//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCPreviewFilterDataProvider.h"

@class NSArray, NSDate, NSDictionary, NSMutableDictionary, NSString, SCAltitude, SCGeoFilterImagesFetcher, SCLocationServicesDataStore, SCSpeed, SCTimestampMetadata, SCUnlockableDataStore, SCVenueFilterSelector, SCWeather;

@interface SCPreviewDefaultFilterDataProvider : NSObject <SCPreviewFilterDataProvider>
{
    _Bool _updating;
    long long _snapSource;
    SCLocationServicesDataStore *_locationServicesDataStore;
    SCUnlockableDataStore *_unlockFilterStore;
    SCGeoFilterImagesFetcher *_geoFilterImagesFetcher;
    NSDate *_initialSpeedCaptureDate;
    NSDate *_initialAltitudeCaptureDate;
    NSMutableDictionary *_geoFilterImages;
    NSMutableDictionary *_geoFilterAppearanceSettings;
    _Bool _hasUserUnlockedFilter;
    id <SCPreviewFilterDataProviderDelegate> _delegate;
    SCVenueFilterSelector *_venueFilterSelector;
    NSArray *_geoFilters;
    SCSpeed *_speed;
    SCTimestampMetadata *_timestamp;
    SCWeather *_weather;
    unsigned long long _batteryStatus;
    NSString *_userUnlockedFilterId;
    SCAltitude *_altitude;
}

@property(readonly, nonatomic) _Bool hasUserUnlockedFilter; // @synthesize hasUserUnlockedFilter=_hasUserUnlockedFilter;
@property(readonly, nonatomic) SCAltitude *altitude; // @synthesize altitude=_altitude;
@property(readonly, copy, nonatomic) NSString *userUnlockedFilterId; // @synthesize userUnlockedFilterId=_userUnlockedFilterId;
@property(readonly, nonatomic) unsigned long long batteryStatus; // @synthesize batteryStatus=_batteryStatus;
@property(readonly, nonatomic) SCWeather *weather; // @synthesize weather=_weather;
@property(readonly, nonatomic) SCTimestampMetadata *timestamp; // @synthesize timestamp=_timestamp;
@property(readonly, nonatomic) SCSpeed *speed; // @synthesize speed=_speed;
@property(readonly, copy, nonatomic) NSArray *geoFilters; // @synthesize geoFilters=_geoFilters;
@property(readonly, nonatomic) SCVenueFilterSelector *venueFilterSelector; // @synthesize venueFilterSelector=_venueFilterSelector;
@property(nonatomic) __weak id <SCPreviewFilterDataProviderDelegate> delegate; // @synthesize delegate=_delegate;
- (void).cxx_destruct;
- (void)_fetchGeoFilterImages;
- (void)_checkUserUnlockedFilter;
- (void)_locationServicesDataStoreDidUpdate:(id)arg1;
- (void)_unlockDataStoreDidUpdate:(id)arg1;
- (id)_activeGeofilters;
- (void)updateWeatherViewType:(id)arg1;
- (void)updateAltitudeType:(id)arg1;
- (void)updateGeoFilter:(id)arg1;
- (void)stopUpdatingFilterData;
- (void)startUpdatingFilterData;
@property(readonly, nonatomic) _Bool isReverseMotionFilterSelected;
@property(readonly, copy, nonatomic) NSString *selectedGeoFilterId;
@property(readonly, copy, nonatomic) NSString *selectedSpeedMotionFilterName;
- (id)selectedMotionFilterName;
@property(readonly, copy, nonatomic) NSString *selectedSmartFilterName;
@property(readonly, copy, nonatomic) NSString *selectedVisualFilterName;
@property(readonly, copy, nonatomic) NSDictionary *reverseMotionFilterConfig;
@property(readonly, copy, nonatomic) NSArray *speedMotionFilterConfigs;
@property(readonly, copy, nonatomic) NSArray *visualFilterNames;
- (_Bool)fromDiscoverShare;
@property(readonly, copy, nonatomic) NSDictionary *geoFilterAppearanceSettingsDictionary;
@property(readonly, copy, nonatomic) NSArray *geoFilterImages;
@property(readonly, nonatomic) unsigned long long updateMode;
- (void)dealloc;
- (id)initWithSnapSource:(long long)arg1;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

