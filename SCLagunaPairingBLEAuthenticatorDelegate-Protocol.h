//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSData, NSString, SCLagunaPeripheral;

@protocol SCLagunaPairingBLEAuthenticatorDelegate <NSObject>
- (_Bool)isPairingWaitingTapConfirm;
- (_Bool)isPairingInManualMode;
- (void)pairingBLEConnectorNeedsTapConfirm;
- (void)pairingBLEConnectorDidFail;
- (void)pairingBLEConnectorDidConnectPeripheral:(SCLagunaPeripheral *)arg1 withSharedSecret:(NSData *)arg2 serialNumber:(NSString *)arg3 firmwareVersion:(NSString *)arg4;
@end

