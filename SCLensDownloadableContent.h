//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCDownloadableContent.h"

@interface SCLensDownloadableContent : SCDownloadableContent
{
}

+ (id)shared;
- (void)verifyTrackingData;
- (id)eventUniqueId;
- (id)resourceName;
- (id)fileNames;
- (id)directoryName;
- (id)init;

@end

