//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "UIView.h"

@class Friend, SCExtendedHitButton, SCLoadingIndicatorView, UIButton, UIImageView, UILabel;

@interface SCAddFriendButtonV10 : UIView
{
    Friend *_friend;
    long long _state;
    long long _style;
    struct CGRect _height;
    id <SCAddFriendButtonV10Delegate> _delegate;
    id <SCStartChatDelegate> _startChatDelegate;
    SCLoadingIndicatorView *_checkingIndicator;
    SCLoadingIndicatorView *_uncheckingIndicator;
    SCLoadingIndicatorView *_buttonLoadingIndicator;
    SCExtendedHitButton *_button;
    UIView *_buttonContentContainer;
    UIImageView *_buttonImageContainer;
    UILabel *_buttonTitleLabel;
    UIButton *_chatButton;
    UIButton *_snapButton;
}

+ (id)buttonWithState:(long long)arg1 friend:(id)arg2 delegate:(id)arg3 style:(long long)arg4;
@property(retain, nonatomic) UIButton *snapButton; // @synthesize snapButton=_snapButton;
@property(retain, nonatomic) UIButton *chatButton; // @synthesize chatButton=_chatButton;
@property(retain, nonatomic) UILabel *buttonTitleLabel; // @synthesize buttonTitleLabel=_buttonTitleLabel;
@property(retain, nonatomic) UIImageView *buttonImageContainer; // @synthesize buttonImageContainer=_buttonImageContainer;
@property(retain, nonatomic) UIView *buttonContentContainer; // @synthesize buttonContentContainer=_buttonContentContainer;
@property(retain, nonatomic) SCExtendedHitButton *button; // @synthesize button=_button;
@property(retain, nonatomic) SCLoadingIndicatorView *buttonLoadingIndicator; // @synthesize buttonLoadingIndicator=_buttonLoadingIndicator;
@property(retain, nonatomic) SCLoadingIndicatorView *uncheckingIndicator; // @synthesize uncheckingIndicator=_uncheckingIndicator;
@property(retain, nonatomic) SCLoadingIndicatorView *checkingIndicator; // @synthesize checkingIndicator=_checkingIndicator;
- (void).cxx_destruct;
- (double)currentWidth;
- (void)_setButtonContentContainerLeftOffset:(double)arg1 RightOffset:(double)arg2;
- (void)_updateButtonContentBackgroundColorWithState:(long long)arg1 style:(long long)arg2;
- (_Bool)_needsUpdateWidth:(long long)arg1;
- (id)_getBorderColorWithStyle:(long long)arg1;
- (id)_getTitleColorWithState:(long long)arg1 style:(long long)arg2;
- (id)_getBackgroundColorWithState:(long long)arg1 style:(long long)arg2;
- (id)_getTitleTextWithState:(long long)arg1 friend:(id)arg2;
- (id)_getAttributedTitleWithState:(long long)arg1 style:(long long)arg2 friend:(id)arg3;
- (id)_getIndicatorColorWithState:(long long)arg1 style:(long long)arg2;
- (id)_getButtonImageWithState:(long long)arg1 style:(long long)arg2;
- (_Bool)_shouldShowDisplayNamePopUp:(id)arg1 userAsFriend:(id)arg2;
- (void)_snapFriendPressed;
- (void)_chatFriendPressed;
- (void)_addFriendPressed;
- (id)_newIndicator:(long long)arg1 style:(long long)arg2;
- (_Bool)updateButtonWithState:(long long)arg1 friend:(id)arg2 style:(long long)arg3;
- (id)initWithFrame:(struct CGRect)arg1;

@end

