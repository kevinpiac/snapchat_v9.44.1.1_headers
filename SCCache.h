//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "SCCache.h"

@class NSString, SCCacheManager, TMDiskCache, TMMemoryCache;

@interface SCCache : NSObject <SCCache>
{
    _Bool _isUserSessionScoped;
    _Bool _isInvalidated;
    NSString *_kindName;
    CDUnknownBlockType _didRemoveObjectFromDiskBlock;
    CDUnknownBlockType _didRemoveObjectFromMemoryBlock;
    TMMemoryCache *_memoryCache;
    TMDiskCache *_diskCache;
    SCCacheManager *_cacheManager;
    id <SCConcurrentPerforming> _workQueuePerformer;
    id <SCConcurrentPerforming> _diskCacheQueuePerformer;
    id <SCConcurrentPerforming> _completionQueuePerformer;
}

+ (void)setDiskByteLimit:(unsigned long long)arg1;
+ (void)setMemoryByteLimit:(unsigned long long)arg1;
@property(nonatomic) _Bool isInvalidated; // @synthesize isInvalidated=_isInvalidated;
@property(retain, nonatomic) id <SCConcurrentPerforming> completionQueuePerformer; // @synthesize completionQueuePerformer=_completionQueuePerformer;
@property(retain, nonatomic) id <SCConcurrentPerforming> diskCacheQueuePerformer; // @synthesize diskCacheQueuePerformer=_diskCacheQueuePerformer;
@property(retain, nonatomic) id <SCConcurrentPerforming> workQueuePerformer; // @synthesize workQueuePerformer=_workQueuePerformer;
@property(retain, nonatomic) SCCacheManager *cacheManager; // @synthesize cacheManager=_cacheManager;
@property(retain, nonatomic) TMDiskCache *diskCache; // @synthesize diskCache=_diskCache;
@property(retain, nonatomic) TMMemoryCache *memoryCache; // @synthesize memoryCache=_memoryCache;
@property(nonatomic) _Bool isUserSessionScoped; // @synthesize isUserSessionScoped=_isUserSessionScoped;
@property(copy, nonatomic) CDUnknownBlockType didRemoveObjectFromMemoryBlock; // @synthesize didRemoveObjectFromMemoryBlock=_didRemoveObjectFromMemoryBlock;
@property(copy, nonatomic) CDUnknownBlockType didRemoveObjectFromDiskBlock; // @synthesize didRemoveObjectFromDiskBlock=_didRemoveObjectFromDiskBlock;
@property(readonly, nonatomic) NSString *kindName; // @synthesize kindName=_kindName;
- (void).cxx_destruct;
- (void)readObjectFromDiskForKey:(id)arg1 dataDecoding:(CDUnknownBlockType)arg2 resetExpiration:(id)arg3 whenLessThanDelta:(double)arg4 block:(CDUnknownBlockType)arg5;
- (void)executeDiskCompletionBlock:(CDUnknownBlockType)arg1 withKey:(id)arg2 object:(id)arg3 fileURL:(id)arg4;
- (void)executeCompletionBlock:(CDUnknownBlockType)arg1 withKey:(id)arg2 object:(id)arg3;
- (void)executeCacheCompletionBlock:(CDUnknownBlockType)arg1 onGroup:(id)arg2;
- (void)memoryDidRemoveObject:(id)arg1 key:(id)arg2;
- (void)diskDidRemoveObject:(id)arg1 key:(id)arg2 fileURL:(id)arg3;
- (_Bool)contains:(id)arg1 includingDiskLookup:(_Bool)arg2;
- (void)removeObjectsForKeys:(id)arg1 block:(CDUnknownBlockType)arg2;
- (void)removeAllObjectsWithBlock:(CDUnknownBlockType)arg1;
- (void)removeAllObjectsFromMemoryWithBlock:(CDUnknownBlockType)arg1;
- (void)removeAllObjectsFromStorage:(unsigned long long)arg1 exceptKeys:(id)arg2 block:(CDUnknownBlockType)arg3;
- (void)removeExpiredContentWithBlock:(CDUnknownBlockType)arg1;
- (void)removeAllObjectsExceptKeys:(id)arg1 block:(CDUnknownBlockType)arg2;
- (void)removeObjectForKey:(id)arg1 block:(CDUnknownBlockType)arg2;
- (void)invalidate;
- (void)objectForKey:(id)arg1 dataDecoding:(CDUnknownBlockType)arg2 resetExpiration:(id)arg3 whenLessThanDelta:(double)arg4 block:(CDUnknownBlockType)arg5;
- (void)objectForKey:(id)arg1 dataDecoding:(CDUnknownBlockType)arg2 block:(CDUnknownBlockType)arg3;
- (void)setObject:(id)arg1 dataEncoding:(CDUnknownBlockType)arg2 forKey:(id)arg3 expiration:(id)arg4 block:(CDUnknownBlockType)arg5;
- (id)initWithName:(id)arg1 cacheManager:(id)arg2;
- (id)initWithName:(id)arg1;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

