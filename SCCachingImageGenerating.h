//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "FLAnimatedImageGenerating.h"

@class NSDictionary, NSString, SCCachingMediaManager, UIImage;

@interface SCCachingImageGenerating : NSObject <FLAnimatedImageGenerating>
{
    id <SCCachingMediaEntity> _entity;
    SCCachingMediaManager *_cachingMediaManager;
    SCCachingImageGenerating *_sourceImageGenerating;
    long long _sourceLevel;
    struct CGSize _targetSize;
    unsigned long long _count;
    id <SCCachingImageGeneratingDelegate> _delegate;
    NSDictionary *_images;
    UIImage *_decodedImage;
}

+ (id)defaultPerformer;
@property(retain) UIImage *decodedImage; // @synthesize decodedImage=_decodedImage;
@property(copy) NSDictionary *images; // @synthesize images=_images;
@property(nonatomic) __weak id <SCCachingImageGeneratingDelegate> delegate; // @synthesize delegate=_delegate;
@property(readonly, nonatomic) unsigned long long count; // @synthesize count=_count;
- (void).cxx_destruct;
- (void)_generateMoreImagesAtIndex:(unsigned long long)arg1 decodeRequest:(id)arg2 resultHandler:(CDUnknownBlockType)arg3;
- (id)imageAtIndex:(unsigned long long)arg1 queue:(id)arg2 resultHandler:(CDUnknownBlockType)arg3;
- (id)imageAtIndex:(unsigned long long)arg1;
- (id)decodeImageWithPerformer:(id)arg1 resultHandler:(CDUnknownBlockType)arg2;
- (_Bool)hasDecoded;
- (id)initWithImages:(id)arg1 decodedImage:(id)arg2;
- (id)initWithImages:(id)arg1 decodedImage:(id)arg2 targetSize:(struct CGSize)arg3 sourceImageGenerating:(id)arg4 sourceLevel:(long long)arg5 count:(unsigned long long)arg6;
- (id)initWithImages:(id)arg1 decodedImage:(id)arg2 entity:(id)arg3 cachingMediaManager:(id)arg4 sourceLevel:(long long)arg5 count:(unsigned long long)arg6;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly, copy) NSString *description;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

