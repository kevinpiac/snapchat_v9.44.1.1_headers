//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

#import "CBPeripheralDelegate.h"
#import "NSCoding.h"
#import "SCLagunaBluetoothMonitorDelegate.h"
#import "SCLagunaCBCentralManagerEventListener.h"
#import "SCLagunaPeripheralDelegate.h"
#import "SCLagunaWifiSsidEventListener.h"

@class NSData, NSDate, NSMutableArray, NSNumber, NSString, NSUUID, SCLagunaBluetoothMonitor, SCLagunaCBCentralManager, SCLagunaContentStore, SCLagunaDeviceEventListenerAnnouncer, SCLagunaDeviceProgressMonitor, SCLagunaFirmwareUpdater, SCLagunaPeripheral, SCLagunaTask, SCLagunaTaskQueue, SCLagunaWeakTimer, SCLagunaWifiSsidScanner, SCQueuePerformer, VLKNrfRequest, VLKNrfTemperatureReport;

@interface SCLagunaDevice : NSObject <CBPeripheralDelegate, SCLagunaPeripheralDelegate, NSCoding, SCLagunaCBCentralManagerEventListener, SCLagunaBluetoothMonitorDelegate, SCLagunaWifiSsidEventListener>
{
    _Bool _hasSpaceToRecord;
    _Bool _userDidForgetDevice;
    _Bool _wifiBooted;
    _Bool _hasReconciledContentList;
    _Bool _detectedBluetoothOverloadError;
    NSString *_serialNumber;
    NSString *_displayName;
    NSString *_firmwareVersion;
    NSString *_hardwareVersion;
    NSNumber *_batteryLevel;
    NSNumber *_storageLevel;
    long long _deviceNumber;
    long long _color;
    long long _firstPairedTimestamp;
    long long _lastPairedStatusUpdatedTimestamp;
    long long _lastNameUpdatedTimestamp;
    SCQueuePerformer *_performer;
    SCLagunaWeakTimer *_deviceUpdateTimer;
    SCLagunaWeakTimer *_connectionTimer;
    SCLagunaWeakTimer *_encryptionTimer;
    SCLagunaWeakTimer *_transitionTimer;
    SCLagunaWeakTimer *_wifiStateTimer;
    VLKNrfRequest *_outstandingWifiStartRequest;
    SCLagunaWeakTimer *_rssiTimer;
    SCLagunaFirmwareUpdater *_firmwareUpdater;
    SCLagunaTask *_firmwareUploadTask;
    SCLagunaBluetoothMonitor *_bluetoothMonitor;
    SCLagunaWifiSsidScanner *_wifiSsidScanner;
    SCQueuePerformer *_centralManagerPerformer;
    long long _transferDisabledReason;
    SCLagunaDeviceProgressMonitor *_progressMonitor;
    SCLagunaWeakTimer *_ambaWatchdogKickTimer;
    NSMutableArray *_responseMonitors;
    SCLagunaContentStore *_contentStore;
    SCLagunaDeviceEventListenerAnnouncer *_deviceAnnouncer;
    SCLagunaPeripheral *_peripheral;
    SCLagunaTaskQueue *_taskQueue;
    SCLagunaCBCentralManager *_centralManager;
    NSString *_shortDisplayName;
    NSData *_sharedSecret;
    long long _state;
    NSUUID *_identifier;
    id <SCLagunaLibraryLogger> _analyticsLogger;
    long long _numberOfConnectionAttempts;
    unsigned long long _lastConnectionFailureReason;
    VLKNrfTemperatureReport *_temperatureReport;
    NSDate *_lastTemperatureReportTime;
}

+ (id)deviceColorToString:(long long)arg1;
+ (long long)_protoColorToDeviceColor:(int)arg1;
+ (id)emojiForDeviceNumber:(long long)arg1;
+ (id)_emojis;
@property(retain, nonatomic) NSDate *lastTemperatureReportTime; // @synthesize lastTemperatureReportTime=_lastTemperatureReportTime;
@property(retain, nonatomic) VLKNrfTemperatureReport *temperatureReport; // @synthesize temperatureReport=_temperatureReport;
@property(nonatomic) unsigned long long lastConnectionFailureReason; // @synthesize lastConnectionFailureReason=_lastConnectionFailureReason;
@property(nonatomic) long long numberOfConnectionAttempts; // @synthesize numberOfConnectionAttempts=_numberOfConnectionAttempts;
@property(nonatomic) __weak id <SCLagunaLibraryLogger> analyticsLogger; // @synthesize analyticsLogger=_analyticsLogger;
@property(retain, nonatomic) NSUUID *identifier; // @synthesize identifier=_identifier;
@property(nonatomic) long long state; // @synthesize state=_state;
@property(retain, nonatomic) NSData *sharedSecret; // @synthesize sharedSecret=_sharedSecret;
@property(copy, nonatomic) NSString *shortDisplayName; // @synthesize shortDisplayName=_shortDisplayName;
@property(nonatomic) __weak SCLagunaCBCentralManager *centralManager; // @synthesize centralManager=_centralManager;
@property(retain, nonatomic) SCLagunaTaskQueue *taskQueue; // @synthesize taskQueue=_taskQueue;
@property(retain, nonatomic) SCLagunaPeripheral *peripheral; // @synthesize peripheral=_peripheral;
@property(retain, nonatomic) SCLagunaDeviceEventListenerAnnouncer *deviceAnnouncer; // @synthesize deviceAnnouncer=_deviceAnnouncer;
@property(retain, nonatomic) SCLagunaContentStore *contentStore; // @synthesize contentStore=_contentStore;
@property(retain, nonatomic) NSMutableArray *responseMonitors; // @synthesize responseMonitors=_responseMonitors;
@property(retain, nonatomic) SCLagunaWeakTimer *ambaWatchdogKickTimer; // @synthesize ambaWatchdogKickTimer=_ambaWatchdogKickTimer;
@property(retain, nonatomic) SCLagunaDeviceProgressMonitor *progressMonitor; // @synthesize progressMonitor=_progressMonitor;
@property(nonatomic) long long transferDisabledReason; // @synthesize transferDisabledReason=_transferDisabledReason;
@property(retain, nonatomic) SCQueuePerformer *centralManagerPerformer; // @synthesize centralManagerPerformer=_centralManagerPerformer;
@property(retain, nonatomic) SCLagunaWifiSsidScanner *wifiSsidScanner; // @synthesize wifiSsidScanner=_wifiSsidScanner;
@property(retain, nonatomic) SCLagunaBluetoothMonitor *bluetoothMonitor; // @synthesize bluetoothMonitor=_bluetoothMonitor;
@property(retain, nonatomic) SCLagunaTask *firmwareUploadTask; // @synthesize firmwareUploadTask=_firmwareUploadTask;
@property(retain, nonatomic) SCLagunaFirmwareUpdater *firmwareUpdater; // @synthesize firmwareUpdater=_firmwareUpdater;
@property(retain, nonatomic) SCLagunaWeakTimer *rssiTimer; // @synthesize rssiTimer=_rssiTimer;
@property(retain, nonatomic) VLKNrfRequest *outstandingWifiStartRequest; // @synthesize outstandingWifiStartRequest=_outstandingWifiStartRequest;
@property(retain, nonatomic) SCLagunaWeakTimer *wifiStateTimer; // @synthesize wifiStateTimer=_wifiStateTimer;
@property(retain, nonatomic) SCLagunaWeakTimer *transitionTimer; // @synthesize transitionTimer=_transitionTimer;
@property(retain, nonatomic) SCLagunaWeakTimer *encryptionTimer; // @synthesize encryptionTimer=_encryptionTimer;
@property(retain, nonatomic) SCLagunaWeakTimer *connectionTimer; // @synthesize connectionTimer=_connectionTimer;
@property(retain, nonatomic) SCLagunaWeakTimer *deviceUpdateTimer; // @synthesize deviceUpdateTimer=_deviceUpdateTimer;
@property(retain, nonatomic) SCQueuePerformer *performer; // @synthesize performer=_performer;
@property(nonatomic) _Bool detectedBluetoothOverloadError; // @synthesize detectedBluetoothOverloadError=_detectedBluetoothOverloadError;
@property(nonatomic) _Bool hasReconciledContentList; // @synthesize hasReconciledContentList=_hasReconciledContentList;
@property(nonatomic) _Bool wifiBooted; // @synthesize wifiBooted=_wifiBooted;
@property(nonatomic) _Bool userDidForgetDevice; // @synthesize userDidForgetDevice=_userDidForgetDevice;
@property(nonatomic) long long lastNameUpdatedTimestamp; // @synthesize lastNameUpdatedTimestamp=_lastNameUpdatedTimestamp;
@property(nonatomic) long long lastPairedStatusUpdatedTimestamp; // @synthesize lastPairedStatusUpdatedTimestamp=_lastPairedStatusUpdatedTimestamp;
@property(nonatomic) long long firstPairedTimestamp; // @synthesize firstPairedTimestamp=_firstPairedTimestamp;
@property(nonatomic) long long color; // @synthesize color=_color;
@property(nonatomic) long long deviceNumber; // @synthesize deviceNumber=_deviceNumber;
@property(nonatomic) _Bool hasSpaceToRecord; // @synthesize hasSpaceToRecord=_hasSpaceToRecord;
@property(retain, nonatomic) NSNumber *storageLevel; // @synthesize storageLevel=_storageLevel;
@property(retain, nonatomic) NSNumber *batteryLevel; // @synthesize batteryLevel=_batteryLevel;
@property(copy, nonatomic) NSString *hardwareVersion; // @synthesize hardwareVersion=_hardwareVersion;
@property(copy, nonatomic) NSString *firmwareVersion; // @synthesize firmwareVersion=_firmwareVersion;
@property(copy, nonatomic) NSString *displayName; // @synthesize displayName=_displayName;
@property(copy, nonatomic) NSString *serialNumber; // @synthesize serialNumber=_serialNumber;
- (void).cxx_destruct;
- (void)centralManager:(id)arg1 didDisconnectPeripheral:(id)arg2 error:(id)arg3;
- (void)centralManager:(id)arg1 didFailToConnectPeripheral:(id)arg2 error:(id)arg3;
- (void)centralManager:(id)arg1 didConnectPeripheral:(id)arg2;
- (void)centralManagerDidUpdateState:(id)arg1;
- (void)wifiScannerDidUpdateWifiSsid:(id)arg1;
- (void)peripheralDidUpdateName:(id)arg1;
- (void)peripheralOnError:(id)arg1;
- (void)peripheralDidReceiveEncryptionResponse:(id)arg1;
- (void)peripheralDidReceiveNrfResponse:(id)arg1 forRequest:(id)arg2;
- (void)peripheralDidSetupEncryption;
- (void)peripheralDidDiscoverCharacteristics;
- (void)addDeviceLogsRequest:(CDUnknownBlockType)arg1;
- (void)addRefreshContentRequest;
- (void)firmwareUpdaterDidFail;
- (void)firmwareUpdaterDidSucceed;
- (void)firmwareUpdaterSendNrfRequest:(id)arg1;
- (void)firmwareUpdaterSendFlashUpdateRequest;
- (void)firmwareUploadTaskDidFail;
- (void)firmwareUploadTaskDidSucceed;
- (void)resetFirmwareUpdater;
- (void)requestFirmwareUpdateToFirmwareVersion:(id)arg1 digest:(id)arg2 filepath:(id)arg3;
- (void)contentListReconciled;
- (void)reset;
- (void)unpair;
- (void)removeCorruptContent:(id)arg1;
- (void)adoptBabyDevice:(id)arg1;
- (void)setTransferDisabled:(_Bool)arg1 forReason:(long long)arg2;
- (_Bool)_transferDisabled;
- (void)sendFirmwareGetDigestRequest;
- (void)sendFirmwareRevertBinaryRequest;
- (void)setPeripheralDisplayName;
- (void)clearCrashReport;
- (void)requestCrashReport;
- (void)requestBleDisconnection;
- (void)resetConnectionTimer;
- (void)ambaWatchdogKick;
- (void)requestWiFiDisconnection;
- (void)requestWiFiConnection;
- (void)requestBluetoothDisconnection;
- (void)requestBluetoothConnection;
- (void)_updateWithPeripheral:(id)arg1;
- (void)_connectPeripheral:(id)arg1;
- (void)_reconnectPeripheral;
- (void)_disconnectPeripheral;
- (void)_transitionToState:(long long)arg1;
- (void)_transitionToState:(long long)arg1 afterTime:(double)arg2;
- (void)_transitionToStateTimeoutComplete:(id)arg1;
- (void)_fire;
- (void)cancelAmbaWatchdogKickTimer;
- (void)startAmbaWatchdogKickTimer;
- (_Bool)_handleBluetoothOverload:(int)arg1;
- (_Bool)_handleTemperatureReport:(id)arg1;
- (unsigned long long)_handleWifiState:(int)arg1;
- (unsigned long long)_handleHardwareVersion:(id)arg1;
- (unsigned long long)_handleAmbaFsStatus:(id)arg1;
- (unsigned long long)_handleSerialNumberUpdate:(id)arg1;
- (unsigned long long)_handleFirmwareVersion:(id)arg1;
- (unsigned long long)_handleDeviceColor:(int)arg1;
- (unsigned long long)_handleBatteryLevel:(int)arg1;
- (void)_handleAmbaCrashed:(_Bool)arg1;
- (long long)_wifiState;
- (long long)_bluetoothState;
- (void)_handleResponse:(id)arg1 forRequest:(id)arg2;
- (void)_passMonitorsResponse:(id)arg1 forRequest:(id)arg2;
- (void)clearContentWithSuccess:(CDUnknownBlockType)arg1 failure:(CDUnknownBlockType)arg2;
- (void)_sendDeviceReset;
- (void)_sendWifiStateRequest;
- (void)_sendDeviceUpdateRequest;
- (void)_sendDeviceInfoRequest;
- (void)_sendStopWiFiRequest;
- (void)_sendStartWiFiRequest;
- (void)_sendStopBTRequest;
- (void)_sendStartBTRequest;
- (id)_myUUID;
- (void)_applicationDidBecomeActiveNotification:(id)arg1;
- (void)_applicationWillTerminateNotification:(id)arg1;
- (void)_sendReadRSSI;
- (void)_stopRssiUpdates;
- (void)_startRssiUpdates;
- (void)_stopWifiStateTimer;
- (void)_startWifiStateTimer;
- (void)_stopEncryptionSetupTimer;
- (void)_startEncryptionSetupTimer;
- (void)_stopConnectionTimeout;
- (void)_startConnectionTimeout;
- (void)_connectionDidTimeout;
- (void)_stopDeviceUpdates;
- (void)_startDeviceUpdates;
- (void)_retrievePeripheralAndReconnect;
- (id)_displayNameBLE;
- (id)_displayNameWiFi;
- (id)_displayNameBT;
- (void)bluetoothNeedsPicker;
- (void)bluetoothDidDisconnect:(id)arg1;
- (void)bluetoothDidConnect:(id)arg1;
- (void)encodeWithCoder:(id)arg1;
- (id)initWithCoder:(id)arg1;
- (_Bool)hasLowBattery;
- (_Bool)hasHdContentToDownload;
@property(readonly) unsigned long long hash;
- (_Bool)isEqual:(id)arg1;
- (void)dealloc;
@property(readonly, copy) NSString *description;
- (void)setupContentWithCache:(id)arg1;
- (void)setupWithCentralManager:(id)arg1 analyticsLogger:(id)arg2 centralManagerPerformer:(id)arg3 progressMonitor:(id)arg4;
- (id)initWithBabyDevice:(id)arg1;
- (id)initWithSerialNumber:(id)arg1 displayName:(id)arg2 color:(long long)arg3 firstPairedTimestamp:(long long)arg4 lastPairedStatusUpdatedTimestamp:(long long)arg5 lastNameUpdatedTimestamp:(long long)arg6 deviceNumber:(long long)arg7 firmwareVersion:(id)arg8;
- (id)init;
- (_Bool)tryingToConnectWiFi;
- (_Bool)connectedOverWiFi;
- (_Bool)connectedOverBT;
- (_Bool)connectedOverBLE;
- (_Bool)connectedOverBTOrWiFi;
- (_Bool)isUnpaired;
- (id)displayNameWithoutEmoji;
- (id)emoji;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly) Class superclass;

@end

