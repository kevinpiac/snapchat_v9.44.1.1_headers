//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "NSObject.h"

@class NSArray;

@interface SCGalleryCollectionViewLayoutCalculator : NSObject
{
    double _screenAspectRatio;
    _Bool _invalidated;
    NSArray *_calculatedLayouts;
    double _collectionViewWidth;
    NSArray *_entries;
}

@property(copy, nonatomic) NSArray *entries; // @synthesize entries=_entries;
@property(nonatomic) double collectionViewWidth; // @synthesize collectionViewWidth=_collectionViewWidth;
- (void).cxx_destruct;
- (id)_layoutRowWithDataModel:(id)arg1;
- (id)_layoutRowWithDataModel1:(id)arg1 dataModel2:(id)arg2;
- (id)_layoutRowWithDataModel1:(id)arg1 dataModel2:(id)arg2 dataModel3:(id)arg3;
- (void)_calculateLayout;
- (id)_layoutWithDataModel1:(id)arg1 dataModel2:(id)arg2 dataModel3:(id)arg3 dataModel4:(id)arg4;
- (void)_appendLayouts:(id)arg1 index:(unsigned long long)arg2 dataModel1:(id)arg3 dataModel2:(id)arg4 dataModel3:(id)arg5 isFirstGroup:(_Bool)arg6;
- (unsigned long long)formatForDataModelAtIndex:(long long)arg1;
- (struct CGSize)sizeForDataModelAtIndex:(long long)arg1;
- (id)init;

@end

