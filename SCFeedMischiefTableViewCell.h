//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCFeedSwipeableTableViewCell.h"

@interface SCFeedMischiefTableViewCell : SCFeedSwipeableTableViewCell
{
}

- (void)snapTimerDidExpire;
- (void)resetNextVC:(id)arg1;
- (void)prepareNextVC:(id)arg1;
- (_Bool)delayedTapGestureRecognizerShouldBegin;
- (_Bool)longPressGestureRecognizerShouldBegin;
- (_Bool)doubleTapGestureRecognizerShouldBegin;
- (_Bool)tapGestureRecognizerShouldBegin;
- (void)handleLongPress:(id)arg1;
- (void)handleDelayedTap:(id)arg1;
- (void)handleDoubleTap:(id)arg1;
- (void)handleTap:(id)arg1;

@end

