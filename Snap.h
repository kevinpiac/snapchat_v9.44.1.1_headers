//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "EphemeralMedia.h"

#import "MediaDataSource.h"
#import "MediaDelegate.h"
#import "MediaImageProcessingDelegate.h"
#import "MediaUploadDelegate.h"
#import "NSCoding.h"
#import "SCChatMessage.h"
#import "SnapVideoFilterDelegate.h"

@class AVPlayerItem, NSArray, NSDate, NSDictionary, NSNumber, NSString, NSURL;

@interface Snap : EphemeralMedia <SnapVideoFilterDelegate, MediaDataSource, MediaDelegate, MediaUploadDelegate, MediaImageProcessingDelegate, NSCoding, SCChatMessage>
{
    _Bool _clearedBySender;
    _Bool _clearedByRecipient;
    _Bool _pending;
    _Bool _recentlyViewedAndHasNotLeftView;
    _Bool _replayed;
    _Bool _expiredWhileStackNotEmpty;
    _Bool _isInitialView;
    _Bool _isPaidToReplay;
    _Bool _isLastViewedSnapInStack;
    _Bool _hideBroadcastTimer;
    _Bool _broadcastSnap;
    _Bool _needsRetry;
    double _closedAt;
    NSString *_display;
    NSString *_sender;
    NSString *_recipient;
    NSString *_correspondentId;
    NSArray *_recipients;
    NSArray *_invitedRecipients;
    long long _replayAnimationStateFeed;
    long long _replayAnimationStateChat;
    long long _screenshots;
    NSDate *_sentTimestamp;
    long long _status;
    long long _state;
    NSDate *_timestamp;
    NSDate *_snapStreakExpiryTime;
    NSNumber *_snapStreakCount;
    NSString *_encryptedSnapId;
    NSDate *_finishViewingTimestamp;
    long long _viewSource;
    AVPlayerItem *_playerItem;
    double _secondsViewed;
    double _timeStartedOnScreen;
    NSNumber *_time_left;
    NSString *_broadcastActionText;
    NSString *_broadcastSecondaryText;
    NSURL *_broadcastUrlToOpen;
    NSURL *_broadcastMediaUrl;
    NSString *_stackId;
    id <SCInviteSnapSenderDelegate> _inviteSnapSenderDelegate;
    NSDictionary *_inviteSnapMetadata;
    NSString *_fideliusVersion;
    NSString *_senderOutAlpha;
    NSString *_recipientOutAlpha;
    NSDate *_fideliusSendTimestamp;
    NSString *_fideliusSnapKey;
    NSString *_fideliusSnapIv;
    NSDictionary *_fideliusInfo;
    unsigned long long _numTimesReloaded;
    unsigned long long _numAutomaticRetries;
    unsigned long long _numTimesCanBeReplayed;
}

+ (id)targetSnapFromEphemeralMedia:(id)arg1;
+ (id)snapFromEphemeralMedia:(id)arg1;
+ (id)stringForSnapStatus:(long long)arg1;
+ (id)stringForSnapState:(long long)arg1;
+ (id)videoDirectory;
+ (_Bool)isStateViewing:(long long)arg1;
+ (_Bool)isStateReceivedAndOpened:(long long)arg1;
+ (_Bool)isStateSendingOrHasFailed:(long long)arg1;
+ (_Bool)shouldHandleLongPressOnSnap:(id)arg1;
+ (_Bool)shouldHandleTapOnSnap:(id)arg1;
+ (id)viewedIconImageForMediaType:(long long)arg1;
+ (id)replayedIconImageForMediaType:(long long)arg1;
@property(nonatomic) unsigned long long numTimesCanBeReplayed; // @synthesize numTimesCanBeReplayed=_numTimesCanBeReplayed;
@property(nonatomic) unsigned long long numAutomaticRetries; // @synthesize numAutomaticRetries=_numAutomaticRetries;
@property(nonatomic) unsigned long long numTimesReloaded; // @synthesize numTimesReloaded=_numTimesReloaded;
@property(retain, nonatomic) NSDictionary *fideliusInfo; // @synthesize fideliusInfo=_fideliusInfo;
@property(retain, nonatomic) NSString *fideliusSnapIv; // @synthesize fideliusSnapIv=_fideliusSnapIv;
@property(retain, nonatomic) NSString *fideliusSnapKey; // @synthesize fideliusSnapKey=_fideliusSnapKey;
@property(retain, nonatomic) NSDate *fideliusSendTimestamp; // @synthesize fideliusSendTimestamp=_fideliusSendTimestamp;
@property(retain, nonatomic) NSString *recipientOutAlpha; // @synthesize recipientOutAlpha=_recipientOutAlpha;
@property(retain, nonatomic) NSString *senderOutAlpha; // @synthesize senderOutAlpha=_senderOutAlpha;
@property(retain, nonatomic) NSString *fideliusVersion; // @synthesize fideliusVersion=_fideliusVersion;
@property(nonatomic) _Bool needsRetry; // @synthesize needsRetry=_needsRetry;
@property(retain, nonatomic) NSDictionary *inviteSnapMetadata; // @synthesize inviteSnapMetadata=_inviteSnapMetadata;
@property(nonatomic) __weak id <SCInviteSnapSenderDelegate> inviteSnapSenderDelegate; // @synthesize inviteSnapSenderDelegate=_inviteSnapSenderDelegate;
@property(copy, nonatomic) NSString *stackId; // @synthesize stackId=_stackId;
@property(retain, nonatomic) NSURL *broadcastMediaUrl; // @synthesize broadcastMediaUrl=_broadcastMediaUrl;
@property(retain, nonatomic) NSURL *broadcastUrlToOpen; // @synthesize broadcastUrlToOpen=_broadcastUrlToOpen;
@property(nonatomic) _Bool broadcastSnap; // @synthesize broadcastSnap=_broadcastSnap;
@property(nonatomic) _Bool hideBroadcastTimer; // @synthesize hideBroadcastTimer=_hideBroadcastTimer;
@property(retain, nonatomic) NSString *broadcastSecondaryText; // @synthesize broadcastSecondaryText=_broadcastSecondaryText;
@property(retain, nonatomic) NSString *broadcastActionText; // @synthesize broadcastActionText=_broadcastActionText;
@property(retain, nonatomic) NSNumber *time_left; // @synthesize time_left=_time_left;
@property(nonatomic) double timeStartedOnScreen; // @synthesize timeStartedOnScreen=_timeStartedOnScreen;
@property(nonatomic) double secondsViewed; // @synthesize secondsViewed=_secondsViewed;
@property(retain, nonatomic) AVPlayerItem *playerItem; // @synthesize playerItem=_playerItem;
@property(nonatomic) _Bool isLastViewedSnapInStack; // @synthesize isLastViewedSnapInStack=_isLastViewedSnapInStack;
@property(nonatomic) _Bool isPaidToReplay; // @synthesize isPaidToReplay=_isPaidToReplay;
@property(nonatomic) _Bool isInitialView; // @synthesize isInitialView=_isInitialView;
@property(nonatomic) long long viewSource; // @synthesize viewSource=_viewSource;
@property(retain, nonatomic) NSDate *finishViewingTimestamp; // @synthesize finishViewingTimestamp=_finishViewingTimestamp;
@property(copy, nonatomic) NSString *encryptedSnapId; // @synthesize encryptedSnapId=_encryptedSnapId;
@property(retain, nonatomic) NSNumber *snapStreakCount; // @synthesize snapStreakCount=_snapStreakCount;
@property(retain, nonatomic) NSDate *snapStreakExpiryTime; // @synthesize snapStreakExpiryTime=_snapStreakExpiryTime;
@property(nonatomic) _Bool expiredWhileStackNotEmpty; // @synthesize expiredWhileStackNotEmpty=_expiredWhileStackNotEmpty;
@property(retain, nonatomic) NSDate *timestamp; // @synthesize timestamp=_timestamp;
@property(nonatomic) long long state; // @synthesize state=_state;
@property(nonatomic) long long status; // @synthesize status=_status;
@property(retain, nonatomic) NSDate *sentTimestamp; // @synthesize sentTimestamp=_sentTimestamp;
@property(nonatomic) long long screenshots; // @synthesize screenshots=_screenshots;
@property(nonatomic) _Bool replayed; // @synthesize replayed=_replayed;
@property(nonatomic) long long replayAnimationStateChat; // @synthesize replayAnimationStateChat=_replayAnimationStateChat;
@property(nonatomic) long long replayAnimationStateFeed; // @synthesize replayAnimationStateFeed=_replayAnimationStateFeed;
@property(retain, nonatomic) NSArray *invitedRecipients; // @synthesize invitedRecipients=_invitedRecipients;
@property(retain, nonatomic) NSArray *recipients; // @synthesize recipients=_recipients;
@property(retain, nonatomic) NSString *correspondentId; // @synthesize correspondentId=_correspondentId;
@property(retain, nonatomic) NSString *recipient; // @synthesize recipient=_recipient;
@property(nonatomic) _Bool recentlyViewedAndHasNotLeftView; // @synthesize recentlyViewedAndHasNotLeftView=_recentlyViewedAndHasNotLeftView;
@property(retain, nonatomic) NSString *sender; // @synthesize sender=_sender;
@property(nonatomic) _Bool pending; // @synthesize pending=_pending;
@property(retain, nonatomic) NSString *display; // @synthesize display=_display;
@property(nonatomic) double closedAt; // @synthesize closedAt=_closedAt;
@property(nonatomic) _Bool clearedByRecipient; // @synthesize clearedByRecipient=_clearedByRecipient;
@property(nonatomic) _Bool clearedBySender; // @synthesize clearedBySender=_clearedBySender;
- (void).cxx_destruct;
- (long long)messageType;
- (_Bool)isInviteSnapSent;
- (_Bool)isWaitingToSendInvite;
- (_Bool)containsInvitedRecipients;
- (_Bool)canDelete;
- (id)clearedDictionary;
- (_Bool)sending;
- (_Bool)failedToSend;
- (_Bool)sentByUser;
- (_Bool)isInvalidAfterUnarchiving;
- (_Bool)isUnread;
- (_Bool)shouldDisplayBelowFoldInChat;
- (id)sendJSON;
- (_Bool)isReleasedBy:(id)arg1;
- (_Bool)isReleased;
- (_Bool)isSavedBy:(id)arg1;
- (_Bool)isSaved;
- (id)conversationId;
- (id)messageRecipient;
- (id)messageSender;
- (id)iterToken;
- (id)sequenceNumber;
- (id)feedSortTimestamp;
- (id)feedDisplayTimestamp;
- (id)messageTimestamp;
- (void)imageProcessingDidSucceedForMedia:(id)arg1;
- (void)postFailToSendNotification;
- (_Bool)shouldPostFailToSendNotification;
- (id)encryptedMediaDataToUpload:(id)arg1;
- (void)mediaUploadDidFailForMedia:(id)arg1;
- (void)mediaUploadDidSucceedForMedia:(id)arg1;
- (long long)uploadMediaTypeForMedia:(id)arg1;
- (id)uploadMediaIdForMedia:(id)arg1;
- (void)fetchMediaDidSucceedForMedia:(id)arg1;
- (void)fetchMediaIsGoneForMedia:(id)arg1;
- (void)fetchMediaDidFailForMedia:(id)arg1;
- (void)fetchMediaIsLoadingForMedia:(id)arg1;
- (_Bool)isBroadcastForMedia:(id)arg1;
- (id)requestContexts;
- (long long)requestFallbackPriority;
- (long long)requestPriorityUserInitiated:(_Bool)arg1;
- (_Bool)needsAuthToFetch;
- (_Bool)encrypt;
- (_Bool)persist;
- (id)encryptionDictionaryForMedia:(id)arg1;
- (id)decryptData:(id)arg1 forMedia:(id)arg2;
- (_Bool)shouldClientDecrypt;
- (id)mediaIdForMedia:(id)arg1;
- (id)endpointParamsForMedia:(id)arg1;
- (id)endpointForMedia:(id)arg1;
- (id)persistingFailuresURLForMedia:(id)arg1;
- (void)loadPersistedFailedSnapData;
- (id)uploadSuccessUserDefaultsKey;
@property(readonly, copy) NSString *description;
- (id)secretReceivedSnapParameters;
- (id)receivedSnapParameters;
- (id)secretSentSnapParameters;
- (id)sentSnapParameters;
- (void)logSendEvent;
- (id)consistentId;
- (_Bool)veryRecentlyOpen;
- (id)username;
- (_Bool)isExpired;
- (_Bool)canBeRemoved;
- (_Bool)isGroupSend;
- (id)nameForView;
- (id)allRecipientDisplayNames;
- (id)_invitedRecipientsInfo;
- (id)_allRecipientUsernames;
- (id)_allRecipients;
- (id)recipientUsernames;
- (id)recipientIdWithDelimiter:(id)arg1;
- (id)initWithCoder:(id)arg1;
- (void)encodeWithCoder:(id)arg1;
- (void)replaceWithSnap:(id)arg1;
- (void)setCanBeReplayed:(_Bool)arg1;
- (void)didReplay;
- (_Bool)hasFreeReplay;
- (_Bool)isUsingFreeReplay;
- (_Bool)canBeReplayed;
- (_Bool)isDoublePost;
- (_Bool)shouldReplaceWithSnap:(id)arg1;
- (_Bool)updateWithSnap:(id)arg1;
- (_Bool)isBroadcastAndFromGCS;
- (_Bool)isPreparedToReplayOrReplaying;
- (_Bool)isSentSnap;
- (_Bool)isReceivedAndUnopened;
- (_Bool)isReceivedAndOpened;
- (_Bool)isViewingOrUnviewed;
- (_Bool)isViewing;
- (_Bool)isLoading;
- (_Bool)isLoaded;
- (_Bool)isNotLoaded;
- (_Bool)isScreenshotted;
- (_Bool)isSentAndOpened;
- (_Bool)isDelivered;
- (_Bool)isSentDeliveredOrOpened;
- (_Bool)isSent;
- (_Bool)isSendingOrHasFailed;
- (_Bool)isMediaProcessingOrFailedToProcess;
- (_Bool)isMediaProcessing;
- (_Bool)hasMediaProcessingFailed;
- (_Bool)hasFailed;
- (_Bool)isSending;
- (id)fideliusPhiNonBootstrap:(id)arg1 recipientUserId:(id)arg2;
- (id)fideliusPhi;
- (id)uploadParametersWithData:(_Bool)arg1;
- (void)uploadSnap;
- (void)setUploading;
- (CDUnknownBlockType)uploadFailureBlock;
- (CDUnknownBlockType)uploadSuccessBlock;
- (void)retrySending:(_Bool)arg1;
- (_Bool)autoRetrySendingIfNetworkReachable;
- (void)doSend;
- (void)postSend;
- (void)sendWithPreSend:(_Bool)arg1 postSend:(_Bool)arg2;
- (void)preSend;
- (void)uploadAndSend;
- (void)markAsFailedToSendAndExpired;
- (_Bool)shouldClearSentSnap;
- (void)incNumTimesReloaded;
- (_Bool)isSentButNotYetReturned;
- (void)clearNumTimesReloaded;
- (id)fideliusInversePhi;
- (void)markAsViewed;
- (void)replay;
- (void)initFields;
- (id)updatedSnapDictionary;
- (void)doFideliusRetry;
- (void)logBadRecipient;
- (void)logBadSenderUserId;
- (void)processReceivedAck:(id)arg1;
- (void)processAck;
- (void)processRetry;
- (id)initWithJSONDictionary:(id)arg1;
- (id)initwithReceivedMessageSCCPJSONDictionary:(id)arg1;
- (id)initWithClientId;
- (void)videoProcessingDidFailForSnapVideoFilter:(id)arg1 error:(id)arg2;
- (void)videoProcessingDidSucceedForSnapVideoFilter:(id)arg1 data:(id)arg2;
- (void)clearFromChat;
- (_Bool)shouldDisplayInChat;
- (void)updateWithStateMessage:(id)arg1;
- (id)stateMessageWithParametersDelegate:(id)arg1;

// Remaining properties
@property(readonly, copy) NSString *debugDescription;
@property(readonly) unsigned long long hash;
@property(readonly) Class superclass;

@end

