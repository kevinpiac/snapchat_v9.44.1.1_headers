//
//     Generated by class-dump 3.5 (64 bit).
//
//     class-dump is Copyright (C) 1997-1998, 2000-2001, 2004-2013 by Steve Nygard.
//

#import "SCADiscoverSnapViewBase.h"

#import "NamedEvent.h"

@class NSNumber, NSString;

@interface SCADiscoverAdView : SCADiscoverSnapViewBase <NamedEvent>
{
    NSNumber *fullView;
    NSNumber *adIndexCount;
    NSNumber *adIndexPos;
    NSNumber *snapTimeSec;
    NSNumber *mediaDisplayTimeSec;
    long long mediaType;
    NSString *adsnapId;
    NSString *adId;
    NSString *adUnitId;
    NSString *adRequestClientId;
    NSString *adsnapPlacementId;
    NSString *adsnapLineItemId;
    NSString *userAdId;
}

+ (id)copy:(id)arg1;
- (void).cxx_destruct;
- (id)asDictionary;
- (id)getUserAdId;
- (void)setUserAdId:(id)arg1;
- (id)getAdsnapLineItemId;
- (void)setAdsnapLineItemId:(id)arg1;
- (id)getAdsnapPlacementId;
- (void)setAdsnapPlacementId:(id)arg1;
- (id)getAdRequestClientId;
- (void)setAdRequestClientId:(id)arg1;
- (id)getAdUnitId;
- (void)setAdUnitId:(id)arg1;
- (id)getAdId;
- (void)setAdId:(id)arg1;
- (id)getAdsnapId;
- (void)setAdsnapId:(id)arg1;
- (long long)getMediaType;
- (void)setMediaType:(long long)arg1;
- (double)getMediaDisplayTimeSec;
- (void)setMediaDisplayTimeSec:(double)arg1;
- (double)getSnapTimeSec;
- (void)setSnapTimeSec:(double)arg1;
- (long long)getAdIndexPos;
- (void)setAdIndexPos:(long long)arg1;
- (long long)getAdIndexCount;
- (void)setAdIndexCount:(long long)arg1;
- (_Bool)getFullView;
- (void)setFullView:(_Bool)arg1;
- (id)getEventName;
- (id)init;

@end

